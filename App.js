import React, {Component} from 'react';
import {
    View,
    AsyncStorage
} from 'react-native';
import {RootNavigator} from './router';
import ChatEngineProvider from "./pages/Chat/ce-core-chatengineprovider";
import ChatEngineProviderMe from "./pages/Chat/ce-core-chatengineproviderme";

// import ChatEngine from 'chat-engine'
import FlashMessage from "react-native-flash-message";

import Spinner from 'react-native-loading-spinner-overlay';

export default class App extends Component {

    constructor(props) {
        super(props);
        console.disableYellowBox = true; 
        this.state = {
            isLoggedIn: null,
            checkLogIn: false
        };
        AsyncStorage.setItem('otherUserChannelName','')
    }

    componentDidMount() {
        this.getSetupData();
    }

    async getSetupData() {
        let _this = this;
        await AsyncStorage.multiGet(['isLoggedIn', ChatEngineProvider.ASYNC_STORAGE_USERDATA_KEY]).then((value) => {
            if (value[0][1]) {
                if (value[1][1]) {
                    console.log("value 1   1")
                    console.log(value[1][1])
                    console.log(value)
                    // try {
                    //     ChatEngineProvider.connect(value[1][1]).then(() => {
                    //         // ChatEngine.once('$.ready', this.registerPush.bind(this), );  

                    //         _this.setState({
                    //             isLoggedIn: true,
                    //             checkLogIn: true
                    //         })
                    //     });
                    // } catch (error) {
                    //     // Error retrieving data
                    // }
                    ChatEngineProviderMe.connect(
                        value[1][1],
                        
                      ).then(() => {
                        this.setState({loader: false})
                        ChatEngineProvider.connect(
                        value[1][1],
                        
                        ).then(() => {
                            _this.setState({
                                isLoggedIn: true,
                                checkLogIn: true
                            })
                          }
                        );
                      });


                } else {
                    _this.setState({
                        isLoggedIn: true,
                        checkLogIn: true
                    })
                }
            } else {
                _this.setState({
                    isLoggedIn: false,
                    checkLogIn: true
                })
            }
        })
    }


    render() {
        const RenderScreen = RootNavigator(this.state.isLoggedIn);
        console.log("this.state.isLoggedIn")
        console.log(this.state.isLoggedIn)
        if (this.state.checkLogIn) {
            return (
                <View style={{flex: 1, backgroundColor: 'white'}}>
                  <RenderScreen/>
                  <FlashMessage position="top" />
                </View>
            );
        } else {
            return (
                <View style={{flex: 1, backgroundColor: 'white'}}>
                    <FlashMessage position="top" />
                    <Spinner
                        visible={!this.state.checkLogIn}
                        textContent={"Loading..."}
                        color='#00DBBB'
                        overlayColor='rgba(0, 0, 0, 0.6)'
                        textStyle={{color: '#00DBBB'}}
                    />
                    
                </View>
            );
        }
    }
}

