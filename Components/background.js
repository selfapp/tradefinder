import React from 'react';
import { View, ScrollView, ImageBackground, Keyboard, TouchableWithoutFeedback } from 'react-native'
export default Background = (props) => {
    const color = props.backgroundColor || ['#336979', '#8DA9B7'];
    return (
        <ImageBackground style={{
            flex: 1,
            alignItems: 'center',
        }} source={require('../assets/login.png')} resizeMode='cover' >
            <ScrollView contentContainerStyle={{ alignItems: 'center', paddingBottom: 50 }}
                scrollEventThrottle={1000}
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps={'handled'}
                keyboardDismissMode={'interactive'}
                onScroll={props.handleScroll}>
                {props.children}
            </ScrollView>
        </ImageBackground>
    )
}