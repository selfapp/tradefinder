import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, ImageBackground, Alert} from 'react-native';
import {
  CustomPicker,
  FieldTemplateSettings,
  OptionTemplateSettings,
  CustomPickerActions
} from 'react-native-custom-picker'


export default class CustomPickerComponent extends Component {

  constructor(props) {
    super(props)
  }  

  renderHeader() {
    return (
      <View style={styles.headerContainer}>
        <Text style={{ margin: 0, fontSize: 22, textAlign: 'center', marginTop: 10, marginBottom: 10, color: '#5A5A5A' }}>
          Select Category
    </Text>
      </View>
    )
  }

  renderFooter(action) {
    return (
      <TouchableOpacity
        style={styles.footerContainer}
        onPress={() => {
          Alert.alert('Close Dropdown', "Are you sure?", [
            {
              text: 'Yes',
              onPress: action.close.bind(this)
            }
          ])
        }}
      >
        <View style={styles.button3} >
          <Text style={{ color: '#ffffff', fontSize: 15, fontWeight: 'bold', textAlign: 'center' }}>Done</Text>
        </View>
      </TouchableOpacity>
    )
  }

  renderField(settings) {
    const { selectedItem, defaultText, getLabel, clear } = settings
    return (
      <View>
        {!selectedItem && <Text style={[styles.text, { color: 'grey' }]}>{defaultText}</Text>}
        {selectedItem && (
          <View style={styles.innerContainer}>
            <Text style={[styles.text, { color: selectedItem.color }]}>
              {getLabel(selectedItem)}
            </Text>
          </View>
        )}
      </View>
    )
  }

  renderOption(settings) {
    const { item, getLabel } = settings
    return (
      <View style={styles.optionContainer}>
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.innerContainer}>
            <Text style={{ color: '#222222' }}>{getLabel(item)}</Text>
          </View>
        </ScrollView>
      </View>
    )
  }

  render() {
    return (
      <ScrollView>
          <View style={{ marginLeft: '6%', width: '83%' }}>
            <ImageBackground style={{ marginLeft: '0%' }} source={require('../assets/yellowback-icon.png')} >
              <CustomPicker
                placeholder={'Select from dropdown'}
                options={this.props.options}
                getLabel={item => item.name}
                fieldTemplate={this.renderField}
                optionTemplate={this.renderOption}
                headerTemplate={this.renderHeader}
                footerTemplate={this.renderFooter}
                onValueChange={value => {
                this.props.getCategoryDetailValues(value)
                }}
              >
              </CustomPicker>
            </ImageBackground>
          </View>
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: '#E8E8E8'
  },
  footerContainer: {
    backgroundColor: '#ffffff',
  },
  text: {
    fontSize: 14,
    textAlign: 'center',
    color: '#000000',
    marginTop: 5,
    marginBottom: 5,
    marginLeft: '15%',
    marginRight: '15%',
    fontWeight: 'bold'
  },
  innerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  optionContainer: {
    padding: 2,
    borderBottomColor: 'gray',
    borderBottomWidth: 0.8,
    justifyContent: 'center',
    marginTop: 10,
  },
  scrollContainer: {
    backgroundColor: '#ffffff'
  },
  button3: {
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: '#00DBBB',
    width: '80%',
    height: 45,
    marginLeft: '10%',
    marginRight: '10%',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});