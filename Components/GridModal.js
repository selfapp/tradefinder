import React, { Component, PropTypes } from 'react';
import {
    StyleSheet, Text, View, ScrollView, Image, TouchableOpacity, AsyncStorage, ActivityIndicator,
    KeyboardAvoidingView, Alert, TextInput, Modal, FlatList,
    TouchableWithoutFeedback, ImageBackground, Platform, Dimensions
} from 'react-native';
import Button from './button';
const { width, height } = Dimensions.get('window')


    export default GridModal = (props) => {
        return (
            <Modal
                transparent={true}
                visible={props.gridModalVisible}
                onRequestClose={() => {
                    // alert("Modal has been closed.")
                }}>
                <TouchableWithoutFeedback style={{ flex: 1 }} onPress={() => props.setGridModalVisible(false)}>
                    
                    <View style={styles.optionMenuModal}>
                        <View style={styles.optionMenuView}>
                        <KeyboardAvoidingView
                        behavior='position'
                        style={styles.modalContentDetails}
                        >
                            <View style={{backgroundColor:'white'}}>
                            <View style={{ height: '10%', backgroundColor: '#EBEBEB', alignItems: 'center', width: '100%', marginBottom: 10 }}>
                                <Text style={{ color: '#565656', marginTop: '4%', fontSize: 17 }}>
                                    Add to Wishlist
                        </Text>
                            </View>
                            <View style={{width:'100%'}}> 

                                <Text style={{ color: '#565656', marginTop: '4%', fontSize: 17,marginHorizontal:10 }}>
                                   It helps to let people know what you wish to trade your items for.
                        </Text>

                       <View style={{ flexDirection: 'row', marginTop: '4%' }}>
                    <Text style={{ color: '#585858', fontSize: 18, fontWeight: '700', marginLeft: '5%' }}>Pick a Category:</Text>
                </View>
                
                {props.error !== ''?(<View style={{ flexDirection: 'row'}}>
                    <Text style={{ color: 'red', fontSize: 14, fontWeight: '500', marginLeft: '5%' }}>{props.error}</Text>
                </View>):(null)}
                
                <TouchableOpacity style={{width: '90%', borderRadius: 2, borderWidth: 2, borderColor: '#585858', marginLeft: '5%',
                               marginTop:'4%',height:32}}
              onPress={() => props.navigatecategory()}
              >

                <View  style={{flexDirection: 'row',alignItems:'center' }}>
                    <Text style={{ color: '#585858', fontSize: 16, fontWeight: '700', marginLeft: '3%',width:'85%' }}>{(props.selectedCat) ? props.selectedCat.name : 'Select from Dropdown'}</Text>
                    <Image source={require('../assets/down-arroe.png')}
                            style={{width:25, height: 25, }}
                             resizeMode='contain'/>
                </View>
                </TouchableOpacity>
                <View style={{marginTop:'4%',marginLeft: '5%'}}>
                <Text style={{ color: '#585858', fontSize: 18, fontWeight: '700',width:'85%' }}>Type Keywords:</Text>

               
           
            <TextInput
                    style={{ width: '96%',height: 70,textAlignVertical:'top',borderWidth:1,paddingLeft:'5%',marginTop:10}}
                    autoCorrect={false}
                    // multiline={true}
                    returnKeyLabel='done'
                    returnKeyType='done'
                    keyboardType='default'
                    placeholder="Your keywords." 
                    onChangeText={(itemDescripton) => props.textcategory(itemDescripton)}
                    placeholderTextColor="#585858" underlineColorAndroid='transparent' />
               
            
                </View>

                <TouchableOpacity
                        style={{
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#14DFC1',
                            marginTop: 20,
                            marginBottom:10,
                            height: 50,
                            marginLeft: '5%',
                            marginRight:'5%',
                        }}
                        onPress= {() => props.done()}>
                        <Text style={{
                            fontSize: 16, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Bold"
                        }}>{
                                props.makeOffer? 
                                'Save Trade':'DONE'
                            } </Text>
                </TouchableOpacity>
                            
                </View>
                </View>
                </KeyboardAvoidingView>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }

const styles = StyleSheet.create({
    optionMenuModal: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,.6)',
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3
    },
    optionMenuView: {
        backgroundColor: 'green',
        width: '90%',
        height: '80%',
        backgroundColor: '#ffffff',
        borderRadius: 3,
        elevation: 3,
    },
    ImageComponentStyleGrid: {
        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        width: (width / 4),
        height: (width / 4),
        marginTop: '5%',
        left: '5%',
        marginLeft: '4.5%',
        marginRight: '1%',
        marginBottom: '1%'
    },
    TextComponentStyleGrid: {
        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        width: (width / 4),
        height: (width / 10),
        left: '10%',
        marginLeft: '1%',
        marginRight: '3%',
    },
    ItemTextStyle: {
        color: '#222222',
        fontSize: 10,
        textAlign: 'center',
    },
    modalContentDetails: {
        ...Platform.select({
            android: {
                // top: '5%',
                width: '100%',
                // marginLeft: '5%',
                // marginRight: '5%',
                // marginTop: '0%',
                // height:'55%',
              //  marginBottom: '10%',
                backgroundColor: 'white',
                borderWidth: 1,
                borderRadius: 4,
                borderColor: 'rgba(0, 0, 0, 0.1)',
            },
            ios: {
                //  top:'5%',
                width: '100%',
                // marginLeft: '5%',
                // marginRight: '5%',
                backgroundColor: 'white',
                borderWidth: 1,
                borderRadius: 4,
                borderColor: 'rgba(0, 0, 0, 0.1)',
            }
        })
    }
});