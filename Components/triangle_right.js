import React from 'react';
import {StyleSheet, View} from 'react-native';

const TriangleRight = (props) => (
    <View style={[styles.triangleRight, {
        borderLeftColor: props.color,
        borderLeftWidth: props.size,
        borderTopWidth: props.size,
        borderBottomWidth: props.size
    }]}/>
);

const styles = StyleSheet.create({
    triangleRight: {
        width: 0,
        height: 0,
        borderTopColor: 'transparent',
        borderBottomColor: 'transparent'
    }
});

export default TriangleRight