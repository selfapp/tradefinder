import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  ImageBackground, 
  TouchableOpacity, 
  ScrollView, 
  TouchableHighlight, 
  Dimensions, 
  FlatList, 
  Alert,
  Image
} from 'react-native';
const { width, height } = Dimensions.get('window')
import {
  BoldText,RegularText
} from '../Components/styledTexts'

export default Grid = (props) => {
    return (
      <ScrollView style={styles.scrollContainer}>
          {
           (props.itemsList && props.itemsList.length) ? (
              <FlatList
                data={props.itemsList}
                extraData={props.mySelectedItem}
                renderItem={({ item }) =>
                
                  <TouchableOpacity 
                  disabled={props.mySelectedItem === item.id ? (true):(false)}
                  style={{justifyContent:'center'}} onPress={() => props.passingGridData(item)}>
                    {props.tradingItems ?
                      (props.mySelectedItems.indexOf(item.id) >= 0 ? (
                        <Image style={{ height: 20, width: 20, position: 'absolute', zIndex: 5,top: 5,alignSelf:'flex-end',right:10 }}
                          resizeMode='contain'
                          source={require('../assets/check_circle.png')}
                        />
                      ) : null) : (props.mySelectedItem === item.id ? (
                        <Image style={{ height: 20, width: 20, position: 'absolute', zIndex: 5,top: 5,alignSelf:'flex-end',right:10 }}
                          resizeMode='contain'
                          source={require('../assets/check_circle.png')}
                        />
                      ) : null)
                    } 
                    <Image style={styles.ImageComponentStyleGrid}
                      source={{ uri: item.pictures[0].picture }}
                    />
                    <View style={styles.TextComponentStyleGrid}>
                      <RegularText style={styles.ItemTextStyle} numberOfLines={1}>{item.title}</RegularText>
                    </View>
                  </TouchableOpacity>}
                numColumns={3}
                key={'THREE COLUMN'}
              />
            ) :
              (
                <View style={{ marginTop: '15%', alignItems: 'center' }} >
                  <BoldText
                    style={{
                      justifyContent: 'center',
                      color: '#888888', fontSize: 20,
                    }}
                  >
                    No Items Found
                </BoldText>
                </View>
              )
          }
      </ScrollView>
    );
  }


const styles = StyleSheet.create({
  scrollContainer: {
    backgroundColor: '#ffffff'
  },
  ImageComponentStyleGrid: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
    width: (width / 4),
    height: (width / 4),
    marginTop: '5%',
    left: '10%',
    marginLeft: '1%',
    marginRight: '6%',
    marginBottom: '1%'
  },
  TextComponentStyleGrid: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'flex-start',
    width: (width / 4),
    height: (width / 10),
    left: '10%',
    marginLeft: '1%',
    marginRight: '6%',
  },
  ItemTextStyle: {
    color: '#222222',
    fontSize: 10,
    textAlign: 'left'
  }
});
