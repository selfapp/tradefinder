import React, {PropTypes} from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    ActivityIndicator,
    KeyboardAvoidingView,
    TextInput,
    Modal,
    TouchableWithoutFeedback,
    Platform,Text
} from 'react-native';
import Button from './button';
import {
    BoldText,
    RegularText
} from './styledTexts'

export default EditProfileModal = (props) => {
    return (
        <Modal
            transparent={true}
            visible={props.visibleModal}
            onRequestClose={() => props.setEditProfileModalVisible(false)}
        >
            <TouchableWithoutFeedback style={{flex: 1}} onPress={() => 
                //  props.setEditProfileModalVisible(false)
                 console.log('')
                }>
                <View
                    style={styles.modalBackground}
                >
                    <KeyboardAvoidingView
                        behavior='position'
                        keyboardVerticalOffset={-100}
                        style={styles.modalContent}
                        // keyboardVerticalOffset={
                        //     Platform.select({
                        //         ios: () => 0,
                        //         android: () => 50
                        //     })()
                        // }
                    >
                        <View style={{backgroundColor:'white'}}>
                        <View style={{
                            marginTop: 0, width: '100%', backgroundColor: '#ECECEC',
                            borderWidth: 1, borderColor: '#ECECEC', borderRadius: 4
                        }}>
                            <BoldText
                                style={{
                                    margin: 0,
                                    fontSize: 22,
                                    textAlign: 'center',
                                    color: '#585858',
                                    marginTop: 10,
                                    marginBottom: 10
                                }}>
                                Edit Profile
                            </BoldText>
                        </View>
                        {props.loading ?
                            <ActivityIndicator
                                style={{zIndex: 2, position: 'absolute', bottom: 0, top: 0, alignSelf: 'center'}}
                                color='#00DBBB' size='large'/> : null
                        }
                        <TouchableOpacity
                            onPress={() => {
                                props.openImagePicker()
                            }}
                            style={{alignItems: 'center', marginTop: '3%'}}>
                            <Image
                                style={{borderRadius: 40, height: 80, width: 80, alignItems: 'center'}}
                                source={props.profile_pic_uri}
                            />
                            <Image style={{marginTop: 15, height: 50, width: 50, position: 'absolute'}}
                                   source={require('../assets/camra.png')}
                                   resizeMode='contain'/>

                        </TouchableOpacity>
                        <BoldText style={{
                            fontWeight: '600',
                            fontSize: 20,
                            marginTop: 5,
                            justifyContent: 'center',
                            marginLeft: '5%',
                            color: '#585858'
                        }}>
                            Name:
                        </BoldText>
                        <View style={{
                            borderRadius: 4,
                            width: '90%',
                            marginTop: '3%',
                            height: 50,
                            marginRight: '5%',
                            marginLeft: '5%',
                            borderColor: 'gray',
                            borderWidth: 2
                        }}>
                            <TextInput
                                style={{
                                    height: 50,
                                    fontWeight: '600',
                                    fontFamily: "NolanNext-Bold",
                                    fontSize: 20,
                                    marginLeft: 5,
                                    color: '#585858'
                                }}
                                maxLength={30}
                                value={props.username}
                                autoCorrect={false}
                                placeholder="Please enter the name"
                                onChangeText={props.onChangeText}
                                placeholderTextColor="gray"
                                underlineColorAndroid='transparent'
                            />
                        </View>
                        <BoldText style={{
                            fontWeight: '600',
                            fontSize: 20,
                            marginTop: 5,
                            justifyContent: 'center',
                            marginLeft: '5%',
                            color: '#585858'
                        }}>
                            Email:
                        </BoldText>
                        <View style={{
                            borderRadius: 4,
                            width: '90%',
                            marginTop: '3%',
                            height: 50,
                            marginRight: '5%',
                            marginLeft: '5%',
                            borderColor: 'gray',
                            borderWidth: 2
                        }}>
                            <TextInput
                                style={{
                                    height: 50,
                                    fontWeight: '600',
                                    fontFamily: "NolanNext-Bold",
                                    fontSize: 20,
                                    marginLeft: 5,
                                    color: '#585858'
                                }}
                                //maxLength={30}
                                value={props.email}
                                autoCorrect={false}
                                keyboardType={"email-address"}
                                placeholder="Please enter the email"
                                onChangeText={props.onChangeTextEmail}
                                placeholderTextColor="gray"
                                underlineColorAndroid='transparent'
                            />
                        </View>
                        <BoldText style={{
                            fontWeight: '600',
                            fontSize: 20,
                            marginTop: '3%',
                            justifyContent: 'center',
                            marginLeft: '5%',
                            color: '#585858'
                        }}>
                            Location:
                        </BoldText>
                        <View style={{
                            borderRadius: 4,
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: '90%',
                            marginTop: '3%',
                            height: 50,
                            marginRight: '5%',
                            marginLeft: '5%',
                            borderColor: 'gray',
                            borderWidth: 2,
                            flexDirection: 'row'
                        }}>
                            <View style={{justifyContent: 'center', width: '65%'}}>
                                {props.address.length > 0 ?
                                    <BoldText style={{ fontSize: 20, color: '#585858' }}>
                                        {props.address}
                                    </BoldText>
                                    :
                                    <BoldText style={{ fontSize: 20, color: 'gray' }}>Click "Find Me"</BoldText>
                                }

                            </View>

                            <View style={{justifyContent: 'center', width: '30%', height: '100%'}}>
                                <TouchableOpacity
                                    onPress={() => {
                                        props.getLocation()
                                    }}
                                    style={{
                                        height: '90%',
                                        marginTop: '3%',
                                        borderRadius: 10,
                                        alignItems: 'center',
                                        backgroundColor: '#00DBBB'
                                    }}>

                                    <View style={{justifyContent: 'center', height: '90%'}}>
                                        <BoldText style={{
                                            color: 'white',
                                            fontSize: 18,
                                            fontWeight: '800',
                                            textAlign: 'center'
                                        }}>Find Me</BoldText>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>

                        
            <TouchableOpacity
                        style={{
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#1FDBB8',
                            
                            marginTop: 20,
                            height: 45,
                            marginLeft: '5%',
                            marginRight:'5%',
                        }}
                        onPress={() => {
                            props.submitProfileDetail()
                        }}>
                        <Text style={{
                            fontSize: 22, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Regular"
                        }}>Done</Text>
        </TouchableOpacity>


                        {/* <View style={{ height: 20 }} /> */}
                        </View>
                    </KeyboardAvoidingView>
                </View>
            </TouchableWithoutFeedback>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        backgroundColor: '#00000040'
    },
    modalContent: {
        ...Platform.select({
            android: {
                top: '5%',
                width: '90%',
                marginLeft: '5%',
                marginRight: '5%',
                // marginTop: '0%',
                // height:'55%',
              //  marginBottom: '10%',
                backgroundColor: 'white',
                borderWidth: 1,
                borderRadius: 4,
                borderColor: 'rgba(0, 0, 0, 0.1)',
            },
            ios: {
                width: '90%',
                marginLeft: '5%',
                marginRight: '5%',
                marginTop: '20%',
                height: '85%',
                backgroundColor: 'white',
                padding: 0,
                borderWidth: 1,
                borderRadius: 4,
                borderColor: 'rgba(0, 0, 0, 0.1)',
            }
        })
    }
});
