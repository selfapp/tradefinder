import React, { Component } from 'react';
import {
    StyleSheet, Text, View, Image, Dimensions, TouchableOpacity, ScrollView, FlatList, TextInput,
    KeyboardAvoidingView
} from 'react-native';
import Button from './button';
import NameTypingIndicator   
    from "../pages/Chat/ce-view-nametypingindicator";
import styles from "../pages/Chat/ce-theme-style";
import ChatEngineProvider from "../pages/Chat/ce-core-chatengineprovider";
import MessageList from "../pages/Chat/ce-view-messagelist";
import MessageEntry from "../pages/Chat/ce-view-messageentry";
const { width, height } = Dimensions.get('window')

export default OffersViewFirstComponent = (props) => {
   
    return (
        <View style={{ height: height / 1.65}}>
            <View>
                <NameTypingIndicator chatRoomModel={ChatEngineProvider.getChatRoomModel()} />
                <View style={{ height: height / 1.9, marginTop: '0%' }}>
                
                <TouchableOpacity
                        style={{
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#14DFC1',
                            marginTop: 10,
                            height: 40,
                            marginLeft: '5%',
                            marginRight:'5%',
                        }}
                        onPress= {() => {(props.isOfferCreated) ? (null):( props.createOffer())}}>
                        <Text style={{
                            fontSize: 16, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Bold"
                        }}>Create Trade</Text>
                </TouchableOpacity>
                
                    <MessageList navigation={props.navigation}
                        now={ChatEngineProvider.getChatRoomModel().state.now}
                        chatRoomModel={ChatEngineProvider.getChatRoomModel()} />
                </View>
              
                <View style={{ marginBottom: 0  }}>
                    <MessageEntry chatRoomModel={ChatEngineProvider.getChatRoomModel()}
                        typingIndicator
                    />
                </View>
            </View>
        </View>
    );
}

const styles1 = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    }
});