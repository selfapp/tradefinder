import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import navigate from './navigate';

export default Header = (props) => {
    return (
        <View style={{ backgroundColor: '#ECECEC' }}>
            <View style={{ flexDirection: 'row', height: 50, marginTop: 10, backgroundColor: '#ECECEC' }}>
                <View style={{ width: '15%',marginTop:5 }} >
                    {props.TradeMatchbackButton || props.TradeRoutebackButton ?
                        (
                            <TouchableOpacity activeOpacity={0.5}
                                onPress={() =>
                                   props.TradeMatchbackButton?
                                    props.getToInitialState()
                                :  props.setTradeRoutebackButton(false)
                                }
                            >
                                <Image
                                    source={require('../assets/back.png')}
                                    style={{ marginLeft: 15, height: 30, width: 18 }}
                                    resizeMode='stretch'
                                />
                            </TouchableOpacity>
                        )
                        : (
                            <TouchableOpacity activeOpacity={0.5}
                                onPress={() => navigate.navigateTo(props.navigation, 'PostItem')
                                }>
                                <Image
                                    source={require('../assets/plus.png')}
                                    style={{ marginLeft: 15, height: 28, width: 28 }}
                                />
                            </TouchableOpacity>
                        )}
                </View>
                <View style={{ width: '70%',marginTop:5  }} >
                    <Image source={require('../assets/tf-logo.png')}
                        style={{ height: 31, width: 31, marginLeft: '44%', marginRight: '44%' }}
                    />
                </View>
                <View style={{ width: '15%',marginTop:5  }}>
                    <TouchableOpacity activeOpacity={0.5}
                        onPress={() => navigate.navigateTo(props.navigation, 'ProfileContainer',{delegate:props._this})}>
                        <Image
                            source={require('../assets/Profile.png')}
                            style={{ marginRight: 15, height: 35, width: 35 }}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}