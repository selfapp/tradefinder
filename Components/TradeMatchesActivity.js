import React, { Component } from 'react';
import {
  StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity, ScrollView, TouchableHighlight, Dimensions,
  PixelRatio, FlatList, Alert
} from 'react-native';
const { width, height } = Dimensions.get('window')
import Grid from './Grid';
import Button from './button';

  export default TradeMatchesActivity = (props) => {
    return (
      <View  style={styles.mainContainer}>
        {props.tradingItems ?
          (
            <Text style={styles.text}>
              Select one of your items to see what other people are willing to offer for your items
      </Text>
          ) :
          (
            <Text style={styles.text}>
              Select an item to see what you can get 
    </Text>
          )
        }
        <ScrollView style={{ width: '97%', marginLeft: '1.5%', marginRight: '1.5%', borderWidth: 1.5, borderRadius: 4, marginBottom: 10 }}>
          <View style={styles.mainContainer} >
            <View>
              <ScrollView horizontal={true} directionalLockEnabled={true}
                style={{ width: '100%' }}>
                {props.TradeDetailArray.map((data, key) =>
                  (
                    <View key={key} style={{ borderColor: 'gray', borderWidth: 1 }}>
                      <View style={{ backgroundColor: data.color, width: '100%', marginLeft: '0%', marginRight: 0, marginTop: 0, marginBottom: 0 }}>
                        <Text style={{ textAlign: 'center', color: '#ffffff', fontWeight: 'bold' }}>{data.title}</Text>
                      </View>
                      <TouchableOpacity>
                        <View style={{
                          margin: 10, paddingLeft: 10, paddingRight: 10, paddingTop: 10,
                          paddingBottom: 10
                        }}>
                          <Image
                            resizeMode='contain'
                            source={data.file} style={{ width: 45, height: 45 }} />
                        </View>
                      </TouchableOpacity>
                    </View>
                  ))
                }
              </ScrollView>
            </View>
            <Grid
              itemsList={props.itemsList}
              passingGridData={(data) =>{
                props.setMyItem(data);
              }}
              mySelectedItem={props.mySelectedItem}
              mySelectedItems={props.mySelectedItems}
              tradingItems={props.tradingItems}
            />


<TouchableOpacity disabled={!props.mySelectedItem}
                        style={{
                            alignItems: 'center',
                            justifyContent:'center',
                            marginTop: 10,
                            marginBottom:20,
                            height: 50,
                            marginLeft: '5%',
                            marginRight:'5%',
                        }}
                        onPress= {
                          props.tradingItems ? () =>
                            props.saveTrade() : () => props.getSearchedResults()}>
                        <Text style={{
                            fontSize: 18, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Bold"
                        }}>{props.tradingItems ? 'Save Trade' : 'Find Matches'}</Text>
                </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }

const styles = StyleSheet.create({
  MainContainer:
    {
      flex: 1,
      alignItems: 'center',
    },
  text: {
    fontSize: 15,
    textAlign: 'center',
    color: 'gray',
    marginTop: 8,
    marginBottom: 10,
    marginLeft: '10%',
    marginRight: '10%',
    fontWeight: 'bold'
  }
});