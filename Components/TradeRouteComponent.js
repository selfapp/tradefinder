import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, FlatList} from 'react-native';
import TriangleRight from './triangle_right';

export default class TradeRouteComponent extends Component {


    _renderChain = (item) => {
        var views = [];
        for (var i = 0; i < item; i++) {
            views.push(
                <TriangleRight color='orange' size={14}/>
            )
        }
        return views;
    };

    render() {
        return (
            <View>
                <View style={{
                    borderColor: 'gray',
                    borderRadius: 2,
                    borderWidth: 1, width: '98%', margin: '1%'
                }}>
                    <Text style={{fontSize: 17, alignSelf: 'center', fontWeight: '700', marginTop: 10}}>
                        My Traded Items
                    </Text>
                    {
                        this.props.tradedArray.length ? (
                            <FlatList
                                data={this.props.tradedArray}
                                renderItem={({item, index}) =>
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => this.props.offerDetailScreen(item, 'item')}
                                            style={{alignItems: 'center'}}>
                                            <View style={{
                                                backgroundColor: "#ffffff",
                                                width: '90%',
                                                borderColor: '#acacac',
                                                borderWidth: 1,
                                                borderRadius: 5,
                                                height: 90,
                                                marginBottom: 30,
                                                marginTop: 10
                                            }}>
                                                <View style={{flexDirection: 'row'}}>
                                                    <View style={{
                                                        flex: 1,
                                                        backgroundColor: 'grey',
                                                        borderBottomLeftRadius: 3,
                                                        borderTopLeftRadius: 3
                                                    }}>
                                                        <Text>Start</Text>
                                                    </View>
                                                    <View style={{
                                                        flex: 3, backgroundColor: 'red',
                                                        borderBottomRightRadius: 3, borderTopRightRadius: 3
                                                    }}>
                                                        <Text>{index + 1} Trade</Text>
                                                    </View>
                                                </View>
                                                <View style={{
                                                    flexDirection: 'row',
                                                    alignItems: 'center',
                                                    justifyContent: 'space-between',
                                                    flex: 1,
                                                    marginHorizontal: 20
                                                }}>
                                                    <Image
                                                        source={{uri: this.props.myItemPic}}
                                                        style={{width: 55, height: 50}}
                                                        resizeMode='contain'
                                                    />
                                                    {
                                                        item.tradematch.length ?
                                                            (
                                                                this._renderChain(item.tradematch[0].length)
                                                            ) :
                                                            (
                                                                <TriangleRight color='orange' size={14}/>
                                                            )
                                                    }
                                                    <Image
                                                        source={{uri: item.item.pictures[0].picture}}
                                                        // source={{ uri: item.item.picture }}
                                                        style={{width: 55, height: 50}}
                                                        resizeMode='contain'
                                                    />
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                }/>
                        ) : (
                            <Text
                                style={{
                                    justifyContent: 'center',
                                    color: '#888888',
                                    fontWeight: 'bold',
                                    fontSize: 20,
                                    alignSelf: 'center',
                                    marginTop: 10,
                                    marginBottom: 10
                                }}
                            >
                                No Items Found
                            </Text>
                        )
                    }
                </View>
                <View style={{
                    borderColor: 'gray', borderWidth: 1, width: '98%',
                    borderRadius: 2,
                    margin: '1%'
                }}>
                    <Text style={{
                        fontSize: 17, fontWeight: '700',
                        alignSelf: 'center', marginTop: 10
                    }}>
                        Other's Traded Items
                    </Text>
                    {
                        this.props.tradeWithArray.length ? (
                            <FlatList
                                data={this.props.tradeWithArray}
                                renderItem={({item, index}) =>
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => this.props.offerDetailScreen(item, 'trade_with')}
                                            style={{alignItems: 'center'}}>
                                            <View style={{
                                                backgroundColor: "#ffffff",
                                                width: '90%',
                                                borderColor: '#acacac',
                                                borderWidth: 1,
                                                borderRadius: 5,
                                                height: 90,
                                                marginBottom: 30,
                                                marginTop: 10
                                            }}>
                                                <View style={{flexDirection: 'row'}}>
                                                    <View style={{
                                                        flex: 1,
                                                        backgroundColor: 'grey',
                                                        borderBottomLeftRadius: 3,
                                                        borderTopLeftRadius: 3
                                                    }}>
                                                        <Text>Start</Text>
                                                    </View>
                                                    <View style={{
                                                        flex: 3, backgroundColor: 'red',
                                                        borderBottomRightRadius: 3, borderTopRightRadius: 3
                                                    }}>
                                                        <Text>{index + 1} Trade</Text>
                                                    </View>
                                                </View>
                                                <View style={{
                                                    flexDirection: 'row',
                                                    alignItems: 'center',
                                                    justifyContent: 'space-between',
                                                    flex: 1,
                                                    marginHorizontal: 20
                                                }}>
                                                    <Image
                                                        source={{uri: this.props.myItemPic}}
                                                        style={{width: 55, height: 50}}
                                                        resizeMode='contain'
                                                    />
                                                    <TriangleRight color='orange' size={14}/>
                                                    <Image
                                                        source={{uri: item.trade_with.pictures[0].picture}}
                                                        style={{width: 55, height: 50}}
                                                        resizeMode='contain'
                                                    />
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                }/>
                        ) : (
                            <Text
                                style={{
                                    alignSelf: 'center',
                                    color: '#888888', fontWeight: 'bold', fontSize: 17,
                                    marginBottom: 10
                                }}
                            >
                                No Items Found
                            </Text>
                        )
                    }
                </View>
            </View>
        );
    }
}
