import React from 'react';
import { Image, Platform } from 'react-native';

export default DefaultImage = (props) => {
    return (
        <Image
                resizeMode='contain'
                style= {props.defaultImageStyle}
                source={require('../assets/no_image.png')} resizeMode='contain'
              />
    )
}