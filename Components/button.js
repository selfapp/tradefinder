import React from 'react';
import { Text, TouchableOpacity, Platform } from 'react-native';

export default Button = (props) => {
    return (
        <TouchableOpacity
        disabled={props.disabled}
            style={{
                alignItems: 'center', width: props.width,
                backgroundColor: props.backgroundColor,
                opacity:props.disabled?0.5:1,
                marginTop: props.marginTop,
                height: props.height,
                marginLeft: props.marginLeft,
                marginRight: props.marginRight,
                marginBottom: props.marginBottom,
                borderColor:props.borderColor,
                borderRadius: 5,
                borderWidth:props.borderWidth,
                borderRadius: 5
            }}
            onPress={props.onPress}>
            <Text style={{
                fontSize: props.fontSize || 18, color: props.color,
                marginTop: props.textMarginTop,
                height:props.height,
                fontWeight: '700',
                fontFamily: "NolanNext-Regular"
            }}>{props.name}</Text>
        </TouchableOpacity>
    )
}