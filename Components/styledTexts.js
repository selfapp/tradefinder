import React, { Component } from 'react';
import { Text} from 'react-native';

class BoldText extends Component {
    render() {
        let {
            style
        } = this.props;
        if(style == null || style === undefined) {
            style = {}
        }
        return (<Text style={[{fontFamily: "NolanNext-Bold"}, style]}>{this.props.children}</Text>);
    }
}

class ItalicText extends Component {
    render() {
        let {
            style
        } = this.props;
        if(style == null || style === undefined) {
            style = {}
        }
        return (<Text style={[{fontFamily: "NolanNext-Italic"}, style]}>{this.props.children}</Text>);
    }
}
class RegularText extends Component {
    render() {
        let {
            style
        } = this.props;
        if(style == null || style === undefined) {
            style = {}
        }
        return (<Text style={[{fontFamily: "NolanNext-Regular",color:'#585858'}, style]} numberOfLines={this.props.numberOfLines?this.props.numberOfLines:null}>{this.props.children}</Text>);
    }
}
module.exports = {
    BoldText,
    ItalicText,
    RegularText
}