import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, FlatList, TextInput} from 'react-native';
import CheckBox from 'react-native-check-box'
import DateTimePicker from 'react-native-modal-datetime-picker';
import MapView from 'react-native-maps';
import moment from 'moment';
import {BoldText, RegularText} from '../Components/styledTexts';
import api from '../api';


export default class OffersViewSecondComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            address: '',
            is_place_with_item: '',
            is_place_item: '',
            key_date: '',
            key_place: '',
            lat: '',
            lng: ''
        }

    }

    UNSAFE_componentWillReceiveProps() {
        this.setData()
    }

    setData = () => {
        if (this.props.myItem == 'me') {
            this.setState({
                is_place_with_item: this.props.accepted_place_with_item,
                is_place_item: this.props.accepted_place_item,
                key_date: 'accepted_date_item',
                key_place: 'accepted_place_item',
                lat: this.props.latitude,
                lng: this.props.longitude
            })
        } else if (this.props.myItem == 'other') {
            this.setState({
                is_place_with_item: this.props.accepted_place_item,
                is_place_item: this.props.accepted_place_with_item,
                key_date: 'accepted_date_with_item',
                key_place: 'accepted_place_with_item',
                lat: this.props.latitude,
                lng: this.props.longitude
            })
        } else {
            this.setState({
                is_place_with_item: false,
                is_place_item: false,
            })
        }
    }

    getLoactionDetails = async () => {
        var _this = this
        let url = 'https://maps.googleapis.com/maps/api/geocode/json?address='
        let response = await api.getLocationRequest(url + this.state.address + '&key=AIzaSyBy4SEL5Madg88ynd8ZbqARpsGMeBTT1As');
        if (response.status === 200) {
            try {
                response.json()
                    .then(function (data) {
                        _this.setState({
                            lat: data['results'][0]['geometry']['location']['lat'],
                            lng: data['results'][0]['geometry']['location']['lng']
                        })

                    })
            } catch (error) {
                alert("There was an error saving your contract.")
            }
        }
    };

    render() {

        return (

            <ScrollView style={styles.container}>
                <View style={{marginTop: 50, flexDirection: 'row'}}>
                    <View style={{width: '80%', marginHorizontal: '10%'}}>
                        <TouchableOpacity style={{
                            borderWidth: 1, borderRadius: 10, borderColor: 'gray',
                            height: 40
                        }}
                                          onPress={this.props._showDateTimePicker}>
                            <View style={{flexDirection: 'row', height: '100%'}}>
                                <View style={{width: '20%'}}>
                                    <Image
                                        source={require('../assets/calendar.png')}
                                        resizeMode='contain'
                                        style={{
                                            position: 'absolute',
                                            top: '10%',
                                            height: 30,
                                            width: 30,
                                            justifyContent: 'center',
                                            marginLeft: '5%',
                                        }}
                                    />
                                </View>
                                <View style={{width: '79%', height: '100%', justifyContent: 'center'}}>
                                    <RegularText style={{
                                        fontSize: 16,
                                        fontWeight: '700'
                                    }}>{moment(this.props.offer_date).format('MMMM  D, YYYY [ - ] h:mm A z')}</RegularText>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <DateTimePicker
                            isVisible={this.props.isDateTimePickerVisible}
                            onConfirm={this.props._handleDatePicked}
                            onCancel={this.props._hideDateTimePicker}
                            minimumDate={new Date(Date.now())}
                        />

                    </View>

                </View>
                <View style={{
                    height: 35, marginLeft: '10%', marginRight: '10%',
                    width: '80%', borderColor: 'gray', borderWidth: 0.5, borderRadius: 5,
                    marginBottom: 10, marginTop: 10
                }}>
                    <TextInput
                        style={{
                            height: 35, marginLeft: '2%', fontFamily: "NolanNext-Regular"
                        }}
                        autoCorrect={false}
                        keyboardType='default'
                        defaultValue={this.props.offer_place}
                        placeholder="Type location here."
                        onChangeText={(address) => this.setState({address})}
                        // onChangeText={(name) => {

                        //   props.getLocationName(name)
                        // }}
                        onSubmitEditing={() => {
                            this.getLoactionDetails()
                        }
                        }

                        placeholderTextColor="#585858" underlineColorAndroid='transparent'/>
                </View>

                <View style={{marginTop: 15, flexDirection: 'row', height: 200}}>
                    <View style={{width: '15%', alignItems: 'center', justifyContent: 'flex-end'}}>
                        <CheckBox
                            checkBoxColor={'red'}
                            style={{}}
                            isChecked={this.state.is_place_with_item}
                            onClick={() => {
                                this.props.getLocationName(this.state.address, this.state.lat, this.state.lng)
                                this.props.updateOffer(this.state.key_place, !this.state.is_place_with_item, this.state.address, this.state.lat, this.state.lng)
                            }
                            }
                            leftText={''}
                            disabled={true}
                        />
                    </View>
                    <View style={{
                        width: '70%',
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                        backgroundColor: 'red'
                    }}>
                        {this.state.lat || this.props.latitude ? (
                            <MapView
                                style={{alignSelf: 'stretch', height: '100%', borderRadius: 5}}
                                region={{
                                    latitude: parseFloat(this.state.lat ? this.state.lat : this.props.latitude),
                                    longitude: parseFloat(this.state.lng ? this.state.lng : this.props.longitude),
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}
                            >
                                <MapView.Marker
                                    coordinate={{
                                        latitude: parseFloat(this.state.lat ? this.state.lat : this.props.latitude),
                                        longitude: parseFloat(this.state.lng ? this.state.lng : this.props.longitude),
                                    }}>
                                    <Image source={require('../assets/star.png')} style={{width: 50, height: 50}}/>
                                </MapView.Marker>
                            </MapView>
                        ) : (<MapView
                            style={{height: '100%', alignSelf: 'stretch'}}
                            region={null}
                        />)
                        }

                    </View>
                    <View style={{width: '15%', alignItems: 'center', justifyContent: 'flex-end'}}>
                        <CheckBox
                            checkBoxColor={'green'}
                            style={{}}
                            onClick={() => {
                                this.props.getLocationName(this.state.address, this.state.lat, this.state.lng)
                                this.props.updateOffer(this.state.key_place, !this.state.is_place_item, this.state.address, this.state.lat, this.state.lng)
                            }
                            }
                            isChecked={this.state.is_place_item}
                            leftText={''}
                        />
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%'
    }
});
