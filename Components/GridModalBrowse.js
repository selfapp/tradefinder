import React, { Component, PropTypes } from 'react';
import {
    StyleSheet, Text, View, ScrollView, Image, TouchableOpacity, AsyncStorage, ActivityIndicator,
    KeyboardAvoidingView, Alert, TextInput, Modal, FlatList,
    TouchableWithoutFeedback, ImageBackground, Platform, Dimensions
} from 'react-native';
import Button from './button';
const { width, height } = Dimensions.get('window')
import {BoldText,RegularText,ItalicText} from '../Components/styledTexts'

    export default GridModalBrowse = (props) => {
        return (
            <Modal
                transparent={true}
                visible={props.gridModalVisible}
                onRequestClose={() => {
                    alert("Modal has been closed.")
                }}>
                <TouchableWithoutFeedback style={{ flex: 1 }} onPress={() => props.setGridModalVisible(false)}>
                    <View style={styles.optionMenuModal}>
                        <View style={styles.optionMenuView}>
                            <View style={{ height: '10%', backgroundColor: '#EBEBEB', alignItems: 'center', width: '100%', marginBottom: 10 }}>
                                <BoldText style={{ color: '#565656', marginTop: '4%', fontSize: 17 }}>Select Item</BoldText>
                            </View>
                            {
                                props.itemList.length ? (
                                    <FlatList
                                        data={props.itemList}
                                        extraData={props.allSelectedItemList}
                                        renderItem={({ item }) =>
                                            <TouchableOpacity
                                                onPress={() => props.selectedGridValue(item.id)}>
                                                {
                                                    (props.allSelectedItemList.indexOf(item.id) >= 0 ? (
                                                        <Image style={{ height: 20, width: 20, position: 'absolute', zIndex: 5, left: 15, top: 5 }}
                                                            resizeMode='contain'
                                                            source={require('../assets/check_circle.png')}
                                                        />
                                                    ) : null)
                                                }
                                                <Image style={styles.ImageComponentStyleGrid}
                                                source={{ uri: item.pictures[0].picture }}
// source={{ uri: item.picture }}
                                                />
                                                <View style={styles.TextComponentStyleGrid}>
                                                    <Text style={styles.ItemTextStyle} numberOfLines={1}>{item.title}</Text>
                                                </View>
                                            </TouchableOpacity>}
                                        numColumns={3}
                                        key={'THREE COLUMN'}
                                    />
                                ) :
                                    (
                                        <View style={{ marginTop: '15%', alignItems: 'center' }} >
                                            <RegularText
                                                style={{
                                                    justifyContent: 'center',
                                                    color: '#888888', fontWeight: 'bold', fontSize: 20,
                                                }}
                                            >
                                                No Items Found
                </RegularText>
                                        </View>
                                    )
                            }
                            {/* <Button name={
                                props.makeOffer? 
                                'Save Trade':'DONE'
                            } 
                            paddingHorizontal={10} fontSize={16}
                                onPress={() => props.done()}
                                marginTop={'10%'}
                                textMarginTop={13}
                                backgroundColor={'#14DFC1'}
                                color={'white'}
                                height={50}
                                marginLeft={'5%'}
                                marginRight={'5%'}
                                marginBottom={'5%'}
                                fontFamily={"NolanNext-Bold"}
                            /> */}

<TouchableOpacity
                        style={{
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#14DFC1',
                            marginTop: 20,
                            marginBottom:10,
                            height: 50,
                            marginLeft: '5%',
                            marginRight:'5%',
                        }}
                        onPress= {() => props.done()}>
                        <Text style={{
                            fontSize: 16, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Bold"
                        }}>{
                            props.makeOffer? 
                            'Save Trade':'DONE'
                        }  </Text>
                </TouchableOpacity>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }

const styles = StyleSheet.create({
    optionMenuModal: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,.6)',
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3
    },
    optionMenuView: {
        backgroundColor: 'green',
        width: '90%',
        height: '80%',
        backgroundColor: '#ffffff',
        borderRadius: 3,
        elevation: 3,
    },
    ImageComponentStyleGrid: {
        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        width: (width / 4),
        height: (width / 4),
        marginTop: '5%',
        left: '5%',
        marginLeft: '4.5%',
        marginRight: '1%',
        marginBottom: '1%'
    },
    TextComponentStyleGrid: {
        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        width: (width / 4),
        height: (width / 10),
        left: '10%',
        marginLeft: '1%',
        marginRight: '3%',
    },
    ItemTextStyle: {
        color: '#222222',
        fontSize: 10,
        textAlign: 'center',
    }
});
