package com.tradefindr;

import com.facebook.react.ReactActivity;
import android.content.Intent;     // <--- import 
import com.pubnub.cennotifications.modules.CENNotifications;


public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */

          @Override
         public void onActivityResult(int requestCode, int resultCode, Intent data) {
             super.onActivityResult(requestCode, resultCode, data);
             MainApplication.getCallbackManager().onActivityResult(requestCode, resultCode, data);
         }
    @Override
    public void onNewIntent(Intent intent) {
        CENNotifications.onNotification(this, intent, null);
    }

    @Override
    protected String getMainComponentName() {
        return "TradeFindr";
    }
}
