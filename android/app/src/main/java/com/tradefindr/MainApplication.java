package com.tradefindr;
import cl.json.ShareApplication;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.facebook.react.ReactApplication;
import com.oblador.vectoricons.VectorIconsPackage;
import com.barefootcoders.android.react.KDSocialShare.KDSocialShare;
import cl.json.RNSharePackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.imagepicker.ImagePickerPackage;
import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.mg.app.PickerPackage;
import org.reactnative.camera.RNCameraPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.pubnub.cennotifications.CENNotificationsPackage;
import java.util.Arrays;
import java.util.List;

import com.facebook.CallbackManager;

public class MainApplication extends Application implements ShareApplication, ReactApplication {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

    protected static CallbackManager getCallbackManager() {
        return mCallbackManager;
    }

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new VectorIconsPackage(),
            new KDSocialShare(),
            new RNSharePackage(),
            new MapsPackage(),
            new ImagePickerPackage(),
            new RNFusedLocationPackage(),
            new PickerPackage(),
            new RNCameraPackage(),
            new CENNotificationsPackage(),
            new FBSDKPackage(mCallbackManager)
            );
        }

        @Override
        protected String getJSMainModuleName() {
            return "index";
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }
    @Override
    public String getFileProviderAuthority() {
        return "com.tradefindr.provider";
    }


    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }
}
