import React, {Component} from 'react';
import {
    Platform
} from 'react-native';
import Login from './pages/Login';
import Verification from './pages/Verification';
import Offers from './pages/Offers/Offers';
import ChatDetail from './pages/Offers/ChatDetail';
import OfferDetails from './pages/Offers/OfferDetails';
import Home from './pages/Home/Home';
import Browse from './pages/Browse/Browse';
import BrowseDetails from './pages/Browse/BrowseDetails';
import PostItem from './pages/Home/PostItem';
import YourItem from './pages/Home/YourItem';
import TradeStep from './pages/Home/TradeStep';
import ProfileContainer from './pages/MyProfile/ProfileContainer';
import PostItemDetails from './pages/Home/PostItemDetails';
import PostItemFinish from './pages/Home/PostItemFinish';
import Category from './pages/Home/Category';
import {TabNavigator, TabBarBottom, TabBarTop, StackNavigator, NavigationActions} from 'react-navigation';
import TradeMatches from './pages/TradeMatches';
import ItemDetails from './pages/ItemDetails';
import TradeMatchesSecond from './pages/TradeMatchesSecond';
import TradeRoutes from './pages/TradeRoutes';
import ProfileNew from './pages/MyProfile/ProfileNew';
import TradeDetails from './pages/TradeDetails';
import TradeRoutesList from './pages/TradeRoutesList'
import ShowQR from './pages/Offers/ShowQR';
import ScanQR from './pages/Offers/ScanQR';
import Trades from './pages/Trades';
let currentIndex;

const BrowseNavigation = StackNavigator({
    Browse: {
        screen: Browse
    },
    BrowseDetails: {
        screen: BrowseDetails,
        navigationOptions: {
            headerMode: 'screen'
        }
    }
});

// Create Bottom Tab
const TradeFindr = TabNavigator(
    {
        Home: {
            screen: ProfileContainer,
        },
        Trades:{
            screen:Trades,
        },
        TradeMatches: {
            screen: TradeMatches,
        },
        TradeRoutes: {
            screen: TradeRoutes,
        },
        BrowseNavigation: {
            screen: BrowseNavigation,
            navigationOptions: {
                header: null
            }
        }
    },
    {
        tabBarComponent: ({jumpToIndex, ...props}) => {
            if (props.navigationState.index === 1) {

            }
            return (
                
                <TabBarBottom
                    {...props}
                    jumpToIndex={index => {
                        if (currentIndex === index && index === 0) {
                            let resetTabAction = NavigationActions.navigate({
                                routeName: "Home",
                                action: NavigationActions.reset({
                                    index: 0,
                                    actions: [NavigationActions.navigate({routeName: "Home"})]
                                })
                            });
                            
                            props.navigation.dispatch(resetTabAction);
                        } else {
                            currentIndex = index;
                            jumpToIndex(index);
                        }
                    }}
                />)
        },
        tabBarOptions: {
            activeTintColor: '#00DBBB',
            inactiveTintColor: 'gray',
            upperCaseLabel: false,
            labelStyle: {
                fontSize: 14,
            },
            style: {
                backgroundColor: '#FFFFFF',
                borderBottomWidth: 1,
                borderBottomColor: '#ededed',
                height: (Platform.OS === 'ios') ? 68 : 64
            },
            indicatorStyle: {
                height: null,
                top: 0,
                backgroundColor: '#00DBBB'
            },
            showIcon: true,
            showLabel: true,
            lazy: true
        },
        tabBarPosition: "bottom",
        // initialRouteName: 'Trades',
        animationEnabled: false,
        swipeEnabled: false
    }
);

export const RootNavigator = (signedIn) => {
    return StackNavigator({
            Login: {
                screen: Login,
                navigationOptions: {
                    header: null
                }
            },
            ProfileContainer: {
                screen: ProfileContainer
            },
            PostItem: {
                screen: PostItem,
                navigationOptions: {
                    headerMode: 'screen'
                }
            },
            PostItemDetails: {
                screen: PostItemDetails,
                navigationOptions: {
                    headerMode: 'screen'
                }
            },
            PostItemFinish: {
                screen: PostItemFinish,
                navigationOptions: {
                    headerMode: 'screen'
                }
            },
            Category: {
                screen: Category,
                navigationOptions: {
                    headerMode: 'screen'
                }
            },
            Verification: {
                screen: Verification,
                navigationOptions: {
                    header: null
                }
            },
            TradeFindr: {
                screen: TradeFindr,

            },
            ProfileNew: {
                screen: ProfileNew,

            },
            Offers: {
                screen: Offers,
            },
            YourItem: {
                screen: YourItem,
                navigationOptions: {
                    headerMode: 'screen'
                }
            },
            TradeMatchesSecond: {
                screen: TradeMatchesSecond,
            },
            TradeStep: {
                screen: TradeStep,
                navigationOptions: {
                    headerMode: 'screen'
                },
            },
            TradeDetails: {
                screen: TradeDetails,
                navigationOptions: {
                    headerMode: 'null'
                },
            },
            TradeRoutesList: {
                screen: TradeRoutesList,
                navigationOptions: {
                    headerMode: 'screen'
                },
            },
            OfferDetails: {
                screen: OfferDetails,
                navigationOptions: {
                    headerMode: 'screen'
                }
            },
            ShowQR: {
                screen: ShowQR,
                navigationOptions: {
                    headerMode: 'screen'
                }
            },
            ScanQR: {
                screen: ScanQR,
                navigationOptions: {
                    headerMode: 'screen'
                }
            },
            ItemDetails: {
                screen: ItemDetails,
                navigationOptions: {
                    headerMode: 'screen'
                }
            },
            ChatDetail: {
                screen: ChatDetail,
                navigationOptions: {
                    headerMode: 'screen'
                }
            },
        },

        {
            initialRouteName: signedIn ? 'TradeFindr' : 'Login',

        }
    );
};
