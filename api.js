import { AsyncStorage } from 'react-native';
var baseURL = 'http://18.236.27.189/api'; // dev
var api = {
    request(url, method, body, header) {
        console.log(baseURL + url);
            return AsyncStorage.getItem('userDetailValues').then((data)=> {
                let userObject = JSON.parse(data);
                return fetch(baseURL + url, {
                    method: method,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer '+ (userObject ? userObject.access_token : null)
                    },
                    body: body === null ? null : JSON.stringify(body)
                })
            });
    },
    getLocationRequest(url, method, header) {
        return fetch(url, {
            method: method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': header
            }
        })
    },
    getRequest(url, method, header) {
        console.log(baseURL + url);
        return AsyncStorage.getItem('userDetailValues').then((data)=> {
            let userObject = JSON.parse(data);
            return fetch(baseURL + url, {
                method: method,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+ (userObject ? userObject.access_token : null)
                },
            })
        });
    },
};
module.exports = api;