import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    TextInput,
    AsyncStorage,
    Platform,
    Alert
} from 'react-native';
import CheckBox from 'react-native-check-box'
import DateTimePicker from 'react-native-modal-datetime-picker';
import MapView from 'react-native-maps';
import moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import OffersViewFirstComponent from '../../Components/OffersViewFirstComponent';
import ChatEngineProvider from "../Chat/ce-core-chatengineprovider";
import ChatEngineProviderMe from "../Chat/ce-core-chatengineproviderme";
import api from '../../api';
import {
    RegularText
} from '../../Components/styledTexts';


export default class OfferDetails extends Component {

    static navigationOptions = ({navigation}) => ({
        title: 'Offer Details',
        tabBarLabel: 'Offer Details',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../../assets/tab2.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => navigation.goBack(null)}
            >
                <Image
                    source={require('../../assets/back.png')}
                    style={{marginLeft: 15, height: 30, width: 18}}
                    resizeMode='stretch'
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <View style={{marginRight: 15, height: 35, width: 35}}></View>
    
            ),
        headerTintColor: '#ffffff',
        headerStyle: {
            backgroundColor: 'rgba(52, 219, 184, 1)',
            borderBottomColor: '#ffffff',
            borderBottomWidth: 0,
        },
        headerTitleStyle: {
            fontWeight: '700',
            fontSize: 20,
            marginTop: 2,
            flex: 1,
            justifyContent: 'center',
            textAlign: 'center',
            alignSelf: 'center',
            color: '#ffffff',
            fontFamily: "NolanNext-Bold",
        }
    });

    _showDateTimePicker = () => this.setState({isDateTimePickerVisible: true});
    _hideDateTimePicker = () => this.setState({isDateTimePickerVisible: false});

    _handleDatePicked = (date) => {
        console.log("Date picked - --- -" + date)
        date = moment(date).format('YYYY-MM-DD HH:mm:ss');
        console.log("date is"+date)

        this.setState({
            offer_date: date,
            accepted_date_with_item: false,
            accepted_date_item: false
        });
        this._hideDateTimePicker();
    };

    constructor(props) {
        super(props);

        this.state = {
            tradedItem: null,
            spinner: false,
            isOfferCreated: false, updatedOfferDetails: '',
            offer_date: '',
            offer_place: '',
            isContain: false,
            offerCreatedBy: 0,
            offerCreatedWith: 0,
            myproduct: '',
            otherItem: '',
            otherItemName: '',
            myItemId: 0,
            withItemId: 0,
            offer: '',
            latitude: '',
            longitude: '',
            accepted_date_item: false,
            accepted_place_item: false,
            accepted_date_with_item: false,
            accepted_place_with_item: false,
            myItem: '', channel_list: [], offerId: 0,
            tradeId: 0,
            is_place_with_item: '',
            is_place_item: '',
            key_date: '',
            key_place: '',
            address: '',
            offer_code: ''
        };
        this.offer_id = 0;
        this._interval = 0;
    }

    createChannelName = () => {
        var _this = this;
        var channel_name = "";
        if (this.state.offerCreatedBy > this.state.offerCreatedWith) {
            channel_name = this.state.offerCreatedWith + "-" + this.state.offerCreatedBy
        } else {
            channel_name = this.state.offerCreatedBy + "-" + this.state.offerCreatedWith
        }
            console.log("Channel Name offer details " + channel_name);
            console.log("Offer created with id", this.state.offerCreatedWith)
            console.log("Offer created by id", this.state.offerCreatedBy)

            ChatEngineProvider.getChatRoomModel()
            .connect(channel_name)
            .then(() => {
              ChatEngineProviderMe.getChatRoomModel()
                .connect('notify' + this.state.offerCreatedWith.toString(), false)
                .then(() => {
                  let chats = [ChatEngineProviderMe._chatRoomModel.state.chat];
                  AsyncStorage.getItem('deviceToken').then((token) => {
                    ChatEngineProviderMe._chatEngine.me.notifications.enable(chats, token, (error) => {
                      console.log("enter into push enable........", error);
                      if (error !== null) {
                        this.setState({spinner: false});
                        Alert.alert(
                          'Push Notification error',
                          `Unable to enable notifications for global: ${error.message}`,
                          [{text: 'OK'}],
                          {cancelable: true}
                        );
                      } else {
                        // Enable message publish button.
                        this.setState({spinner: false});
                        console.log("ENABLED PUSH NOTIFICATION FOR PERSONAL CHANNEL");
                      }
                    });
                  });
                });
            });
        // ChatEngineProvider.getChatRoomModel().connect(channel_name).then(() => {

        //     var chat =  ChatEngineProvider._chatRoomModel.state.chat;
        
        //     var pushToken = ''
        //     AsyncStorage.getItem('deviceToken').then((token)=> {
        //        // console.log("shjkdfkjhsaj")
        //         pushToken = token
        //         console.log("chat sjhfdkhsdfk jsh j")
        //         console.log(chat)
        //         console.log("Device token is "+ pushToken)
        //         let chats = [chat];
    
        //     ChatEngineProvider._chatEngine.me.notifications.enable(chats, pushToken, (error) => {
        //         console.log("enter into push enable........")
        //       if (error !== null) {
        //         Alert.alert(
        //           'Push Notification error',
        //           `Unable to enable notifications for global: ${error.message}`,
        //           [{ text: 'OK' }],
        //           { cancelable: true }
        //         );
        //       } else {
        //         // Enable message publish button.
        //         console.log("enavble push notification........")
        //       }
        //     });
        // })
        //     _this.setState({
        //         spinner: false
        //     });
        // });
    };

    componentDidMount() {
        console.log("offer jsflkjsfksjf ")
        console.log("My Item " + JSON.stringify(this.props.navigation.state.params))
        var _this = this;
        this.setState({spinner: true});
        if (this.props.navigation.state.params.isOfferCreated === true) {
            this.setState({
                isOfferCreated: this.props.navigation.state.params.isOfferCreated,
                offer_code: this.props.navigation.state.params.offerCode,
                offerId: this.props.navigation.state.params.myItem
            });
            setTimeout(function () {
            _this.getNewOffer();
            _this.getOfferDetailValues();
            }, 2000);
            //Rajeev need to chaeck the use of this method
            // _this._interval = setInterval(() => {
            //     _this.getOfferDetailValues()
            // }, 5000);
        } else {
            this.setState({
                offerCreatedBy: this.props.navigation.state.params.myItem.createdBy,
                offerCreatedWith: this.props.navigation.state.params.myItem.createdWith,
                myproduct: this.props.navigation.state.params.myItem.myItem,
                otherItem: this.props.navigation.state.params.myItem.otherItem,
                otherItemName: this.props.navigation.state.params.myItem.otherItemName,
                tradeId: this.props.navigation.state.params.myItem.id,
                myItemId: this.props.navigation.state.params.myItem.trade_item_id,
                withItemId: this.props.navigation.state.params.myItem.withItemId,
            });

            setTimeout(function () {
                _this.createChannelName();
                _this.getOfferDetailValues();
            }, 2000);
        }
    }

    getNewOffer = async () => {

        console.log("getNewOffer....")
        var _this = this;
        let response = await api.getRequest('/' + _this.state.offerId + '/tradeoffer', 'GET');
        if (response.status === 200) {
            await response.json()
                .then(function (data) {
                    _this.setState({
                        offerId: data.offer.id,
                        myproduct: data.offer.trade.item.pictures[0].picture,
                        otherItem: data.offer.trade.trades_with[0].pictures[0].picture,
                        otherItemName: data.offer.trade.trades_with[0].name,
                        offerCreatedBy: data.offer.offer_created_by_user,
                        offerCreatedWith: data.offer.offer_created_with_user,
                        tradeId: data.offer.trade_id,
                        myItemId: data.offer.offer_created_by,
                        withItemId: data.offer.offer_created_with,
                        offer: (data.offer.offer_created_by_user === data.current_user.id) ? 'me' : 'other',
                        latitude: data.offer.latitude,
                        longitude: data.offer.longitude,
                        offer_date: data.offer.offer_date,
                        offer_place: data.offer.offer_place,
                    });

                    setTimeout(function () {
                        _this.setData();
                        _this.createChannelName();
                    }, 2000);
                });
        } else {
            _this.setState({
                spinner: false
            });
        }
    };

    componentWillUnmount() {
        clearInterval(this._interval);
    }

    updateOffer = async (selectedCheckbox, is_checked, address, latValue, lngValue) => {

        console.log("update offer claledd djklj")
        var _this = this;
        _this.setState({spinner: true});
        try {
            let body = {
                "offer": {
                    // "trade_id": _this.state.tradeId,
                    "offer_date": _this.state.offer_date,
                    "offer_place": address,
                    "latitude": parseFloat(latValue),
                    "longitude": parseFloat(lngValue),
                    // "accepted_place_item": (is_changed_place_item) ? is_checked : _this.state.accepted_place_item,
                    // "accepted_place_with_item": (is_changed_place_with_item) ? is_checked : _this.state.accepted_place_with_item
                }
            };
            let response = await api.request('/' + _this.state.offerId + '/tradeoffer', 'PUT', body);
            if (response.status === 200) {
                response.json().then((data) => {
                     console.log("Put API tradeoffer");
                     console.log(data);

                     _this.setState({
                         spinner: false
                         });
                    if (data.offer != undefined && data.offer.status === "COMPLETE") {
                        _this.setState({
                            spinner: false
                            });
                        setTimeout(function () {
                            clearInterval(_this._interval);
                            Alert.alert(
                                'Alert',
                                'Offer is completed',
                                [
                                    {
                                        text: 'OK', onPress: () => {
                                            _this.props.navigation.navigate('TradeFindr')
                                        }
                                    }
                                ],
                                {cancelable: false})


                            // alert('Offer is completed');
                            // _this.props.navigation.navigate('TradeFindr')
                            // navigate.navigateWithReset(_this.props.navigation, 'TradeFindr')
                        }, 500);
                    } else {
                        _this.setState({
                            spinner: false
                            });
                            setTimeout(function () {
                            alert(data.message);
                            },500);
                        _this.setState({
                            isOfferCreated: true,
                            offer_date: data.offer.offer_date,
                            offer_place: data.offer.offer_place,
                            latitude: data.offer.latitude,
                            longitude: data.offer.longitude,
                            // accepted_place_item: data.offer.accepted_place_item,
                            // accepted_place_with_item: data.offer.accepted_place_with_item
                        });
                        _this.offer_id = data.offer.id;
                        _this.setData()
                    }
                });
            } else {
                _this.setState({spinner: false});
            }
        } catch (error) {
            _this.setState({spinner: false});
        }
    };

    createOffer = async () => {

        console.log("Called createOffer ........3")
        var _this = this;
        _this.setState({spinner: true})
        var date = new Date();
        date = moment(date).format('YYYY-MM-DD HH:mm:ss')
        _this.setState({offer_date: date});
        let body = {
            "tradeoffer": {
                "trade_id": this.state.tradeId,
                "offer_date": date,
                "offer_place": this.state.offer_place,
                "latitude": this.state.latitude,
                "longitude": this.state.longitude,
                "accepted_place_item": false,
                "accepted_place_with_item": false,
                "offer_created_by": this.state.myItemId,
                "offer_created_with": this.state.withItemId
            }
        };

        console.log("body of trade offer detail")
        console.log(body)
        try {
            let response = await api.request('/tradeoffer', 'POST', body);
            console.log("trade offer create offer response")
            console.log(response)
            if(response.status === 500){
                _this.createOffer();
            }else{
            if (response.status === 200) {
                response.json().then((data) => {

                    console.log("trade offer create offer data")
                    console.log(data)
                    _this.setState({spinner: false, isOfferCreated: true, offerId: data.offer.id});

                    _this.getOfferDetailValues();
                        //Rajeev need to chaeck the use of this method

                    // _this._interval = setInterval(() => {
                    //     _this.getOfferDetailValues();
                    //     _this.setData()
                    // }, 5000);
                });
            } else {
                _this.setState({spinner: false});
            }
        }
        } catch (error) {
            _this.setState({spinner: false});
        }
    };

    getLocationName = async (locationName, latValue, lngValue) => {
        this.setState({
            offer_place: locationName,
            latitude: latValue,
            longitude: lngValue,
            accepted_place_item: false,
            accepted_place_with_item: false
        });
    };

    getOfferDetailValues = async () => {
        console.log("getOfferDetailValues......")

        var _this = this;
        let response = await api.getRequest('/' + _this.state.offerId + '/tradeoffer', 'GET');
        if (response.status === 200) {

            response.json().then(function (data) {
                    console.log("get Offer details Values data")
                    console.log(data)

                    if (data.offer != undefined && data.offer.status === "COMPLETE") {
                        _this.setState({
                            spinner: false
                        });
                        setTimeout(function () {
                            clearInterval(_this._interval);
                            Alert.alert(
                                'Alert',
                                'Offer is completed',
                                [
                                    {
                                        text: 'OK', onPress: () => {
                                            _this.props.navigation.navigate('TradeFindr')
                                        }
                                    }
                                ],
                                {cancelable: false})
                            // _this.props.navigation.navigate('TradeFindr')

                            // navigate.navigateWithReset(_this.props.navigation, 'TradeFindr')
                        }, 500);
                        // clearInterval(_this._interval);
                        // _this.props.navigation.navigate('TradeFindr');
                    }
                    
                    if (data.offer != null){
                    _this.setState({
                        spinner: false,
                        isOfferCreated: true,
                        offer_place: data.offer.offer_place,
                        latitude: data.offer.latitude,
                        longitude: data.offer.longitude,
                        offer_date: data.offer.offer_date,
                        accepted_place_item: data.offer.accepted_place_item,
                        accepted_place_with_item: data.offer.accepted_place_with_item,
                        offer: (data.offer.offer_created_by_user == data.current_user.id) ? 'me' : 'other',
                        offer_code:data.offer.offer_code
                    });

                    _this.setData()
                    

                    _this.offer_id = data.offer.id;
                    if (_this.props.navigation.state.params.itemSend === 'my_item') {
                        _this.setState({
                            tradedItem: data.offer.trade.item,
                            isContain: true,
                            tradeId: data.offer.trade.id
                        });
                    }
                }

                });
        } else {
            _this.setState({
                spinner: false
            });
        }
    };

    getLoactionDetails = async () => {
        var _this = this;
        let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
        let response = await api.getLocationRequest(url + this.state.address + '&key=AIzaSyBy4SEL5Madg88ynd8ZbqARpsGMeBTT1As');
        if (response.status === 200) {
            try {
                response.json()
                    .then(function (data) {
                        _this.setState({
                            latitude: data['results'][0]['geometry']['location']['lat'],
                            longitude: data['results'][0]['geometry']['location']['lng']
                        });
                    });
            } catch (error) {
                alert("There was an error saving your contract.");
            }
        }
    };

    setData = () => {
        if (this.state.offer === 'me') {
            this.setState({
                is_place_with_item: this.state.accepted_place_with_item,
                is_place_item: this.state.accepted_place_item,
                key_date: 'accepted_date_item',
                key_place: 'accepted_place_item',
            });
        } else if (this.state.offer === 'other') {
            this.setState({
                is_place_with_item: this.state.accepted_place_item,
                is_place_item: this.state.accepted_place_with_item,
                key_date: 'accepted_date_with_item',
                key_place: 'accepted_place_with_item',
            });
        } else {
            this.setState({
                is_place_with_item: false,
                is_place_item: false,
            });
        }
    };

    render() {

        if(this.state.offer === 'other'){
console.log("enter into if ", this.state.offerId)
        }else{
            console.log("enter into else ", this.state.offer_code)
        }
        
        return (
            <View style={styles.container}>
                <View style={{height: '25%', width: '100%'}}>
                    <View style={{height: '100%', width: '100%', flexDirection: 'row',}}>
                        <View style={{width: '45%'}}>
                            <Image
                                resizeMode={"contain"}
                                style={styles.ImageComponentStyle}
                                source={{uri: this.state.myproduct}}
                            />
                        </View>

                        <View style={{width: '10%', justifyContent: 'center'}}>
                            <Image
                                source={require('../../assets/forward.png')}
                                resizeMode='contain'
                                style={{height: 30, width: 30, alignSelf: 'center'}}
                            />
                        </View>
                        <View style={{width: '45%'}}>
                            <Image
                                resizeMode={"contain"}
                                style={styles.ImageComponentStyle}
                                source={{uri: this.state.otherItem}}
                            />
                            <RegularText
                                style={{textAlign: 'center', fontSize: 12}}>{this.state.otherItemName}</RegularText>
                        </View>
                    </View>
                    <View
                        style={{
                            borderBottomColor: '#acacac',
                            borderBottomWidth: 1,
                            marginTop: 20,
                        }}
                    />

                </View>


                <ScrollView
                    contentContainerStyle={{flex: 1, marginTop: '6%'}}
                    showsVerticalScrollIndicator={false}
                    keyboardShouldPersistTaps={'handled'}
                    keyboardDismissMode={'interactive'}
                    onScroll={this.props.handleScroll}
                >
                    <KeyboardAwareScrollView
                        ref='scroll'
                        extraScrollHeight={(Platform.OS === "ios") ? 0 : -50}
                        keyboardOpeningTime={0}
                        keyboardShouldPersistTaps="handled" 
                        enableResetScrollToCoords={true}
                        enableAutomaticScroll={true}
                        enableOnAndroid={true}
                        keyboardVerticalOffset={Platform.select({ ios: 60, android: -170 })}
                    >

                        <View style={{flex: 1}}>
                            {
                                this.state.isOfferCreated ? (
                                    <ScrollView style={{flex: 1, height: '100%'}}>
                                        <View style={{marginTop: 50, flexDirection: 'row'}}>
                                            <View style={{width: '80%', marginHorizontal: '10%'}}>
                                                <TouchableOpacity
                                                    style={{
                                                        borderWidth: 1, borderRadius: 10, borderColor: 'gray',
                                                        height: 40
                                                    }}
                                                    onPress={this._showDateTimePicker}
                                                >
                                                    <View style={{flexDirection: 'row', height: '100%'}}>
                                                        <View style={{width: '20%'}}>
                                                            <Image
                                                                source={require('../../assets/calendar.png')}
                                                                resizeMode='contain'
                                                                style={{
                                                                    position: 'absolute',
                                                                    top: '10%',
                                                                    height: 30,
                                                                    width: 30,
                                                                    justifyContent: 'center',
                                                                    marginLeft: '5%',
                                                                }}
                                                            />
                                                        </View>
                                                        <View
                                                            style={{
                                                                width: '79%',
                                                                height: '100%',
                                                                justifyContent: 'center'
                                                            }}
                                                        >
                                                            <RegularText
                                                                style={{
                                                                    fontSize: 16,
                                                                    fontWeight: '700'
                                                                }}
                                                            >
                                                                {moment(this.state.offer_date).format('MMMM  D, YYYY [ - ] h:mm A z')}
                                                                {/* {moment(this.state.offer_date).format('MMMM  D, YYYY')} */}

                                                            </RegularText>
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                                <DateTimePicker
                                                    isVisible={this.state.isDateTimePickerVisible}
                                                    onConfirm={this._handleDatePicked}
                                                    onCancel={this._hideDateTimePicker}
                                                    datePickerModeAndroid={'spinner'}
                                                    mode={'datetime'}
                                                    is24Hour={false}
                                                   minimumDate={new Date(Date.now())}
                                                />

                                            </View>

                                        </View>
                                        <View
                                            style={{
                                                height: 35, marginLeft: '10%', marginRight: '10%',
                                                width: '80%', borderColor: 'gray', borderWidth: 0.5, borderRadius: 5,
                                                marginBottom: 10, marginTop: 10
                                            }}
                                        >
                                            <TextInput
                                                style={{
                                                    height: 35, marginLeft: '2%', fontFamily: "NolanNext-Regular"
                                                }}
                                                autoCorrect={false}
                                                keyboardType='default'
                                                defaultValue={this.state.offer_place}
                                                placeholder="Type location here."
                                                onChangeText={(address) => this.setState({address})}
                                                onSubmitEditing={() => {
                                                    this.getLoactionDetails()
                                                }}
                                                placeholderTextColor="#585858" underlineColorAndroid='transparent'
                                            />
                                        </View>

                                        <View style={{marginTop: 15, flexDirection: 'row', height: 200, alignItems:'center'}}>
                                            <View style={{
                                                width: '10%',
                                                alignItems: 'center',
                                                justifyContent: 'flex-end'
                                            }}>
                                                
                                            </View>
                                            <View style={{
                                                width: '80%',
                                                alignItems: 'center',
                                                justifyContent: 'flex-end',
                                                backgroundColor: 'red',
                                                justifyContent:'center',
                                                alignItems:'center'
                                            }}>
                                                {this.state.latitude ? (
                                                    <MapView
                                                        style={{alignSelf: 'stretch', height: '100%', borderRadius: 5}}
                                                        region={{
                                                            latitude: parseFloat(this.state.latitude),
                                                            longitude: parseFloat(this.state.longitude),
                                                            latitudeDelta: 0.0922,
                                                            longitudeDelta: 0.0421,
                                                        }}
                                                    >
                                                        <MapView.Marker
                                                            coordinate={{
                                                                latitude: parseFloat(this.state.latitude),
                                                                longitude: parseFloat(this.state.longitude),
                                                            }}>
                                                            <Image source={require('../../assets/star.png')}
                                                                   style={{width: 50, height: 50}}/>
                                                        </MapView.Marker>
                                                    </MapView>
                                                ) : (<MapView
                                                    style={{height: '100%', alignSelf: 'stretch'}}
                                                    region={null}
                                                />)
                                                }

                                            </View>
                                            {/* <View style={{
                                                width: '15%',
                                                alignItems: 'center',
                                                justifyContent: 'flex-end'
                                            }}>
                                               
                                            </View> */}
                                        </View>

            {(this.state.latitude) ? (
                                     <View style={{
                                           height: 50,
                                            marginTop: 10,
                                            flexDirection: 'row',
                                            justifyContent: 'space-around'
                                        }}>
                                        <TouchableOpacity
                                             style={{
                                                    borderRadius: 5,
                                                    borderColor: '#acacac',
                                                    borderWidth: 1,
                                                    justifyContent: 'space-around',
                                                    width: '40%'
                                                }}
                                            onPress={() => {
                                                console.log(this.state, this.state.offer);
                                                        this.getLocationName(this.state.address, this.state.latitude, this.state.longitude)
                                                        this.updateOffer(this.state.key_place, !this.state.is_place_with_item, this.state.address, this.state.latitude, this.state.longitude)
                                                        }}
                                             >
                                                <Text style={{alignSelf: 'center'}}>Update Info.</Text>
                                        </TouchableOpacity>

                                        {(this.state.offer === 'other') ? (
                                                    <TouchableOpacity
                                                          style={{
                                                               borderRadius: 5,
                                                               borderColor: '#acacac',
                                                               borderWidth: 1,
                                                               justifyContent: 'space-around',
                                                                width: '40%'
                                                              }}
                                                            onPress={() => {
                                                                 console.log(this.state, this.state.offer);
                                                                    this.props.navigation.navigate('ScanQR', {offer_id: this.state.offerId});
                                                                 }}
                                                             >
                                                              <Text style={{alignSelf: 'center'}}>Scan QR</Text>
                                                        </TouchableOpacity>
                                         ) :(<TouchableOpacity
                                                style={{
                                                        borderRadius: 5,
                                                        borderColor: '#acacac',
                                                        borderWidth: 1,
                                                        justifyContent: 'space-around',
                                                        width: '40%'
                                                        }}

                                                onPress={() => {
                                                     if (this.state.offer === 'me') {

                                                        if(this.state.offer_code.length > 0){
                                                            this.props.navigation.navigate('ShowQR',{offer_code:this.state.offer_code})
                                                        }else {
                                                            this.getOfferDetailValues()
                                                        }
                                                        
                                                         }
                                                     }}
                                             >
                                             <Text style={{alignSelf: 'center'}}>Show QR</Text>
                                                </TouchableOpacity>)}                                            
                                         </View>

                                    ) : (
                                <View style={{
                                          height: 50,
                                          marginTop: 10,
                                           flexDirection: 'row',
                                           justifyContent: 'space-around'
                                        }}>
                                    <TouchableOpacity
                                       style={{
                                       borderRadius: 5,
                                          borderColor: '#acacac',
                                          borderWidth: 1,
                                           justifyContent: 'space-around',
                                           width: '70%'
                                          }}
                                       onPress={() => {
                                      console.log(this.state, this.state.offer);
                                      this.getLocationName(this.state.address, this.state.latitude, this.state.longitude)
                                                        this.updateOffer(this.state.key_place, !this.state.is_place_with_item, this.state.address, this.state.latitude, this.state.longitude)
                                      }}
                                       >
                                   <Text style={{alignSelf: 'center'}}>Confirm</Text>
                                  </TouchableOpacity>
                                 </View>
                            )}


                                        
                                    </ScrollView>
                                ) : (
<OffersViewFirstComponent
    createOffer={this.createOffer}
    isOfferCreated={this.state.isOfferCreated}
    navigation={this.props.navigation}
/>
                                    
                                )
                            }
                        </View>
                        <Spinner
                            visible={this.state.spinner}
                            textContent={"Loading..."}
                            color='#00DBBB'
                            overlayColor='rgba(0, 0, 0, 0.6)'
                            textStyle={{color: '#00DBBB'}}
                        />
                    </KeyboardAwareScrollView>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    ImageComponentStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '90%',
        height: '90%',
        margin: '2%'
    },
    ItemTextStyle: {
        color: '#222222',
        padding: 0,
        fontSize: 10,
        textAlign: 'left',
        fontWeight: 'bold',
        marginTop: 0,
        marginBottom: 2,
        marginLeft: '12%'
    },
    icon: {
        width: 23,
        height: 23,
    },
});
