import React, {Component} from 'react';
import {
    Text, View, Image, TouchableOpacity,
} from 'react-native';
import QRCode from 'react-native-qrcode';
import {
    BoldText, RegularText
} from '../../Components/styledTexts';

export default class ShowQR extends Component {

    static navigationOptions = ({navigation}) => ({
        title: 'Complete Trade',
        tabBarLabel: 'Offer Details',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../../assets/tab2.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => navigation.goBack(null)}
            >
                <Image
                    source={require('../../assets/back.png')}
                    style={{marginLeft: 15, height: 30, width: 18}}
                    resizeMode='stretch'
                />
            </TouchableOpacity>
        ),

        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 2,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#fff',
                fontFamily: "NolanNext-Bold",
            }
    });


    constructor(props){
        super(props);
    }



    render(){
        console.log("this.props.navigation.state.params.offer_code")
        console.log(this.props.navigation.state.params.offer_code)
        return(
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

                <View style={{ flex:1, justifyContent: 'center', alignItems: 'center' }}>
                    <Image
                        source={require('../../assets/cam-dark.png')}
                        style={{height: 100, width: 100}}
                        resizeMode='contain'/>
                </View>
                <View style={{ flex: 1 }}>
                    <RegularText style={{ fontSize: 15,textAlign: 'center', color: '#585858' }} > {'Scan or take a picture of QR Code to \n complete trade.'} </RegularText>
                </View>
                <View style={{flex: 2 }}>
                    <QRCode
                        value={this.props.navigation.state.params.offer_code}
                        size={200}
                        bgColor='purple'
                        fgColor='white'/>
                </View>
            </View>

        )
    }

}
