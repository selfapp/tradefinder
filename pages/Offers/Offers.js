import React, {Component} from 'react';
import {Icon} from 'native-base';
import {
    StyleSheet, Text, Alert,
    AsyncStorage, View, Image, ImageBackground, TouchableOpacity, ScrollView, TouchableHighlight, FlatList
} from 'react-native';
import api from '../../api';
import Spinner from 'react-native-loading-spinner-overlay';
import navigate from '../../Components/navigate';
import ChatEngineProvider from "../Chat/ce-core-chatengineprovider";
import ChatEngineProviderMe from "../Chat/ce-core-chatengineproviderme";
import DefaultImage from '../../Components/DefaultImage';


export default class Offers extends Component {

    static navigationOptions = ({navigation}) => ({
        headerTitle: 'Offers',
        tabBarLabel: 'Offers',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../../assets/chat.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => navigation.goBack(null)}
            >
                <Image
                    source={require('../../assets/back.png')}
                    style={{marginLeft: 15, height: 30, width: 18}}
                    resizeMode='stretch'
                />
            </TouchableOpacity>
        ),
        headerRight: (
        <View style={{marginRight: 15, height: 35, width: 35}}></View>

            // <TouchableOpacity
            //     onPress={() => navigation.navigate('chatDetails')}
            //     activeOpacity={0.5}>
            //     <Image
            //         source={require('../../assets/chat.png')}
            //         style={{marginRight: 15, height: 35, width: 35}}
            //     />
            // </TouchableOpacity>
        ),
        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 2,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#fff'
            }
    });

    constructor(props) {
        super(props);
        this.state = {
            spinner: false,
            channel_list: [],
            refreshing: false
        }
    }

    componentDidMount() {
        this.setState({
            spinner: true
        })
        this.get_channel_list()
    }

    get_channel_list = async () => {
        var _this = this
        let response = await api.getRequest('/channel', 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    console.log("Channel list " + JSON.stringify(data));
                    _this.setState({
                        spinner: false,
                        channel_list: data.channels,
                        refreshing: false
                    })
                })
        } else {
            _this.setState({
                spinner: false
            })
        }
    };

    goToChatDetailScreen(channel_name,otherUserId, username) {

        var _this = this
        _this.setState({
            spinner: true
        });

        ChatEngineProvider.getChatRoomModel()
        .connect(channel_name)
        .then(() => {
          ChatEngineProviderMe.getChatRoomModel()
            .connect('notify' + otherUserId.toString(), false)
            .then(() => {
              let chats = [ChatEngineProviderMe._chatRoomModel.state.chat];
              console.log("Chat data")
              console.log(chats)
              console.log("username is")
              console.log(username)
              AsyncStorage.getItem('deviceToken').then((token) => {
                ChatEngineProviderMe._chatEngine.me.notifications.enable(chats, token, (error) => {
                  console.log("enter into push enable........", error);
                  if (error !== null) {
                    this.setState({spinner: false});
                    Alert.alert(
                      'Push Notification error',
                      `Unable to enable notifications for global: ${error.message}`,
                      [{text: 'OK'}],
                      {cancelable: true}
                    );
                  } else {
                    this.setState({spinner: false});
                    console.log("ENABLED PUSH NOTIFICATION FOR PERSONAL CHANNEL");
                    // Enable message publish button.
                    setTimeout(function () {
                        _this.setState({spinner: false});
                        AsyncStorage.setItem('otherUserChannelName',JSON.stringify(otherUserId))
                        navigate.navigateTo(_this.props.navigation, 'ChatDetail',{userId: otherUserId, userName:username})
                    }, 100);
                   
                  }
                });
              });
                  
        });
            });
        // });
        
        // ChatEngineProvider.getChatRoomModel().connect(channel_name).then(() => {
        //     // _this.setState({
        //     //     spinner: false
        //     // })
        //     var chat =  ChatEngineProvider._chatRoomModel.state.chat;
        //     console.log("after connection hjj")
        //     console.log(chat)
        //     var pushToken = ''
        //     AsyncStorage.getItem('deviceToken').then((token)=> {
        //        // console.log("shjkdfkjhsaj")
        //         pushToken = token
        //         console.log("chat sjhfdkhsdfk jsh j")
        //         console.log(chat)
        //         console.log("Device token is "+ pushToken)
        //         let chats = [chat];
    
        //     ChatEngineProvider._chatEngine.me.notifications.enable(chats, pushToken, (error) => {
        //         console.log("enter into push enable........")
        //       if (error !== null) {
        //         Alert.alert(
        //           'Push Notification error',
        //           `Unable to enable notifications for global: ${error.message}`,
        //           [{ text: 'OK' }],
        //           { cancelable: true }
        //         );
        //       } else {
        //         // Enable message publish button.
        //         console.log("enable push notification........")
        //       }
        //     });
        // })
        //     setTimeout(function () {
        //         _this.setState({
        //             spinner: false
        //         });
        //         navigate.navigateTo(_this.props.navigation, 'ChatDetail',{userId: id, userName:username})
        //     }, 100);
        // });
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        this.get_channel_list()
    }

    render_channel_list = ({item}) => (

       // console.log("item shhfdsgjsd" + item);
       //item_with_user or item_user
        <View>
            {
                item.item_with_user ? (
                    item.item_with_user.is_blocked ? (null) : (
                        <TouchableOpacity
                                    onPress={() => {
                                        item.item_with_user !== undefined ?
                                        this.goToChatDetailScreen(item.channel_name,item.item_with_user.id, item.item_with_user.first_name)
                                    :(null);
                                }}
                                    style={{
                                        height: 70, marginTop: '2%',
                                        marginLeft: '5%', marginRight: '5%',
                                        borderRadius: 8, borderColor: 'gray', borderWidth: 1,
                                        width: '90%',
                                    }}>
                                    {
                                        item.item_with_user !== undefined ?
                                            (<View style={{flexDirection: 'row'}}>
                                                    <View style={{width: '30%'}}>
                                                        {
                                                            item.item_with_user.picture ?
                                                                (<Image
                                                                    style={{
                                                                        height: 50,
                                                                        width: 50,
                                                                        borderRadius: 25,
                                                                        marginLeft: 15,
                                                                        marginTop: 9
                                                                    }}
                                                                    source={{uri: item.item_with_user.picture}}
                                                                />) : (
                                                                    <DefaultImage
                                                                        defaultImageStyle={{
                                                                            height: 50,
                                                                            width: 50,
                                                                            borderRadius: 25,
                                                                            marginLeft: 15,
                                                                            marginTop: 9
                                                                        }}
                                                                    />
                                                                )
                                                        }
                                                    </View>
                                                    <View style={{width: '70%'}}>
                                                        {
                                                            item.item_with_user.first_name ?
                                                                (<Text
                                                                    style={{marginLeft: '10%', marginTop: 18, fontSize: 17}}
                                                                >
                                                                    {item.item_with_user.first_name}
                                                                </Text>) : (<Text
                                                                    style={{marginLeft: '10%', marginTop: 18, fontSize: 17}}
                                                                >
                                                                    Name Not Given
                                                                </Text>)
                                                        }
                                                        <Text></Text>
                                                    </View>
                                                </View>
                                            ) : (null)
                                    }
                                    </TouchableOpacity>
                ) 
                
                ): (null)
            }
             {
                item.item_user ? (
                    item.item_user.is_blocked ? (null) : (
                        <TouchableOpacity
                                    onPress={() => {
                                        item.item_user !== undefined ?
                                        this.goToChatDetailScreen(item.channel_name,item.item_user.id, item.item_user.first_name)
                                    :(null);
                                }}
                                    style={{
                                        height: 70, marginTop: '2%',
                                        marginLeft: '5%', marginRight: '5%',
                                        borderRadius: 8, borderColor: 'gray', borderWidth: 1,
                                        width: '90%',
                                    }}>
                                    {
                                        item.item_user !== undefined ?
                                            (<View style={{flexDirection: 'row'}}>
                                                    <View style={{width: '30%'}}>
                                                        {
                                                            item.item_user.picture ?
                                                                (<Image
                                                                    style={{
                                                                        height: 50,
                                                                        width: 50,
                                                                        borderRadius: 25,
                                                                        marginLeft: 15,
                                                                        marginTop: 9
                                                                    }}
                                                                    source={{uri: item.item_user.picture}}
                                                                />) : (
                                                                    <DefaultImage
                                                                        defaultImageStyle={{
                                                                            height: 50,
                                                                            width: 50,
                                                                            borderRadius: 25,
                                                                            marginLeft: 15,
                                                                            marginTop: 9
                                                                        }}
                                                                    />
                                                                )
                                                        }
                                                    </View>
                                                    <View style={{width: '70%'}}>
                                                        {
                                                            item.item_user.first_name ?
                                                                (<Text
                                                                    style={{marginLeft: '10%', marginTop: 18, fontSize: 17}}
                                                                >
                                                                    {item.item_user.first_name}
                                                                </Text>) : (<Text
                                                                    style={{marginLeft: '10%', marginTop: 18, fontSize: 17}}
                                                                >
                                                                    Name Not Given
                                                                </Text>)
                                                        }
                                                        <Text></Text>
                                                    </View>
                                                </View>
                                            ) : (null)
                                    }
                                    </TouchableOpacity>
                ) 
                
                ): (null)
            }
        </View>
        )

    render() {

        console.log("this.state.channel_list")
        console.log(this.state.channel_list)
        return (
            <View style={styles.scrollContainer}>
                <FlatList
                    style={{marginTop: '2%'}}
                    removeClippedSubviews={false}
                    data={this.state.channel_list}
                    onRefresh={this._onRefresh}
                    keyExtractor={(item, index) => index.toString()}
                    refreshing={this.state.refreshing}
                    renderItem={this.render_channel_list}
                />
                <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.6)'
                    textStyle={{color: '#00DBBB'}}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: 'white',
        flex: 1
    },
    icon: {
        width: 23,
        height: 23,
    }
});
