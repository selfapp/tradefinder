import React, {Component} from 'react';
import QRCodeScanner from 'react-native-qrcode-scanner';
import API from '../../api';

export default class ScanQR extends Component {
    async onRead(data) {
        try{
        let offer_id = this.props.navigation.state.params.offer_id;
        let response = await API.request(`/${offer_id}/complete-tradeoffer`, 'PUT', {
            offer: {
                "offer_code": data
            }
        });
        if(response.status === 200){
            response.json().then((data) => {
                    if(data.error === false){
                        alert(data.message)
                    }else{
                        alert(data.message)
                    }
            });
        }else{
            console.log("Error in complete offer")
        }
      }catch(e){
        console.log("Error " + e);
     }
    }

    render() {
        return <QRCodeScanner
            onRead={(e) => {
                console.log(JSON.stringify(e.data))
                this.onRead(e.data)}
            }
        />
    }

}
