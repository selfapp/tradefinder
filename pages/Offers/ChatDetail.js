import React, { Component } from 'react';
import {
  StyleSheet, Text, View, Image, TouchableOpacity, AsyncStorage, FlatList, Dimensions, TextInput,
  KeyboardAvoidingView, Alert, Platform
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ChatEngineProvider from "../Chat/ce-core-chatengineprovider";
import NameTypingIndicator
  from "../Chat/ce-view-nametypingindicator";
import MessageList from "../Chat/ce-view-messagelist";
import MessageEntry from "../Chat/ce-view-messageentry";

export default class ChatDetail extends Component {

  componentWillUnmount(){
    console.log("componentWillUnmount called for otherUserChannelName")
    AsyncStorage.setItem('otherUserChannelName','')
  }

  static navigationOptions = ({ navigation }) => ({

    headerTitle: navigation.state.params.userName,
    tabBarLabel: 'Offers',
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require('../../assets/chat.png')}
        style={[styles.icon, { tintColor: tintColor }]}
      />
    ),
    headerLeft: (
      <TouchableOpacity activeOpacity={0.5}
        onPress={() => navigation.goBack(null)}>
        <Image
          source={require('../../assets/back.png')}
          style={{ marginLeft: 15, height: 30, width: 18 }}
          resizeMode='stretch'
        />
      </TouchableOpacity>
    ),
    headerRight: (
      <TouchableOpacity
      onPress={() => {
            navigation.navigate("ProfileNew", {userId: navigation.state.params.userId})
            }}
        // onPress={() => navigation.navigate('ProfileContainer')}
        activeOpacity={0.5}>
        <Image
          source={require('../../assets/Profile.png')}
          style={{ marginRight: 15, height: 35, width: 35 }}
        />
      </TouchableOpacity>
    ),
    headerTintColor: '#ffffff',
    headerStyle: {
      backgroundColor: 'rgba(52, 219, 184, 1)',
      borderBottomColor: '#ffffff',
      borderBottomWidth: 0,
    },
    headerTitleStyle: {
      fontWeight: '700',
      fontSize: 20,
      marginTop: 5,
      flex: 1,
      justifyContent: 'center',
      textAlign: 'center',
      alignSelf: 'center',
      color: '#fff'
    }
  });



  onTypingIndicator() {
    return (<NameTypingIndicator chatRoomModel={ChatEngineProvider.getChatRoomModel()} />);
  }

  render() {
    return (
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior="padding"
        keyboardVerticalOffset={Platform.select({ ios: 60, android: -170 })}
        enabled
      >
        <View style={{ backgroundColor: 'white', flex: 1 }} >
          {this.onTypingIndicator()}
          <MessageList ref="MessageList" navigation={this.props.navigation}
            now={ChatEngineProvider.getChatRoomModel().state.now}
            chatRoomModel={ChatEngineProvider.getChatRoomModel()} />
          <View style={{ marginTop: 0 }}>
            <MessageEntry
              chatRoomModel={ChatEngineProvider.getChatRoomModel()}
              typingIndicator
              keyboardVerticalOffset={80}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  icon: {
    width: 23,
    height: 23,
  },
});
