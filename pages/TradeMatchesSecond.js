import React, {Component} from 'react';
import {
    StyleSheet,
    View, 
    Image, 
    AsyncStorage,
    TouchableOpacity,
    ScrollView,
    Dimensions
} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import navigate from '../Components/navigate';
import api from '../api';
import Spinner from 'react-native-loading-spinner-overlay';
import CustomMarker from '../Components/CustomMarker';
import _ from 'underscore'
import Grid from '../Components/Grid';
import {RegularText} from '../Components/styledTexts'

const width = Dimensions.get('window').width;

export default class TradeMatchesSecond extends Component {

    static navigationOptions = ({navigation}) => ({
        title: 'Trade Matches',
        tabBarLabel: 'Trade Matches',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../assets/tradeMatches.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => navigation.goBack(null)}
            >
                <Image
                    source={require('../assets/back.png')}
                    style={{marginLeft: 15, height: 28, width: 28, resizeMode: 'contain'}}
                />
            </TouchableOpacity>
        ),

        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 2,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#fff',
                fontFamily: "NolanNext-Bold",
            }
    });

    constructor(props) {
        super(props);
        this.state = {
            customStyleIndex: 0,
            spinner: false,
            isSaveTrade: false,
            userId: '',
            tradingItems: false,
            itemDetailValues: '',
            tradeRouteItems: [],
            sliderValue: [0],
            myTradeRouteItem: [], completeTradeData: "",
            mySelectedItem: '',
            selectedItemPicture: props.navigation.state.params.selectedItemPicture,
            selectedItemName: props.navigation.state.params.itemName,
            itemId: props.navigation.state.params.itemId,
            matchedItemlists: [0, 0, 0, 0, 0],
            TradeRoutebackButton: false, saveTradeButton: false,
            showMyTradeRouteItemsDetail: false,
            TradeDetailArray: [{
                title: 'Your Item',
                image: require('../assets/forward.png'),
                file: {uri: props.navigation.state.params.selectedItemPicture},
                color: "#00C0EF"
            },
                {
                    title: 'Trade1',
                    image: require('../assets/forward.png'),
                    file: require('../assets/box.png'),
                    color: "#626464"
                }

            ]
        };
        this.channel_detail = []
        this.onReceived = this.onReceived.bind(this);
    }

    componentDidMount() {
        this.getSearchedResults(this.state.itemId);
        this.getUserDetailfromLocalDatbase();
    }

    onReceived(notification) {
    }

    save_channel = async () => {
        var _this = this;
        let body = {
            "channel": this.channel_detail
        };
        try {
            let response = await api.request('/channel', 'POST', body);
            if (response.status === 200) {
                response.json().then((data) => {
                    _this.setState({
                        spinner: false, isSaveTrade: false
                    })
                    navigate.navigateTo(this.props.navigation, 'TradeFindr', {
                        completeTradeData: this.state.completeTradeData,
                        delegate: this
                    })

                    _this.channel_detail = [];
                });
            } else {
                _this.setState({spinner: false});
            }
        } catch (error) {
            _this.setState({spinner: false});
            error.json().then((err) => {
            });
            _this.setState({spinner: false});
        }
    }

    set_channel_detail = async (other_user_id) => {
        var channel_name = "";
        console.log("current user id" + this.state.userId);
        console.log("other userId " + other_user_id);

        if (this.state.userId < other_user_id) {
            channel_name = this.state.userId + "-" + other_user_id;
        } else {
            channel_name = other_user_id + "-" + this.state.userId;
        }
            console.log("Channel name in match screen " + channel_name)

        this.channel_detail.push({
            channel_name: channel_name,
            item_with_user_id: other_user_id
        })
    }

    isItem = (id) => {
        for (i = 0; i < this.state.TradeDetailArray.length; i++) {
            if (this.state.matchedItemlists[i] === id) {
                // alert("item is already selected")
                return true
            }
        }
        return false
    }


    setMyItem = item => {
        console.log("Set my item " + JSON.stringify(item));
        if (this.isItem(item.id)) {
            alert("item is already selected")
        } else {
            this.props.navigation.navigate("ItemDetails", {item: item, delegate: this, itemId: item.id, myItem: false});
        }
    }

    onSelectedItem = item => {
        console.log("on selected item Trade match second" + JSON.stringify(item))
        const TradeDetailArrayCopy = [...this.state.TradeDetailArray];
        const index = this.state.matchedItemlists.indexOf(0);
        if (this.state.tradingItems) {
            console.log("set channel detail " )
            this.set_channel_detail(item.user.id)
            if (index != -1) {
                const matchedItemlistsCopy = [...this.state.matchedItemlists]
                matchedItemlistsCopy[index] = item.id
                TradeDetailArrayCopy[index + 1].file = {uri: item.pictures[0].picture}
                if((index + 2) < 6){
                TradeDetailArrayCopy[index + 2] = {
                    title: 'Trade' + (index + 2),
                    image: require('../assets/forward.png'),
                    file: require('../assets/box.png'),
                    color: "#626464"
                }
               }
                this.setState({
                    TradeDetailArray: TradeDetailArrayCopy,
                    matchedItemlists: matchedItemlistsCopy,
                    mySelectedItem: item.id,
                    saveTradeButton: true
                })
                this.getSearchedResults(item.id)
            } else {
                alert('You can select only 5 items')
            }
        } else {
            TradeDetailArrayCopy[0].file = {uri: item.pictures[0].picture}
            this.setState({TradeDetailArray: TradeDetailArrayCopy, mySelectedItem: item.id, saveTradeButton: true})

        }


    }

    getToInitialState = () => {
        // this.getmyItemList()
        this.getSearchedResults(this.state.itemId)
        this.setState(
            {
                mySelectedItem: '',
                matchedItemlists: [0, 0, 0, 0, 0],
                TradeDetailArray: [{
                    title: 'Your Item',
                    file: require('../assets/box.png'),
                    color: "#00C0EF"
                },
                    {
                        title: 'Trade1',
                        file: require('../assets/box.png'),
                        color: "#626464"
                    }
                    
                ]
            });
    }


    async getUserDetailfromLocalDatbase() {
        var _this = this
        await AsyncStorage.getItem('userDetailValues').then(data => {
            var detailvalue = JSON.parse(data);
            _this.state.userId = detailvalue.user.id
        });
    }

    async getUserDetail() {
        var _this = this
        let response = await api.getRequest('/users/' + _this.state.userId, 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    if (_this.state.isSaveTrade === true) {
                        _this.setState({
                            tradingItems: false
                        })
                    } else {
                        _this.setState({
                            spinner: false,
                            tradingItems: false
                        })
                    }


                    let userDetail = _.omit(data.user, "item", "trade_chain", "tradeChain")
                    AsyncStorage.setItem('userDetail', JSON.stringify(userDetail));
                    myTradeRouteItem = _.filter(data.user.item, function (trade_item) {
                        return (trade_item.trade.length > 0 || trade_item.trade_with.length > 0);
                    });
                    _this.setState({
                        myTradeRouteItem: myTradeRouteItem
                    })
                })
        } else {
            _this.setState({
                spinner: false
            })
        }
    }

    saveTrade = async () => {
        var _this = this
        let matchedIdList = [];

        if (_this.state.matchedItemlists === [0, 0, 0, 0, 0]) {
            alert('Please select at least one item for trade')
        } else {
            for (var i = 0; i < _this.state.matchedItemlists.length; i++) {
                if (_this.state.matchedItemlists[i] != 0) {
                    matchedIdList.push(_this.state.matchedItemlists[i])
                } else {
                    break;
                }
            }
            let body = {
                "trade_item_id": _this.state.itemId,
                "tradewith": matchedIdList
            }
            this.setState({spinner: true})
            try {
                let response = await api.request('/trade', 'POST', body);
                if (response.status === 200) {
                    response.json().then((data) => {
                        _this.setState({
                            completeTradeData: data, isSaveTrade: true
                        })
                        _this.getToInitialState()
                        _this.getUserDetail()
                        _this.save_channel()
                    });
                } else {
                    _this.setState({spinner: false});
                }
            } catch (error) {
                _this.setState({spinner: false});
            }
        }
    }

    getSearchedResults = async (itemId) => {

        var _this = this
        _this.setState({
            spinner: true
        })
        let body = {
            "item": {
                "item_id": itemId,
                "distance_in_miles": this.state.sliderValue[0],

            }
        }
        try {
            let response = await api.request('/wishlistitems/search', 'POST', body);
            if (response.status === 200) {
                response.json().then((data) => {
                    if (_this.state.isSaveTrade === true) {
                        _this.setState({
                            itemDetailValues: data.items,
                            tradingItems: true
                        })
                    } else {
                        _this.setState({
                            spinner: false,
                            itemDetailValues: data.items,
                            tradingItems: true
                        })
                    }

                });
            } else {
                _this.setState({spinner: false});
            }
        } catch (error) {
            _this.setState({spinner: false});
        }
    }


    getmyItemList = async () => {
        var _this = this
        _this.setState({
            spinner: true
        })
        let response = await api.getRequest('/user/items', 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    _this.setState({
                        itemDetailValues: data.items,
                        tradingItems: false,
                        spinner: false
                    })
                })
        } else {
            _this.setState({
                spinner: false
            })
        }
    }

    handleCustomIndexSelect = (index) => {
        this.setState({
            ...this.state,
            customStyleIndex: index,
        });
    }

    setTradeRoutebackButton = (isShow) => {
        if (isShow) {
            this.setState({
                TradeRoutebackButton: true,
                showMyTradeRouteItemsDetail: true
            })
        } else {
            this.setState({
                TradeRoutebackButton: false,
                showMyTradeRouteItemsDetail: false
            })
        }
    };


    render() {
        return (
            <View style={styles.mainContainer}>
                {!this.state.saveTradeButton ? null : (<Button
                        name={'Save Trade'}
                        onPress={
                            () => this.saveTrade()}
                        disabled={!this.state.mySelectedItem}
                        marginTop={'7%'}
                        marginBottom={10}
                        backgroundColor={'#14DFC1'}
                        color={'white'}
                        textMarginTop={12}
                        fontSize={22}
                        height={50}
                        marginLeft={'10%'}
                        marginRight={'10%'}
                        fontFamily={"NolanNext-Bold"}
                    />
                )}

                <View style={{height: '10%', justifyContent: 'center'}}>
                    {!this.state.saveTradeButton ? (<RegularText style={styles.text}>
                        Select a match to start a trade route
                    </RegularText>) : (<RegularText style={styles.text}>
                            or add another match to your trade route
                        </RegularText>
                    )}


                </View>

                <ScrollView style={{
                    width: '97%',
                    marginLeft: '1.5%',
                    marginRight: '1.5%',
                    borderWidth: 1,
                    borderColor: 'gray',
                    borderRadius: 5,
                    marginBottom: 10,
                    height:  !this.state.saveTradeButton ? '90%' : '75%'
                }}>
                    <View style={styles.mainContainer}>
                        <View style={{ flexDirection: 'row', width:'100%' }}>
                            {/* <ScrollView horizontal={true} directionalLockEnabled={true}
                                        style={{width: '100%'}}> */}
                                {this.state.TradeDetailArray.map((data, key) =>
                                    (
                                        <View key={key} style={{borderColor: 'gray', marginBottom: 10 , borderWidth: 0, width: (Dimensions.get('window').width - 15) / this.state.TradeDetailArray.length}}>
                                            <View style={{
                                                backgroundColor: (key === this.state.TradeDetailArray.length - 1) ? 'red' : data.color,
                                            }}>
                                                <RegularText style={{
                                                    textAlign: 'center',
                                                    color: '#ffffff',
                                                    fontWeight: 'bold'
                                                }}>{data.title}</RegularText>
                                            </View>

                                            <TouchableOpacity style={{ flexDirection:'row', width:'100%', justifyContent:'space-between' }}>
                                                <View style={{
                                                     paddingLeft: 0, paddingRight: 0, paddingTop: 5,
                                                    paddingBottom: 5, flexDirection: 'row', width:'100%', justifyContent:'space-around' ,
                                                    alignItems:'center',
                                                    borderBottomWidth: (key < this.state.TradeDetailArray.length - 1) ? 1 : 0,
                                                    borderRightWidth:(key === this.state.TradeDetailArray.length - 2) ? 1 : 0,
                                                    borderBottomColor: 'gray',
                                                    borderRightColor:'gray'
                                                }}>
                                                    <Image
                                                        resizeMode='contain'
                                                        source={data.file} style={{width: 55, height: 55, }}/>

                                                      {(key < this.state.TradeDetailArray.length - 1) ?
                                                    <Image
                                                        resizeMode='contain'
                                                        source={data.image} style={{
                                                        width: 20,
                                                        height: 20,
                                                        alignSelf: 'center',
                                                    }}/>
                                                    : (null)}  

                                                </View>
                                                
                                            </TouchableOpacity>
                                        </View>
                                    ))
                                }
                            {/* </ScrollView> */}
                        </View>
                        <View style={styles.sliders}>
                            <MultiSlider
                                values={this.state.sliderValue}
                                min={0}
                                max={50}
                                onValuesChange={sliderValue => this.setState({sliderValue})}
                                onValuesChangeFinish={() => this.getSearchedResults(this.state.itemId)}
                                selectedStyle={{
                                    backgroundColor: '#00C0F4',
                                }}
                                unselectedStyle={{
                                    backgroundColor: '#00C0F4',
                                }}
                                containerStyle={{
                                    height: 40,
                                }}
                                trackStyle={{
                                    height: 5,
                                    backgroundColor: 'red',
                                }}
                                touchDimensions={{
                                    height: 40,
                                    width: 40,
                                    borderRadius: 20,
                                    slipDisplacement: 40,
                                }}
                                customMarker={CustomMarker}
                                sliderLength={width - 90}
                            />
                            <RegularText style={{marginLeft: '5%', marginTop: '-2.15%', color: '#585858'}}>
                                {this.state.sliderValue} miles
                            </RegularText>
                        </View>

                        <RegularText style={{
                            alignSelf: 'center',
                            color: '#585858',
                            textAlign: 'center',
                            fontSize: 14,
                            marginBottom: 5
                        }}>
                        {this.state.selectedItemName} - {this.state.itemDetailValues ? this.state.itemDetailValues.length : 0} items found
                        
                        </RegularText>

                        <Grid
                            itemsList={this.state.itemDetailValues ? this.state.itemDetailValues : []}
                            passingGridData={(data) => {
                                this.setMyItem(data);
                            }}
                            tradeMatches={'tradeMatchesSecond'}
                            mySelectedItem={this.state.mySelectedItem}
                            mySelectedItems={this.state.mySelectedItems}
                            // tradingItems={this.state.tradingItems}
                        />

                    </View>
                </ScrollView>


                <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.6)'
                    textStyle={{color: '#00DBBB'}}
                />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#ffffff',
        flex: 1
    },
    icon: {
        resizeMode: 'contain'
    },
    tabStyle: {
        borderColor: '#D52C43'
    },
    activeTabStyle: {
        backgroundColor: '#D52C43',
    },
    tabTextStyle: {
        color: '#D52C43'
    },
    text: {
        fontSize: 15,
        textAlign: 'center',
        color: '#585858',
        marginTop: 8,
        marginBottom: 10,
        marginLeft: '10%',
        marginRight: '10%',
        fontWeight: 'bold'
    },
    sliders: {
        marginTop: 10,
        marginLeft: '5%', marginBottom: 10,
        width: '100%',
        flexDirection: 'row',
        height: 10
    }
});
