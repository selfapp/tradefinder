import React, {Component} from 'react';
import {
    StyleSheet,
      View,
       ScrollView,
        Image,
         TouchableOpacity,
    FlatList,
} from 'react-native';
import StarRating from 'react-native-star-rating';
import api from '../api';
import Spinner from 'react-native-loading-spinner-overlay';
import {
    BoldText, RegularText
} from '../Components/styledTexts'

export default class TradesList extends Component {

    constructor() {
        super();
        this.state = {
            tradeListArray: [],
            spinner: false,
        }
        this._interval = 0;

    }

    componentDidMount() {
        var _this = this;
        _this.setState({
            spinner: true
        })
        _this._interval = setInterval(() => {
            _this.getTradeList();
        }, 5000);
    }
    componentWillUnmount() {
        clearInterval(this._interval);
    }
// Get trade List
    getTradeList = async () => {
        var _this = this
       
        try {
            let response = await api.getRequest('/trade', 'GET');
            if (response.status === 200) {
                response.json()
                    .then(function (data) {
                      //  console.log("trade list are -----")
                       // console.log(data)
                        _this.setState({
                            spinner: false, tradeListArray: data
                        });
                    })
            } else {
                _this.setState({
                    spinner: false
                });
            }
        } catch (error) {
        }
    };


    // UI Desigin
    render() {
        var newTradeArray = this.state.tradeListArray.mytrade
        //  console.log(" this.state.tradeListArray")
       //  console.log(this.state.tradeListArray)

        return (
            <ScrollView ref='scrollView' style={styles.scrollContainer}>
            { this.state.tradeListArray.trade_completed ? 
                <View style={{
                                  height: 80, marginTop: '5%',
                                        marginLeft: '5%', marginRight: '5%',
                                        borderRadius: 8, borderColor: 'gray', borderWidth: 1,
                                        width: '90%',
                                        flexDirection: 'column', 
                                    }}>
                    {/* <View style={{width: '100%', height: '100%', backgroundColor:'red'}}> */}
                             <View style={{height: 20, backgroundColor: 'gray', borderRadius: 6, justifyContent:'center', alignItems:'center', width: '100%'}}>
                                            <RegularText style={{
                                                textAlign: 'center',
                                                fontSize: 13,
                                                fontWeight: '900',
                                                color: "white",
                                            }}>Trades</RegularText>
                             </View>

                                        <View style={{flexDirection: 'row', justifyContent: 'space-between',  alignItems:'center', height:60 }}>
                                            <Image source={ this.state.tradeListArray.trade_completed.picture_self ?  {uri: this.state.tradeListArray.trade_completed.picture_self} : require('../assets/logo.png') }
                                                   style={{
                                                       marginLeft: 10,
                                                       height: 45,
                                                       width: 45,
                                                       marginTop: 5,
                                                       alignSelf: 'center'
                                                   }}
                                            />
                                            <RegularText style={{
                                                textAlign: 'center',
                                                fontSize: 13,
                                                fontWeight: '700',
                                                color: "#585858",
                                            }}> {this.state.tradeListArray.trade_completed.completed}/{this.state.tradeListArray.trade_completed.total_trades} Trades Confirmed</RegularText>

                            <Image source={ this.state.tradeListArray.trade_completed.picture_other ?  {uri: this.state.tradeListArray.trade_completed.picture_other} : require('../assets/logo.png') }
                                                   style={{
                                                       marginRight:10,
                                                       height: 45,
                                                       width: 45,
                                                       marginTop: 5,
                                                       alignSelf: 'center'
                                                   }}
                                            />
                            </View>
                    {/* </View> */}

                </View>
:<View></View> }
                <RegularText style={{textAlign: 'center', color: '#585858', fontWeight: '700', marginTop: 5}}>My Traded
                    Items</RegularText>
                {
                    {newTradeArray} ? (<FlatList
                        data={this.state.tradeListArray.mytrade}
                        renderItem={({item, index}) =>
                            <View>
                                <TouchableOpacity
                                    style={{
                                        height: 80, marginTop: '5%',
                                        marginLeft: '5%', marginRight: '5%',
                                        borderRadius: 8, borderColor: 'gray', borderWidth: 1,
                                        width: '90%',
                                        flexDirection: 'row'
                                    }}
                                    onPress={() => this.props.navigateToTradeStep(this.state.tradeListArray.mytrade[index])}
                                >
                                    <View style={{width: '100%', height: '100%'}}>
                                        <View style={{height: '20%', backgroundColor: 'red', borderRadius: 6}}>
                                            <RegularText style={{
                                                textAlign: 'center',
                                                fontSize: 13,
                                                fontWeight: '900',
                                                color: "white"
                                            }}>{item.item.title}</RegularText>
                                        </View>
                                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                            <Image source={{uri:item.item.pictures[0].picture}}
                                                   style={{
                                                       marginLeft: 20,
                                                       height: 45,
                                                       width: 45,
                                                       marginTop: 5,
                                                       alignSelf: 'center'
                                                   }}
                                            />
                                            <View style={{alignSelf: 'center'}}>
                                                <FlatList
                                                    data={item.trades_with}
                                                    horizontal={true}
                                                    renderItem={({item, index}) =>
                                                        <View style={{justifyContent: 'center', alignSelf: 'center'}}>
                                                            <Image resizeMode={"contain"}
                                                                   source={require("../assets/forward_red.png")}
                                                                   style={{
                                                                       height: 16,
                                                                       width: 16,
                                                                       marginTop: 5,
                                                                       marginLeft: 5
                                                                   }}
                                                            />
                                                        </View>}/>
                                            </View>
                                        {
                                            item.trades_with.length > 0 ? (
                                                <Image
                                                source={{uri: item.trades_with[item.trades_with.length - 1].pictures[0].picture}}
                                                style={{
                                                    marginLeft: 20, height: 45, width: 45, marginTop: 5, marginRight: 20
                                                }}
                                            />
                                            ) : (null)
                                        }
                                           
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        }/>) : (<RegularText style={{textAlign: 'center', fontWeight: '500', marginTop: 5}}>No items
                        found</RegularText>)}


                <RegularText style={{textAlign: 'center', fontWeight: '600', marginTop: 5}}>Other's Traded
                    Items</RegularText>
                {this.state.tradeListArray.offer_with ? (
                    <FlatList
                        data={this.state.tradeListArray.offer_with}
                        renderItem={({item, index}) =>
                            <View>
                                <TouchableOpacity
                                    style={{
                                        height: 80, marginTop: '5%',
                                        marginLeft: '5%', marginRight: '5%',
                                        borderRadius: 8, borderColor: 'gray', borderWidth: 1,
                                        width: '90%',
                                        flexDirection: 'row'
                                    }}
                                    onPress={() =>
                                        this.props.navigateToOfferDetails(this.state.tradeListArray.offer_with[index].id, true)}
                                >
                                    <View style={{width: '100%', height: '100%'}}>
                                        <View style={{height: '20%', backgroundColor: 'red', borderRadius: 6}}>
                                            <BoldText style={{
                                                textAlign: 'center',
                                                fontSize: 11,
                                                color: "white"
                                            }}>{item.trade_withoffer.title}</BoldText>
                                        </View>
                                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                            
                                            
                                            <Image source={{uri: item.trade_withoffer.pictures[0].picture}}
                                                   style={{
                                                       marginLeft: 20,
                                                       height: 45,
                                                       width: 45,
                                                       marginTop: 5,
                                                       alignSelf: 'center'
                                                   }}
                                            />
            {item.status === 'COMPLETE' ? (
                                    <View style={{ height:45, width: 45, marginLeft: 20,marginTop: 5, backgroundColor:'rgba(0,0,0,0.15)',alignItems:'center', justifyContent:'center',position:'absolute'}}>
                                                <Image
                                                    source={require('../assets/tick.png')}
                                                    style={{
                                                        height: 30,
                                                        width: 30,
                                                        alignSelf: 'center',
                                                        // position: 'absolute'
                                                    }}
                                                />
                                                <BoldText style={{color:'white', fontSize:7, textAlign:'center', flex:1, marginLeft:1, marginRight:1}}>completed</BoldText>
                                                </View>
            ) : (null)}



                                            <View style={{alignSelf: 'center'}}>
                                                <FlatList
                                                    data={'1'}
                                                    horizontal={true}
                                                    renderItem={({item, index}) =>
                                                        <View style={{justifyContent: 'center', alignSelf: 'center'}}>
                                                            <Image resizeMode={"contain"}
                                                                   source={require("../assets/forward_red.png")}
                                                                   style={{
                                                                       height: 16,
                                                                       width: 16,
                                                                       marginTop: 5,
                                                                       marginLeft: 5
                                                                   }}
                                                            />
                                                        </View>}/>
                                            </View>

                                            <Image source={{uri: item.itemoffer.pictures[0].picture}}
                                                   style={{
                                                       marginLeft: 20,
                                                       height: 45,
                                                       width: 45,
                                                       marginTop: 5,
                                                       marginRight: 20
                                                   }}
                                            />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        }/>
                ) : (<RegularText style={{textAlign: 'center', fontWeight: '500', marginTop: 5}}>No items
                    found</RegularText>)}


                <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.6)'
                    textStyle={{color: '#00DBBB'}}
                />

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#ffffff'
    }
});
