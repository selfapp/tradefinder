import React, {Component} from "react";
import {
    AsyncStorage,
    StyleSheet,
    Text,
    View,
    Image,
    Platform,
    KeyboardAvoidingView,
    TextInput,
    Alert, TouchableOpacity
} from "react-native";
import Spinner from 'react-native-loading-spinner-overlay';
import ChatEngineProvider from './Chat/ce-core-chatengineprovider';
import ChatEngineProviderMe from './Chat/ce-core-chatengineproviderme';

// import DeviceInfo from 'react-native-device-info';
import navigate from '../Components/navigate';
import Background from '../Components/background';
import api from '../api';

class Verification extends Component {

    constructor(props) {
        super(props);
        this.state = {
            verificationCode: ''
        };

        // Get Device Info
        // this.device_info = {
        //     "device_info": {
        //         "user_id": '',
        //         'unique_id': DeviceInfo.getUniqueID(),
        //         'manufacturer': DeviceInfo.getManufacturer(),
        //         'brand': DeviceInfo.getBrand(),
        //         'model': DeviceInfo.getModel(),
        //         'device_id': DeviceInfo.getDeviceId(),
        //         'system_name': DeviceInfo.getSystemName(),
        //         'system_version': DeviceInfo.getSystemVersion(),
        //         'bundle_id': DeviceInfo.getBundleId(),
        //         'build_number': DeviceInfo.getBuildNumber(),
        //         'version': DeviceInfo.getVersion(),
        //         'readable_version': DeviceInfo.getReadableVersion(),
        //         'device_name': DeviceInfo.getDeviceName(),
        //         'user_agent': DeviceInfo.getUserAgent(),
        //         'device_locale': DeviceInfo.getDeviceLocale(),
        //         'device_country': DeviceInfo.getDeviceCountry(),
        //         'time_zone': DeviceInfo.getTimezone(),
        //         'is_emulator': DeviceInfo.isEmulator(),
        //         'api_level': DeviceInfo.getAPILevel(),
        //         'instance_id': DeviceInfo.getInstanceID(),
        //         'phone_number': DeviceInfo.getPhoneNumber(),
        //         'first_install_time': DeviceInfo.getFirstInstallTime(),
        //         'last_updated_time': DeviceInfo.getLastUpdateTime(),
        //         'serial_number': DeviceInfo.getSerialNumber(),
        //         "device_token": ''
        //     }
        // }

    }

    // phoneNumber = this.props.navigation.state.params.phoneNumber;
    email = this.props.navigation.state.params.email;

    componentDidMount() {
        // Initialize & Register on OneSignal For Push notification in ios app
        // if (Platform.OS === 'ios') {
        //     OneSignal.registerForPushNotifications();
        // }
        // OneSignal.init("a88439d7-5949-4bf1-a81a-cf13892cb9b2");
        // OneSignal.addEventListener('ids', this.onIds.bind(this));
        // OneSignal.configure()
    }


    
    // onIds(device) {
    //     this.device_info.device_info.device_token = device.userId;
    // }

// Update User Deviec token on server for Push notification 
    // async set_device_info(userData) {
    //     var _this = this;
    //     var userId = userData.user.id
    //     _this.device_info.device_info.user_id = userId;

    //     let res = await api.request('/devices', 'post', _this.device_info, null);
    //     if (res.status === 200) {
    //         res.json().then((data) => {
    //         })
    //     } else {
    //         res.json().then((data) => {
    //             _this.setState({loading: false});
    //             alert(JSON.stringify(data));
    //         })
    //     }
    // }

    async resendVerificationCode() {
          console.log("called resendVerificationCode")
          let _this = this;
          _this.setState({loading: true});
          let body = {
              "user": {
                  "email":_this.email,
              }
          };
          try {
              let response = await api.request('/resendCode', 'post', body, null);
             
              _this.setState({loading: false});
              if (response.status === 200) {
                  console.log("enter into 200")
                  try {
                      response.json().then((data) => {
                          console.log("response verification")
                          console.log(data)
                      })
                    }catch (error) {
                    }
                }else {
                    console.log("enter into eroor")
                    await response.json().then((res) => {
                        console.log("repsonse error ")
                        console.log(res)
                            
                            setTimeout(function () {
                                Alert.alert(res.errors)
                                // Alert.alert('Please enter the correct verification code')
                            }, 500);
                        }
                    );
                }
            } catch (error) {
                this.setState({loading: false});
                Alert.alert(error)
            }

    }
    // Verify  OTP Code 
    async onSubmit() {
        console.log("enter into verigifcation")
        let _this = this;
        if (_this.state.verificationCode.length === 6) {
            _this.setState({loading: true});
            let body = {
                "user": {
                    // "country_code": "+1",
                    // "mobile": _this.phoneNumber,
                    "email":_this.email,
                    "otp": _this.state.verificationCode
                }
            };
            try {
                let response = await api.request('/verifyCode', 'post', body, null);
                console.log("repsonse dfjk")
                console.log(response)
                if (response.status === 200) {
                    console.log("enter into 200")
                    try {
                        response.json().then((data) => {

                            console.log("response verification")
                            console.log(data)
                            data.user ? ((data.user.first_name) ? (AsyncStorage.setItem('username', data.user.first_name)) :null) : null
                            data.user ? ((data.user.id) ? (AsyncStorage.setItem('userid', JSON.stringify(data.user.id))) :null) : null

                            var userId = data.user.id
                            // var userName = data.user.first_name
                        // Initialize Chat Engine 
                            // ChatEngineProvider.connect(userId.toString()).then(() => {
                            //     try {
                            //         AsyncStorage.multiSet(
                            //             [['isLoggedIn', 'true'],
                            //                 ['userDetailValues', JSON.stringify(data)],
                            //                 [ChatEngineProvider.ASYNC_STORAGE_USERDATA_KEY, userId.toString()]]
                            //         ).then(() => {
                            //             _this.setState({loading: false});
                            //             navigate.navigateWithReset(_this.props.navigation, 'TradeFindr')
                            //             _this.get_channel_list();
                            //         })
                            //     } catch (error) {
                            //         _this.setState({loading: false});
                            //         alert(error)
                            //     }
                            // });

                            ChatEngineProviderMe.connect(
                                userId,
                              ).then(() => {
                               
                                ChatEngineProvider.connect(
                                  userId,
                                ).then(async () => {
    
                                    console.log('RUNNING ASYNC SIMPLE');
                                    try {                                  
                                      AsyncStorage.multiSet(
                                                    [['isLoggedIn', 'true'],
                                                    ['userDetailValues', JSON.stringify(data)],
                                                    [ChatEngineProvider.ASYNC_STORAGE_USERDATA_KEY, userId.toString()],
                                                    // [ChatEngineProvider.ASYNC_STORAGE_USERDATA_NAME_KEY, userName]
                                                ])
                                                _this.setState({loading: false});
                                                // Navigate to Home Screen
                                     navigate.navigateWithReset(_this.props.navigation, 'TradeFindr')
                                     
                                      AsyncStorage.getItem('deviceToken').then((token) => {
                                       
                                        ChatEngineProviderMe.getChatRoomModel()
                                          .connect("notify" + userId.toString(), true)
                                          .then(() => {
                                            var chat = ChatEngineProviderMe.getChatRoomModel().state.chat;
                                            let chats = [chat];
                                            console.log("chat for enable")
                                            console.log(chats)
                                            ChatEngineProviderMe._chatEngine.me.notifications.enable(chats, token, (error) => {
                                              console.log("enter into push enable........", error);
                                              if (error !== null) {
                                                this.setState({loading: false})
                                                Alert.alert(
                                                  'Push Notification error',
                                                  `Unable to enable notifications for global: ${error.message}`,
                                                  [{text: 'OK'}],
                                                  {cancelable: true}
                                                );
                                              } else {
                                                console.log("ENABLED PUSH NOTIFICATION FOR PERSONAL CHANNEL");
                                              }
                                            });
                                          });
                                      });
                                    } catch
                                      (error) {
                                        this.setState({loader: false})
                                      console.log("error during chat initialise " + error);
                                    }
                                  }
                                );
                              });
                        });
                    } catch (error) {
                    }
                } else {
                    console.log("enter into eroor")
                    await response.json().then((res) => {
                        console.log("repsonse error ")
                        console.log(res)
                            _this.setState({loading: false});
                            setTimeout(function () {
                                Alert.alert(res.errors)
                                // Alert.alert('Please enter the correct verification code')
                            }, 500);
                        }
                    );
                }
            } catch (error) {
                this.setState({loading: false});
                setTimeout(function () {
                    Alert.alert('Please enter the correct verification code')
                }, 500);
            }
        } else {
            Alert.alert('Please enter the correct verification code');
        }
    }

    get_channel_list = async () => {

        var arrChannel = []

        console.log("enter into channel list")
        let response = await api.getRequest('/channel', 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    console.log("channel list are .......")
                    console.log(data.channels)

                    if(data.channels.length > 0) {
                        data.channels.map((item) => {

                            ChatEngineProvider.getChatRoomModel().connect(item.channel_name).then(() => {
                               
                                var chat =  ChatEngineProvider._chatRoomModel.state.chat;
                                console.log("after connection hjj")
                                console.log(chat)
                                var pushToken = ''
                                AsyncStorage.getItem('deviceToken').then((token)=> {
                                   // console.log("shjkdfkjhsaj")
                                    pushToken = token
                                    console.log("chat sjhfdkhsdfk jsh j")
                                    console.log(chat)
                                    console.log("Device token is "+ pushToken)
                                   
                                    let chats = [chat];
                        
                                ChatEngineProvider._chatEngine.me.notifications.enable(chats, pushToken, (error) => {
                                    console.log("enter into push enable........")
                                  if (error !== null) {
                                    Alert.alert(
                                      'Push Notification error',
                                      `Unable to enable notifications for global: ${error.message}`,
                                      [{ text: 'OK' }],
                                      { cancelable: true }
                                    );
                                  } else {
                                    // Enable message publish button.
                                    console.log("enavble push notification........")
                                  }
                                });
                            })
                                
                            });

                        });
                    }
                   
                })
        } else {
           
        }
    };

    // UI Design 
    render() {
        return (
            <Background>
                <KeyboardAvoidingView contentContainerStyle={{alignItems: 'center'}} behavior='position'>
                    <Image
                        style={styles.imageStyle}
                        source={require('../assets/logo.png')} resizeMode='contain'
                    />
                    <Text style={styles.textStyle}>
                        TradeFindr
                    </Text>
                    <View style={{
                        width: '90%', height: 50, backgroundColor: 'white', borderRadius: 5,
                        marginTop: '35%', flexDirection: 'row'
                    }}>
                        <Image
                            style={{marginTop:15, height: 20, width: 28}}
                            // source={require('../assets/phone.png')} resizeMode='contain'
                            source={require('../assets/mail.png')} resizeMode='contain'
                        />
                        <TextInput
                            style={{
                                marginLeft: '8%', height: 40, marginTop: '1.5%',
                                width: '80%'
                            }}
                            keyboardType='numeric'
                            autoCorrect={false}
                            placeholder="Enter Verification Code"
                            onChangeText={(verificationCode) => this.setState({verificationCode})}
                            placeholderTextColor="gray" underlineColorAndroid='transparent'/>
                    </View>
                    <Text style={{marginTop:15, fontWeight:'bold'}} onPress={()=>this.resendVerificationCode()}> 
                        Resend verification code
                    </Text>
                    {/* <Button name={'Submit'} paddingHorizontal={10} fontSize={16}
                            onPress={this.onSubmit.bind(this)}
                            marginTop={30}
                            backgroundColor={'#14DFC1'}
                            color={'white'}
                            width={'100%'}
                            textMarginTop={15}
                            height={50}
                    /> */}
                    <TouchableOpacity
                        style={{
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#14DFC1',
                            marginTop: 30,
                            width:'100%',
                            height: 50,
                        }}
                        onPress= {this.onSubmit.bind(this)}>
                        <Text style={{
                            fontSize: 16, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Regular"
                        }}>Submit</Text>
                </TouchableOpacity>

                </KeyboardAvoidingView>
                <Spinner
                    visible={this.state.loading}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.25)'
                    textStyle={{color: '#00DBBB'}}
                />
            </Background>
        );
    }
}

const styles = StyleSheet.create({
    imageStyle:
        {
            marginTop: '20%'
        },
    textStyle:
        {
            marginTop: '5%',
            fontSize: 18,
            fontWeight: '600',
            color: '#A7A8AA'
        }
});

module.exports = Verification;
