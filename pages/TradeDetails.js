import React, {Component} from 'react';
import {
    StyleSheet, Alert, View, ScrollView, Image, TouchableOpacity,
    FlatList, Text
} from 'react-native';
import StarRating from 'react-native-star-rating';
import api from '../api';
import Spinner from 'react-native-loading-spinner-overlay';
import navigate from '../Components/navigate';
import {RegularText, BoldText} from '../Components/styledTexts'

export default class TradeDetails extends Component {
    static navigationOptions = ({navigation}) => ({
        headerTitle: 'Trade Details',
        tabBarLabel: 'Trade Details',

        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => navigation.goBack(null)}>
                <Image
                    source={require('../assets/back.png')}
                    style={{marginLeft: 15, height: 30, width: 18}}
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity 
            style={{marginRight: 15, height: 40, width: 40}}
                activeOpacity={0.0}>
            </TouchableOpacity>
        ),

        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 5,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#fff',
                fontFamily: "NolanNext-Bold",
            }
    });

    constructor() {
        super();
        this.state = {
            tradeListArray: [],
            spinner: false,
            trade_Width: [],
            offerNullIndex:0,
            isOfferNull:false
        }
    }

    componentDidMount() {
        this.setState({trade_Width: this.props.navigation.state.params.object.trades_with})
    }

    render() {
        var noOfCompleted = 0;
        for (let i = 0; i < this.state.trade_Width.length; i++) {
            let temp = this.state.trade_Width[i].offer
            console.log("temp ......")
            console.log(temp)
            if (temp !== null) {
                console.log("wyuirre ieury")
                if (temp.is_complete === true || temp.status == "COMPLETE") {
                   
                    noOfCompleted = noOfCompleted + 1
                    console.log("No of completed 5555....")
                    console.log(noOfCompleted)
                }
            }
        }

        console.log("No of completed ....")
        console.log(noOfCompleted)
        console.log(JSON.stringify(this.props.navigation.state.params.object))
        return (
            <ScrollView ref='scrollView' style={styles.scrollContainer}>
                {/* <Button
                    name={noOfCompleted + '/' + this.state.trade_Width.length + " " + 'Trades Complete'}
                    // disabled={!this.state.mySelectedItem}
                    marginTop={'7%'}
                    marginBottom={10}
                    backgroundColor={'#14DFC1'}
                    color={'white'}
                    textMarginTop={12}
                    height={50}
                    marginLeft={'5%'}
                    marginRight={'5%'}
                    fontFamily={"NolanNext-Bold"}
                /> */}
                <TouchableOpacity
                        style={{
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#14DFC1',
                            marginTop: 15,
                            marginBottom:10,
                            height: 50,
                            marginLeft: '5%',
                            marginRight:'5%',
                        }}>
                        
                        <Text style={{
                            fontSize: 16, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Bold"
                        }}>{noOfCompleted + '/' + this.state.trade_Width.length + " " + 'Trades Complete'}</Text>
                </TouchableOpacity>

                <View>
                    <View
                        style={{
                            backgroundColor: 'white',
                            width: '90%',
                            height: 130,
                            marginTop: '5%',
                            marginLeft: '5%',
                            marginRight: '10%',
                            borderWidth: 0.5,
                            borderRadius: 8,
                        }}>
                        <View style={{height: '18%', backgroundColor: '#14DFC1', borderRadius: 8, marginTop: 1}}>
                            <BoldText style={{textAlign: 'center', fontSize: 15, color: "white"}}>My Item</BoldText>
                        </View>
                        <View style={{flexDirection: 'row', alignSelf: 'center'}}>
                            <Image source={{uri: this.props.navigation.state.params.object.item.pictures[0].picture}}
                                   style={{height: 100, width: 100, alignSelf: 'center',}}
                                   resizeMode='contain'/>

                            <View style={{width: '40%', alignSelf: 'center', alignItems: 'center'}}>
                                <BoldText
                                    style={{fontSize: 15}}>{this.props.navigation.state.params.object.item.title}</BoldText>
                                <RegularText style={{
                                    marginTop: 20,
                                    fontSize: 15
                                }}>{this.props.navigation.state.params.object.item.user.first_name}</RegularText>
                                <RegularText style={{
                                    marginTop: 5,
                                    fontSize: 9,
                                    color: 'grey'
                                }}>{this.props.navigation.state.params.object.item.user.city}{','}{this.props.navigation.state.params.object.item.user.state}</RegularText>
                                <View style={{width: 80, marginTop: '1%'}}>
                                    <StarRating disabled={true}
                                                emptyStar={'ios-star-outline'}
                                                fullStar={'ios-star'}
                                                halfStar={'ios-star-half'}
                                                iconSet={'Ionicons'}
                                                maxStars={5}
                                                starSize={15}
                                                width={50}
                                                rating={parseFloat(this.props.navigation.state.params.object.item.user.averageRating)}
                                                fullStarColor={'#14DFC1'}/>
                                </View>
                            </View>
                            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 30}}>
                                {this.props.navigation.state.params.object.item.user.picture !== null ? (<Image
                                    source={{uri: this.props.navigation.state.params.object.item.user.picture}}
                                    style={{marginLeft: 10, height: 40, width: 40, borderRadius: 20}}
                                />) : (null)}
                            </View>
                        </View>
                    </View>

                    <Image
                        resizeMode='contain'
                        source={require('../assets/down_green.png')}
                        style={{height: 15, width: 15, marginTop: '1%', alignSelf: 'center', marginBottom: 5}}
                    />

                    <FlatList
                        data={this.props.navigation.state.params.object.trades_with}
                        renderItem={({item, index}) =>
                            <View>
                                <View
                                    style={{
                                        backgroundColor: 'white',
                                        width: '90%',
                                        height: 180,
                                        marginTop: '5%',
                                        marginLeft: '5%',
                                        marginRight: '10%',
                                        borderWidth: 0.5,
                                        borderRadius: 8,
                                    }}
                                >
                                    <View
                                        style={[this.props.navigation.state.params.object.trades_with[index].offer === null || 
                                            this.props.navigation.state.params.object.trades_with[index].offer.is_complete === false ?
                                            (styles.detailsContainerRed) : (styles.detailsContainerGreen)]}>
                                        <BoldText style={{
                                            textAlign: 'center',
                                            fontSize: 15,
                                            color: "white"
                                        }}>Step {index + 1}</BoldText>
                                    </View>

                                    <View style={{flexDirection: 'row', alignSelf: 'center'}}>

                                        <View
                                            style={{height: 90, marginTop: 5, justifyContent: 'center', width: '32%',marginLeft:5}}>
                                            <Image source={{uri: item.pictures[0].picture}}
                                                   style={{height: 90, width: 100, alignSelf: 'center'}}
                                                   resizeMode='contain'/>
                                            { 
                                                
                                                (this.props.navigation.state.params.object.trades_with[index].offer === null || this.props.navigation.state.params.object.trades_with[index].offer.status != "COMPLETE") ?
                                                (null) : (
                                                <View style={{ height:90, backgroundColor:'rgba(0,0,0,0.15)',alignItems:'center', justifyContent:'center',position:'absolute', marginLeft:11}}>
                                                <Image
                                                    source={require('../assets/tick.png')}
                                                    style={{
                                                        marginLeft: 20,
                                                        height: 36,
                                                        width: 36,
                                                        marginTop: 20,
                                                        alignSelf: 'center',
                                                        // position: 'absolute'
                                                    }}
                                                />
                                                <BoldText style={{color:'white', fontSize:12, textAlign:'center', flex:1, marginLeft:5, marginRight:5}}>completed</BoldText>
                                                </View>
                                                )}
                                        </View>

                                        <View style={{width: '45%', alignSelf: 'center', alignItems: 'center'}}>
                                            <BoldText style={{fontSize: 15}}>{item.title}</BoldText>
                                            <RegularText style={{
                                                marginTop: 20,
                                                fontSize: 15
                                            }}>{item.user.first_name}</RegularText>
                                            <RegularText style={{
                                                marginTop: 5,
                                                fontSize: 9,
                                                color: 'grey'
                                            }}> {item.user.city}{','}{item.user.state}</RegularText>

                                            <View style={{width: 80, marginTop: '1%'}}>
                                                <StarRating disabled={true}
                                                            emptyStar={'ios-star-outline'}
                                                            fullStar={'ios-star'}
                                                            halfStar={'ios-star-half'}
                                                            iconSet={'Ionicons'}
                                                            maxStars={5}
                                                            starSize={15}
                                                            width={50}
                                                            rating={parseFloat(item.user.averageRating)}
                                                            fullStarColor={'#14DFC1'}/>
                                            </View>
                                        </View>
                                        <View style={{
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            marginTop: 30,
                                            width: '20%'
                                        }}>
                                            {item.user.picture !== null ? (<Image

                                                source={{uri: item.user.picture}}
                                                style={{marginLeft: 10, height: 40, width: 40, borderRadius: 20}}
                                            />) : (null)}
                                        </View>
                                    </View>
                                    <View style={{width:'100%', marginTop:10, alignItems:'center',justifyContent:'center'}}>
                                        <TouchableOpacity style={{
                                        width: '60%',
                                        height: 30,
                                        borderWidth: 0.5,
                                        borderRadius: 5,
                                        alignItems:'center',
                                        justifyContent:'center'
                                    }}
                                    onPress={() => {
                                        console.log(this.props.navigation.state.params.object.trades_with)
                                        if (this.props.navigation.state.params.object.trades_with[index].offer !== null) {

                                            (navigate.navigateTo(this.props.navigation, "OfferDetails", {
                                                myItem: this.props.navigation.state.params.object.trades_with[index].offer.id,
                                                offerCode: this.props.navigation.state.params.object.trades_with[index].offer.offer_code,
                                                isOfferCreated: true
                                            }));
                                        } else {

                                            if (index > 0) {
                                                if (this.props.navigation.state.params.object.trades_with[index - 1].offer === null) {
                                                    alert('Please complete previous step')
                                                }else {
                                                    let body = {
                                                        'myItem': this.props.navigation.state.params.object.item.pictures[0].picture,
                                                        'otherItem': this.props.navigation.state.params.object.trades_with[index].pictures[0].picture,
                                                        'otherItemName': this.props.navigation.state.params.object.trades_with[index].title,
                                                        'createdBy': this.props.navigation.state.params.object.created_by,
                                                        'createdWith': this.props.navigation.state.params.object.trades_with[index].created_by,
                                                        'id': this.props.navigation.state.params.object.id,
                                                        'trade_item_id': this.props.navigation.state.params.object.trade_item_id,
                                                        'withItemId': this.props.navigation.state.params.object.trade_with_item_id[index]

                                                    };

                                                    (navigate.navigateTo(this.props.navigation, "OfferDetails", {
                                                        myItem: body,
                                                        isOfferCreated: false
                                                    }))
                                                }

                                            } else {


                                                let body = {
                                                    'myItem': this.props.navigation.state.params.object.item.pictures[0].picture,
                                                    'otherItem': this.props.navigation.state.params.object.trades_with[index].pictures[0].picture,
                                                    'otherItemName': this.props.navigation.state.params.object.trades_with[index].title,
                                                    'createdBy': this.props.navigation.state.params.object.item.created_by,
                                                    'createdWith': this.props.navigation.state.params.object.trades_with[index].created_by,
                                                    'id': this.props.navigation.state.params.object.id,
                                                    'trade_item_id': this.props.navigation.state.params.object.trade_item_id,
                                                    'withItemId': this.props.navigation.state.params.object.trade_with_item_id[index]

                                                };

                                                (navigate.navigateTo(this.props.navigation, "OfferDetails", {
                                                    myItem: body,
                                                    isOfferCreated: false
                                                }))
                                            }
                                        }
                                    }
                                    }
                                    >
                                        <Text>View Info.</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <Image
                                    resizeMode='contain'
                                    source={require('../assets/down_grey.png')}
                                    style={{
                                        height: 15,
                                        width: 15,
                                        marginTop: '1%',
                                        alignSelf: 'center',
                                        marginBottom: 5
                                    }}
                                />
                            </View>
                        }/>
                </View>

                <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.6)'
                    textStyle={{color: '#00DBBB'}}
                />

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    detailsContainerRed: {
        height: '18%', backgroundColor: 'red', borderRadius: 8, marginTop: 1

    },
    detailsContainerGreen: {height: '18%', backgroundColor: 'green', borderRadius: 8, marginTop: 1}
});
