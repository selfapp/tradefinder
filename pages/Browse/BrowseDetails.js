import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    TextInput,
    AsyncStorage
} from 'react-native';

const {width, height} = Dimensions.get('window')
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import CustomMarker from '../../Components/CustomMarker';
import Grid from '../../Components/Grid';
import api from '../../api';
import Spinner from 'react-native-loading-spinner-overlay';
import navigate from '../../Components/navigate';
import { Dropdown } from 'react-native-material-dropdown';

import {BoldText, RegularText, ItalicText} from '../../Components/styledTexts'
let data = [{
    value: '1 to 49',
  }, {
    value: '50 to 99',
  }, {
    value: '100 to 199',
  },{
    value: '200 to 299',
  },{
    value: '300 to 449',
  },{
    value: '450 to 599',
  },{
    value: '600 to 799',
  },{
    value: '800 to 999',
  },{
    value: '1000 to 1499',
  },{
    value: '1500 to 1999',
  },{
    value: '2000 to 2999',
  },{
    value: '3000 to 4499',
  },{
    value: '4500 to 5999',
  },{
    value: '6000 to 7999',
  },{
    value: '8000 to 9999',
  },{
    value: '10000 to 14999',
  },{
    value: '15000 +',
  },

];


export default class BrowseDetails extends Component {

    static navigationOptions = ({navigation}) => ({
        title: 'Browse',
        tabBarLabel: 'Browse',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../../assets/search.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() =>{
                                  AsyncStorage.getItem('profileCompleted').then((value) => {
                                      value === '1' ? navigation.navigate('PostItem') : alert('Please create a profile before posting the item.')
                                  });
                              }}
            >
                <Image
                    source={require('../../assets/white_plus_icon.png')}
                    style={{marginLeft: 15, height: 28, width: 28}}
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity
                onPress={() => navigation.navigate('Offers')}
                activeOpacity={0.5}>
                <Image
                    source={require('../../assets/chat.png')}
                    style={{marginRight: 15, height: 35, width: 35}}
                />
            </TouchableOpacity>
        ),
        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 2,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#fff',
                fontFamily: "NolanNext-Bold",
            }
    });

    constructor(props) {
        super(props);
        this.state = {
            selectedCategory: this.props.navigation.state.params.selectedCategory,
            selectedCategoryName: this.props.navigation.state.params.selectedCategory.name,
            selectedCategoryId: this.props.navigation.state.params.selectedCategory.id,
            searchedItemlists: [],
            numberOfItems: '',
            maxValue: '',
            minValue: '',
            sliderValue: [0],
            searchedText: '',
            selectedValue:'',
            userId:''
        }
    }

    componentDidMount() {
        this.getSearchedResult();
        this.getUserDetailValues();
    }
    async getUserDetailValues() {
        await AsyncStorage.getItem('userDetailValues').then(data => {
            var detailvalue = JSON.parse(data);
             this.setState({userId:detailvalue.user.id})
        });
    }

    passingGridData = async (items) => {
        let itemId = items.id
        navigate.navigateTo(this.props.navigation, 'YourItem', {itemId: itemId, myItem: (this.state.userId === items.created_by) ? true : false})
    }


    async getSearchedResult() {
        var _this = this
        let body = {
            "item": {
                "name": _this.state.searchedText,
                "min_value": _this.state.minValue,
                "max_value": _this.state.maxValue,
                "distance_in_miles": _this.state.sliderValue[0],
                "category_id": this.state.selectedCategoryId
            }
        }
        _this.setState({
            spinner: true
        })
        try {
            let response = await api.request('/items/search', 'POST', body);
            if (response.status === 200) {
                response.json().then((data) => {
                    _this.setState({
                        spinner: false,
                        searchedItemlists: data.items,
                        numberOfItems: data.count
                    })
                });
            } else {
                _this.setState({spinner: false});
                await response.json().then((res) => {
                    }
                );
            }
        } catch (error) {
            _this.setState({spinner: false});
            error.json().then((err) => {
            });
            _this.setState({spinner: false});
        }
    }

    render() {
        return (
            <ScrollView ref='scrollView' style={styles.scrollContainer}>
                <View>
                    <View>
                        <Text style={{
                            color: '#606060',
                            marginTop: 15,
                            textAlign: 'center',
                            fontSize: 22,
                            fontWeight: '600'
                        }}>{this.state.selectedCategoryName}</Text>
                    </View>
                    <View style={{
                        margin: 10,
                        height: 40,
                        width: '95%',
                        marginLeft: '2.5%',
                        flexDirection: 'row',
                        marginRight: '2.5%',
                        borderRadius: 3,
                        borderWidth: 1,
                        borderColor: 'gray'
                    }}>
                        <TextInput style={{marginLeft: '2%', height: 40, width: '85%', fontFamily: "NolanNext-Regular"}}
                                   autoCorrect={false}
                                   keyboardType='web-search'
                                   onSubmitEditing={this.getSearchedResult.bind(this)}
                                   placeholder="Search" onChangeText={(searchedText) => this.setState({searchedText})}
                                   placeholderTextColor="gray" underlineColorAndroid='transparent'/>
                        <TouchableOpacity onPress={this.getSearchedResult.bind(this)}
                                          style={{marginTop: '2%'}}>
                            <Image source={require('../../assets/search_left.png')}
                                   style={{height: 22, width: 22, tintColor: 'gray'}}/>
                        </TouchableOpacity>

                    </View>

                    <View style={styles.sliders}>
                        <MultiSlider selectedStyle={{backgroundColor: '#00C0F4',}}
                                     unselectedStyle={{backgroundColor: '#00C0F4',}}
                                     values={this.state.sliderValue}
                                     min={0}
                                     max={100}
                                     onValuesChange={sliderValue => this.setState({sliderValue})}

                                     onValuesChangeFinish={this.getSearchedResult.bind(this)}
                                     containerStyle={{height: 40}}
                                     trackStyle={{height: 5, backgroundColor: 'red',}}
                                     touchDimensions={{height: 40, width: 40, borderRadius: 20, slipDisplacement: 40,}}
                                     customMarker={CustomMarker}
                                     sliderLength={245}
                        />
                        <Text style={{
                            marginLeft: 10,
                            marginTop: '-2.15%',
                            color: 'gray'
                        }}> {this.state.sliderValue} miles</Text>
                    </View>

                    <View style={{ width:'90%',marginLeft:'5%'}}>
                    <Dropdown
                        label='Price Range($)'
                         data={data}
                         onChangeText={(selectedValue) => {
                             var words = selectedValue.split('to')
                             if (words.length === 1){
                                words = selectedValue.split('+');
                                this.setState({selectedValue:selectedValue,minValue:words[0].trim(), maxValue:'99999'})
                             }else{
                            this.setState({selectedValue:selectedValue,minValue:words[0].trim(), maxValue:words[words.length - 1].trim()})
                             }
                            console.log(this.state.minValue + '   ' + this.state.maxValue) 
                            this.getSearchedResult();
                        }}

                         />

                </View>


                    <View style={{marginTop: 15, marginBottom: 10, flexDirection: 'row'}}>
                        <Text style={{
                            marginLeft: '2%',
                            color: 'gray',
                            marginTop: '2%'
                        }}> {this.state.numberOfItems} items</Text>
                        {/* <Text style={{marginLeft: '34%', color: 'gray', marginTop: '2%'}}> Value </Text>
                        <TextInput
                            style={{
                                marginLeft: '1%',
                                height: 36,
                                width: '12%',
                                borderColor: 'gray',
                                borderWidth: 1,
                                borderRadius: 5,
                                fontSize: 13,
                                fontFamily: "NolanNext-Regular"
                            }}
                            autoCorrect={false}
                            returnKeyType='done'
                            keyboardType='numeric'
                            placeholder="$Min" onChangeText={(minValue) => this.setState({minValue})}
                            placeholderTextColor="gray"
                            underlineColorAndroid='transparent'
                        />
                        <Text style={{marginLeft: '1%', color: 'gray', marginTop: '2%'}}> To</Text>
                        <TextInput
                            style={{
                                marginLeft: '1%',
                                height: 36,
                                width: '12%',
                                borderColor: 'gray',
                                borderWidth: 1,
                                borderRadius: 5,
                                fontSize: 13,
                                fontFamily: "NolanNext-Regular"
                            }}
                            autoCorrect={false}
                            keyboardType='numeric'
                            returnKeyType='done'
                            placeholder="$Max" onChangeText={(maxValue) => this.setState({maxValue})}
                            placeholderTextColor="gray"
                            underlineColorAndroid='transparent'
                        /> */}
                    </View>
                    <Grid
                        itemsList={this.state.searchedItemlists ? this.state.searchedItemlists : []}
                        passingGridData={this.passingGridData}
                    />
                </View>
                <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.6)'
                    textStyle={{color: '#00DBBB'}}
                />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#ffffff'
    },
    container: {
        flex: 1,
        width: null,
        height: null,
    },
    sliders: {
        marginTop: 20,
        marginLeft: '5%',
        width: '100%',
        flexDirection: 'row',
        height: 10
    },
});
