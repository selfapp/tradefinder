import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Dimensions,
    TextInput,
    Alert,
    ActivityIndicator,
    AsyncStorage
} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import Spinner from 'react-native-loading-spinner-overlay';
import CustomMarker from '../../Components/CustomMarker';
import BrowseDetails from './BrowseDetails';
import Grid from '../../Components/Grid';
import navigate from '../../Components/navigate';
import api from '../../api';
import {RegularText, ItalicText} from '../../Components/styledTexts'
import { Dropdown } from 'react-native-material-dropdown';

const {width, height} = Dimensions.get('window');
var profileUpdated = false;
let data = [{
    value: '1 to 49',
  }, {
    value: '50 to 99',
  }, {
    value: '100 to 199',
  },{
    value: '200 to 299',
  },{
    value: '300 to 449',
  },{
    value: '450 to 599',
  },{
    value: '600 to 799',
  },{
    value: '800 to 999',
  },{
    value: '1000 to 1499',
  },{
    value: '1500 to 1999',
  },{
    value: '2000 to 2999',
  },{
    value: '3000 to 4499',
  },{
    value: '4500 to 5999',
  },{
    value: '6000 to 7999',
  },{
    value: '8000 to 9999',
  },{
    value: '10000 to 14999',
  },{
    value: '15000 +',
  },

];

export default class Browse extends Component {

    static navigationOptions = ({navigation}) => ({
            title: 'Browse',
            tabBarLabel: 'Browse',
            tabBarIcon: ({tintColor}) => (
                <Image
                    source={require('../../assets/search.png')}
                    style={[styles.icon, {tintColor: tintColor}]}
                />
            ),
            headerLeft: (
                <TouchableOpacity activeOpacity={0.5}
                                  onPress={() =>{
                                      AsyncStorage.getItem('profileCompleted').then((value) => {
                                          value === '1' ? navigation.navigate('PostItem') : alert('Please create a profile before posting the item.')
                                      });
                                  }}
                >
                    <Image
                        source={require('../../assets/white_plus_icon.png')}
                        style={{marginLeft: 15, height: 28, width: 28}}
                    />
                </TouchableOpacity>
            ),
            headerRight: (
                <TouchableOpacity
                    onPress={() => navigation.navigate('Offers')}
                    activeOpacity={0.5}>
                    <Image
                        source={require('../../assets/chat.png')}
                        style={{marginRight: 15, height: 35, width: 35}}
                    />
                </TouchableOpacity>
            ),
            headerTintColor: '#ffffff'
            ,
            headerStyle:
                {
                    backgroundColor: 'rgba(52, 219, 184, 1)',
                    borderBottomColor: '#ffffff',
                    borderBottomWidth: 0,
                }
            ,
            headerTitleStyle:
                {
                    fontWeight: '700',
                    fontSize: 20,
                    marginTop: 2,
                    flex: 1,
                    justifyContent: 'center',
                    textAlign: 'center',
                    alignSelf: 'center',
                    color: '#fff',
                    fontFamily: "NolanNext-Bold",

                }
        }
    )
    ;

    constructor(props) {
        super(props);
        this.state = {
            spinner: false,
            maxValue: '',
            category_id: '',
            minValue: '',
            numberOfItems: '',
            sliderValue: [0],
            searchedText: '',
            searchedItemlists: [],
            categoryListArray: [],
            selectedValue:'',
            userId:''
        }
    }

    componentDidMount() {
        this.getSearchedResults()
        this.getCategories()
        this.getUserDetailValues()
    }

    async getUserDetailValues() {
        await AsyncStorage.getItem('userDetailValues').then(data => {
            var detailvalue = JSON.parse(data);
            profileUpdated = detailvalue.user.latitude !== null;
             this.setState({userId:detailvalue.user.id})
        });
    }

    async getCategories() {
        var _this = this
        let response = await api.getRequest('/categories', 'GET',);
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    _this.setState({
                        spinner: false,
                        categoryListArray: data.categories
                    });
                    _this.forceUpdate()
                })
        } else {
            _this.setState({
                spinner: false
            });
        }
    }

    getSearchedResults = async () => {
        var _this = this
        _this.setState({
            spinner: true
        });
        let body = {
            "item": {
                "name": _this.state.searchedText,
                "min_value": _this.state.minValue,
                "max_value": _this.state.maxValue,
                "category_id": _this.state.category_id,
                "distance_in_miles": _this.state.sliderValue[0],
            }
        };
        try {
            let response = await api.request('/items/search', 'POST', body);
            if (response.status === 200) {
                response.json().then((data) => {
                    if (data.message) {
                        _this.setState({
                            spinner: false
                        });
                        
                    } else {
                        _this.setState({
                            searchedItemlists: data.items,
                            numberOfItems: data.count,
                            spinner: false
                        });
                        // console.log(JSON.stringify(data))
                    }
                });
            } else {
                _this.setState({spinner: false});
            }
        } catch (error) {
            _this.setState({spinner: false});
        }
    }
    passingGridData = async (items) => {
        let itemId = items.id
        navigate.navigateTo(this.props.navigation, 'YourItem', {itemId: itemId, myItem: (this.state.userId === items.created_by) ? true : false})
    }

    _renderItem = ({item}) => (
        <TouchableOpacity
            onPress={() => navigate.navigateTo(this.props.navigation, 'BrowseDetails', {selectedCategory: item})}
            style={{
                borderColor: '#00C1ED',
                borderWidth: 1,
                borderRadius: 5,
                justifyContent: 'center',
                marginLeft: 5,
                marginRight: 5,
                marginBottom: 5,
                height: 30
            }}>
            {/* <Image source={{ uri: item.icon }}
        style={{
          height: 20, width: 20,
          marginTop: 2
        }}
        resizeMode='contain'
      /> */}
            <RegularText style={{
                fontSize: 14,
                color: '#00C1ED',
                alignSelf: 'center',
                marginLeft: 5,
                marginRight: 5
            }}> {item.name}</RegularText>
        </TouchableOpacity>
    );

    render() {
        
        return (
            <ScrollView style={styles.container}>
                <View style={{
                    margin: 10,
                    height: 40,
                    width: '95%',
                    marginLeft: '2.5%',
                    flexDirection: 'row',
                    marginRight: '2.5%',
                    borderRadius: 3,
                    borderWidth: 1,
                    borderColor: 'gray'
                }}>
                    <TextInput style={{marginLeft: '2%', height: 40, width: '85%', fontFamily: "NolanNext-Regular"}}
                               autoCorrect={false}
                               keyboardType='web-search'
                               onSubmitEditing={this.getSearchedResults.bind(this)}
                               placeholder="Search" onChangeText={(searchedText) => this.setState({searchedText})}
                               placeholderTextColor="gray" underlineColorAndroid='transparent'/>
                    <TouchableOpacity onPress={this.getSearchedResults.bind(this)}
                                      style={{marginTop: '2%'}}>
                        <Image source={require('../../assets/search_left.png')}
                               style={{height: 22, width: 22, tintColor: 'gray'}}/>
                    </TouchableOpacity>

                </View>
                <View style={styles.sliders}>
                    <MultiSlider values={this.state.sliderValue}
                                 min={0}
                                 max={100}
                                 onValuesChange={sliderValue => this.setState({sliderValue})}
                                 onValuesChangeFinish={this.getSearchedResults.bind(this)}
                                 selectedStyle={{backgroundColor: '#00C0F4',}}
                                 unselectedStyle={{backgroundColor: '#00C0F4',}}
                                 containerStyle={{height: 40,}}
                                 trackStyle={{height: 5, backgroundColor: 'red',}}
                                 touchDimensions={{height: 40, width: 40, borderRadius: 20, slipDisplacement: 40,}}
                                 customMarker={CustomMarker}
                                 sliderLength={(width * 7.5) / 10}
                    />
                    <RegularText style={{
                        marginLeft: 10,
                        marginTop: '-2.15%',
                        color: 'black'
                    }}>{this.state.sliderValue} miles</RegularText>
                </View>
                <View style={{marginTop: '2%', marginLeft: 10, flexDirection: 'row', height: 25, width: '95%'}}>
                    <RegularText style={{color: 'gray', width: '80%'}}>Categories</RegularText>
                    <ItalicText style={{color: '#00C1ED', alignSelf: 'flex-end'}}> See more</ItalicText>
                </View>

            <View style={{width: '94%', marginLeft: '3%', marginTop: 10, height: 50}}>
                <FlatList style={{ flex:1}}
                          horizontal={true}
                          vertical={false}
                          showsVerticalScrollIndicator={false}
                          showsHorizontalScrollIndicator={false}
                          removeClippedSubviews={true}
                          data={this.state.categoryListArray}
                          extraData={this.state}
                          keyExtractor={this._keyExtractor}
                          renderItem={this._renderItem}
                />
            </View>
                {/* <View style={{ alignItems: 'center', marginTop: '1%',height: 25}}>
          <Text style={{ color: 'gray' }}>
            Tap to see more categories
              </Text>
        </View> */}

             <View style={{ width:'90%',marginLeft:'5%', marginTop: -15}}>
                    <Dropdown
                        label='Price Range($)'
                         data={data}
                         onChangeText={(selectedValue) => {
                             var words = selectedValue.split('to')
                             if (words.length === 1){
                                words = selectedValue.split('+');
                                this.setState({selectedValue:selectedValue,minValue:words[0].trim(), maxValue:'99999'})
                             }else{
                            this.setState({selectedValue:selectedValue,minValue:words[0].trim(), maxValue:words[words.length - 1].trim()})
                             }
                            console.log(this.state.minValue + '   ' + this.state.maxValue) 
                            this.getSearchedResults();
                        }}

                         />

                </View>


                <View style={{marginTop: 10, flexDirection: 'row', alignItems:'center'}}>
                    <RegularText style={{
                        marginLeft: '3%',
                        color: 'gray',
                    }}>{this.state.numberOfItems} items</RegularText>

                    {/* <RegularText style={{marginLeft: '28%', color: 'gray', marginTop: '2%'}}> Value</RegularText> */}
                   

                    {/* <TextInput style={{
                        marginLeft: '1%',
                        height: 40,
                        width: '15%',
                        borderColor: 'gray',
                        borderWidth: 1,
                        borderRadius: 5,
                        fontSize: 13,
                        fontFamily: "NolanNext-Regular"
                    }}
                               autoCorrect={false}
                               returnKeyType='done'
                               keyboardType='numeric'
                               onSubmitEditing={this.getSearchedResults.bind(this)}
                               placeholder="$Min" onChangeText={(minValue) => this.setState({minValue})}
                               placeholderTextColor="gray"
                               underlineColorAndroid='transparent'
                    />
                    <RegularText style={{marginLeft: '1%', color: 'gray', marginTop: '2%'}}>To</RegularText>
                    <TextInput style={{
                        marginLeft: '1%',
                        height: 40,
                        width: '15%',
                        borderColor: 'gray',
                        borderWidth: 1,
                        borderRadius: 5,
                        fontSize: 13,
                        fontFamily: "NolanNext-Regular"
                    }}
                               autoCorrect={false}
                               keyboardType='numeric'
                               returnKeyType='done'
                               onSubmitEditing={this.getSearchedResults.bind(this)}
                               placeholder="$Max" onChangeText={(maxValue) => this.setState({maxValue})}
                               placeholderTextColor="gray"
                               underlineColorAndroid='transparent'
                    /> */}
                </View>

                <Grid
                    itemsList={this.state.searchedItemlists ? this.state.searchedItemlists : []}
                    passingGridData={this.passingGridData}
                />
                {/* {this.state.spinner ?
                    <ActivityIndicator style={{zIndex: 2, position: 'absolute', bottom: 0, top: 0, alignSelf: 'center'}}
                                       color='#00DBBB' size='large'/> : null
                } */}
                <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.6)'
                    textStyle={{color: '#00DBBB'}}
                />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    icon: {
        width: 23,
        height: 23,
    },
    text: {
        fontSize: 15,
        textAlign: 'center',
        color: '#000000',
        marginTop: 8,
        marginLeft: '15%',
        marginRight: '15%',
        fontWeight: 'bold'
    },
    item: {
        marginBottom: 10,
    },
    sliders: {
        marginTop: 10,
        marginLeft: '4%',
        width: '95%',
        flexDirection: 'row',
        height: 10,
    }
});
