import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    TextInput,
    Platform,
    TouchableWithoutFeedback,
    Keyboard,
    KeyboardAvoidingView,
    Dimensions,
} from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';

import {
    BoldText, RegularText
} from '../../Components/styledTexts'

let data = [{
    value: '1 to 49',
  }, {
    value: '50 to 99',
  }, {
    value: '100 to 199',
  },{
    value: '200 to 299',
  },{
    value: '300 to 449',
  },{
    value: '450 to 599',
  },{
    value: '600 to 799',
  },{
    value: '800 to 999',
  },{
    value: '1000 to 1499',
  },{
    value: '1500 to 1999',
  },{
    value: '2000 to 2999',
  },{
    value: '3000 to 4499',
  },{
    value: '4500 to 5999',
  },{
    value: '6000 to 7999',
  },{
    value: '8000 to 9999',
  },{
    value: '10000 to 14999',
  },{
    value: '15000 +',
  },

];


export default class PostItemDetails extends Component {
        
    //Create Top Navigation Bar 
    static navigationOptions = ({navigation}) => ({
        headerTitle: 'Post Item',
        tabBarLabel: 'Home',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../../assets/tab2.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => navigation.goBack(null)}
            >
                <Image
                    source={require('../../assets/back.png')}
                    style={{marginLeft: 15, height: 30, width: 18}}
                    resizeMode='stretch'
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity style={{marginRight: 15, height: 40, width: 40}}
                activeOpacity={0.0}>
            </TouchableOpacity>
        ),
        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 5,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#fff',
                fontFamily: "NolanNext-Bold",
            }
    });
    multiSliderValuesChange = (values) => {
        this.setState({
            multiSliderValue: values,
        });
    }

    constructor(props) {
        super(props)
        this.state =
            {
                selectedCategory: null,
                itemDescripton: '',
                minValue: '1',
                maxValue: '10000', widthSlider: 120,
                multiSliderValue: [1, 10000],
                selectedValue:''
            }
    }

// Selected category for item 
    selectedCategory(item) {
        this.setState({selectedCategory: item})
    }

// Navigate to select category
    selectCategory() {
        this.props.navigation.navigate('Category', {delegate: this})
    }

// Navigate to next screen
    next() {
        console.log("pictures count ",this.props.navigation.state.params.data.pictures)
        if (this.state.selectedCategory != null & this.state.itemDescripton != '') {
            let postData = {
                "title": this.props.navigation.state.params.data.title,
                "pictures": this.props.navigation.state.params.data.pictures,
                "category_id": this.state.selectedCategory.id,
                "min_value": String(this.state.minValue),
                "max_value": String(this.state.maxValue),
                "description": this.state.itemDescripton,
                "imageUrl": this.props.navigation.state.params.data.imageUrl
            };
            this.props.navigation.navigate('PostItemFinish', {postData: postData})
        } else {
            alert('Please fill all the required fields.')
        }
    }

// UI Desigin
    render() {


        
        return (
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
             
                <ScrollView style={styles.container}>
                    {/* <KeyboardAvoidingView  behavior='padding' enabled keyboardVerticalOffset={20}> */}
                    <View style={{flexDirection: 'row', marginTop: 5, height: 30}}>
                        <BoldText style={{color: '#585858', fontSize: 22, marginLeft: '5%'}}>Pick a Category:</BoldText>
                    </View>

                    <TouchableOpacity style={{
                        width: '90%',
                        borderRadius: 5,
                        borderWidth: 1,
                        borderColor: '#585858',
                        marginLeft: '5%',
                        marginTop: '4%',
                        height: 30
                    }}
                                      onPress={() => this.selectCategory()}>

                        <View style={{flexDirection: 'row'}}>
                            <RegularText style={{
                                color: '#585858',
                                fontSize: 18,
                                fontWeight: '500',
                                marginLeft: '3%',
                                width: '85%'
                            }}>{(this.state.selectedCategory) ? this.state.selectedCategory.name : 'Select from Dropdown'}</RegularText>
                            <Image source={require('../../assets/down-arroe.png')}
                                   style={{width: 25, height: 25,}}
                                   resizeMode='contain'/>
                        </View>
                    </TouchableOpacity>
                    <View style={{flexDirection: 'row', marginTop: 5, height: 30}}>
                        <BoldText style={{color: '#585858', fontSize: 22, marginLeft: '5%'}}>Value Range</BoldText>
                    </View>
                    <View style={{
                        width: '90%',
                        height: 70,
                        alignSelf: 'center',
                        marginTop: '1%',
                        flexDirection: 'row',
                        justifyContent: "space-between"
                    }}
                          onLayout={(event) => {
                              var {x, y, width, height} = event.nativeEvent.layout;
                              this.setState({widthSlider: width})
                          }}
                    >

                        {/* <MultiSlider
                                      values={[this.state.multiSliderValue[0], this.state.multiSliderValue[1]]}
                                      sliderLength={this.state.widthSlider-30}
                                      markerStyle={{width:20,height:20,borderRadius:20/2,backgroundColor:'white',borderWidth:1,borderColor:'grey',justifyContent:'center'}}
                                      trackStyle={{height:5}}
                                      containerStyle={{height:'50%',marginTop:'5%',marginLeft:'5%'}}
                                      selectedStyle={{backgroundColor:'#14DFC1'}}
                                      onValuesChange={this.multiSliderValuesChange}
                                      min={1}
                                      max={10000}
                                      step={1}
                                      allowOverlap
                                      snapped
                                   /> */}

                        <View style={{flexDirection: 'column', justifyContent: 'center', width: '95%'}}>
                            {/* <View style={{
                                width: '90%',
                                borderRadius: 5,
                                borderWidth: 2,
                                borderColor: '#585858',
                                marginLeft: '5%'
                            }}> */}
                        
                        <Dropdown
                        label='Price Range($)'
                         data={data}
                         onChangeText={(selectedValue) => {
                             var words = selectedValue.split('to')
                             if (words.length === 1){
                                words = selectedValue.split('+');
                                this.setState({selectedValue:selectedValue,minValue:words[0].trim(), maxValue:'99999'})
                             }else{
                            this.setState({selectedValue:selectedValue,minValue:words[0].trim(), maxValue:words[words.length - 1].trim()})
                             }
                            console.log(this.state.minValue + '   ' + this.state.maxValue) 
                        }}

                         />


                                {/* <TextInput style={{
                                    height: 40,
                                    marginTop: '-2%',
                                    marginLeft: '2%',
                                    width: '90%',
                                    fontFamily: "NolanNext-Regular"
                                }}
                                           maxLength={6}
                                           autoCorrect={false}
                                           keyboardType='default'
                                           placeholder="amount"
                                           onChangeText={(minValue) => this.setState({minValue})}
                                           placeholderTextColor="#585858" underlineColorAndroid='transparent'/> */}
                            {/* </View> */}

                            {/* <RegularText style={{width: '100%', textAlign: 'center'}}>
                                Minimum($)
                            </RegularText> */}
                        </View>

                        {/* <View style={{flexDirection: 'column', justifyContent: 'center', width: '40%'}}>

                        <Dropdown
                        label='Maximum($)'
                        data={data}
                        onChangeText={(maxValue) => this.setState({maxValue}) }
                         /> */}

                            {/* <View style={{width: '90%', borderRadius: 5, borderWidth: 2, borderColor: '#585858'}}>
                                <TextInput style={{
                                    height: 40,
                                    marginTop: '-2%',
                                    marginLeft: '2%',
                                    width: '90%',
                                    fontFamily: "NolanNext-Regular"
                                }}
                                           maxLength={6}
                                           autoCorrect={false}
                                           keyboardType='default'
                                           placeholder="amount"
                                           onChangeText={(maxValue) => this.setState({maxValue})}
                                           placeholderTextColor="#585858" underlineColorAndroid='transparent'/>
                            </View>

                            <RegularText style={{justifyContent: 'flex-end', width: '100%', textAlign: 'center'}}>
                                Maximum($)
                            </RegularText> */}
                        {/* </View> */}
                    </View>

                    <View style={{flexDirection: 'row', marginTop: '1%'}}>
                        <BoldText style={{
                            color: '#585858',
                            fontSize: 22,
                            fontWeight: '700',
                            marginLeft: '5%'
                        }}>Description:</BoldText>
                    </View>

                    <View style={{
                        height: 110,
                        width: '90%',
                        marginLeft: '5%',
                        borderRadius: 8,
                        marginTop: '2%',
                        borderColor: '#585858',
                        borderWidth: 1
                    }}>
                        {/* <KeyboardAvoidingView keyboardVerticalOffset={
                            Platform.select({
                                ios: () => 0,
                                android: () => 200
                            })()
                        } behavior={"padding"}> */}
 {/* <KeyboardAvoidingView  behavior='position'
                                      enabled> */}
                            <TextInput
                                style={{
                                    marginLeft: '2%',
                                    width: '96%',
                                    height: '100%',
                                    textAlignVertical: 'top',
                                    fontFamily: "NolanNext-Regular"
                                }}
                                returnKeyType='default'
                                autoCorrect={false}
                                multiline={true}
                                keyboardType='default'
                                placeholder="Your description here."
                                onChangeText={(itemDescripton) => this.setState({itemDescripton: itemDescripton})}
                                placeholderTextColor="#585858" underlineColorAndroid='transparent'/>

                        {/* </KeyboardAvoidingView> */}
                    </View>


                    {/* <Button
                        name={'Next'} fontSize={22}
                        onPress={this.next.bind(this)}
                        marginTop={'7%'}
                        marginBottom={20}
                        textMarginTop={'3%'}
                        backgroundColor={'#14DFC1'}
                        color={'white'}
                        height={45}
                        marginLeft={'5%'}
                        marginRight={'5%'}
                        fontFamily={"NolanNext-Regular"}
                    /> */}

<TouchableOpacity
                        style={{
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#14DFC1',
                            marginBottom:20,
                            marginTop: 20,
                            height: 45,
                            marginLeft: '5%',
                            marginRight:'5%',
                        }}
                        onPress={this.next.bind(this)}>
                        <Text style={{
                            fontSize: 22, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Regular"
                        }}>Next</Text>
                </TouchableOpacity>

                    <View style={{flexDirection: 'row', justifyContent: 'center', height: 80, marginBottom:10}}>
                        <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                            <View style={[styles.circleIcon, {justifyContent: 'center'}]}>
                                <Image source={require('../../assets/white-check.png')}
                                       style={{width: 30, height: 30, marginLeft: '15%'}}
                                       resizeMode='contain'/>
                            </View>
                            <RegularText
                                style={{alignSelf: 'center', fontWeight: '600', color: '#D9D9D9'}}>Photo</RegularText>
                        </View>

                        <Text style={{marginTop: '3%', fontSize: 24, color: '#D9D9D9'}}>------</Text>

                        <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                            <View style={[styles.circleIcon, {backgroundColor: '#14DFC1'}]}><BoldText
                                style={{alignSelf: 'center', color: 'white', fontSize: 24}}>2</BoldText></View>
                            <RegularText
                                style={{alignSelf: 'center', fontWeight: '600', color: '#14DFC1'}}>Details</RegularText>
                        </View>
                        <Text style={{marginTop: '4%', fontSize: 24, color: '#D9D9D9'}}>------</Text>
                        <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                            <View style={styles.circleIcon}><BoldText
                                style={{alignSelf: 'center', color: 'white', fontSize: 24}}>3</BoldText></View>
                            <RegularText
                                style={{alignSelf: 'center', fontWeight: '600', color: '#D9D9D9'}}>Finish</RegularText>
                        </View>

                    </View>

                    {/* </KeyboardAvoidingView> */}
                </ScrollView>
               
            </TouchableWithoutFeedback>
        )
    }

}

const styles = StyleSheet.create({
    container:
        {
            // flex: 1,
            height: Dimensions.get('window').height,
            backgroundColor: 'white'
        },
    icon: {
        width: 23,
        height: 23,
    },
    ImageContainer: {
        width: '100%',
        height: 280,
        justifyContent: 'center',
        alignItems: 'center',
    },
    circleIcon: {
        borderRadius: 50 / 2,
        width: 50,
        height: 50,
        backgroundColor: '#D9D9D9',
        justifyContent: 'center'
    }
});
