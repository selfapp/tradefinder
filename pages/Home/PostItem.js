import React, {Component} from 'react';
import {
    StyleSheet, 
    Text, 
    View, 
    Image, 
    TouchableOpacity,
    ActivityIndicator,
    ScrollView, 
    TextInput, 
    ImageBackground, 
    TouchableWithoutFeedback, 
    Keyboard, KeyboardAvoidingView
} from 'react-native';

import CusomizedImagePicker from 'react-native-customized-image-picker';
import Swiper from 'react-native-swiper';
import Button from '../../Components/button';
import {
    BoldText, RegularText
} from '../../Components/styledTexts'

var ImagePicker = require('react-native-image-picker');
var imageCamera = []
const options = {
    quality: 1.0,
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
        skipBackup: true
    }
};

export default class PostItem extends Component {
    //Create Top Navigation Bar
    static navigationOptions = ({navigation}) => ({
        headerTitle: 'Post Item',
        tabBarLabel: 'Home',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../../assets/tab2.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => navigation.goBack(null)}
            >
                <Image
                    source={require('../../assets/back.png')}
                    style={{marginLeft: 15, height: 30, width: 18}}
                    resizeMode='stretch'
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity 
            style={{marginRight: 15, height: 40, width: 40}}
                activeOpacity={0.0}>
            </TouchableOpacity>
        ),

        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 5,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: 'white',
                fontFamily: "NolanNext-Bold",

            }
    });


    constructor(props) {

        super(props)
        this.state =
            {
                itemTitle: '',
                itemId: '',
                userId: '',
                item_pic: '',
                image: [],
                loading: false, position: 1, cameraImageCount:0

            }
    }


    componentDidMount() {
        console.log("componentDidMount called ......")
        
        imageCamera = []
    }

    componentWillUnmount() {
        console.log("componentWillUnmount called ......")
        
        imageCamera = []
        console.log(imageCamera)
    }
   

// Launch Camera for Upload photo
    launchingCamera() {
        // const img = [];
        const options = {
            quality: 1.0,
            maxWidth: 300,
            maxHeight: 400,
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.launchCamera(options, (response) => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                // img.push('data:image/png;base64,' + response.data);
                imageCamera.push('data:image/png;base64,' + response.data);

                this.setState({item_pic: imageCamera, cameraImageCount:this.state.cameraImageCount+1})
            }
        });
    }
// Gallery for Photo
    launchingGallery() {
        const img = [];
        // CusomizedImagePicker.clean().then(() => {
        //     console.log('removed all tmp images from tmp directory');
        //   }).catch(e => {
        //     console.log(e);
        //   });

        CusomizedImagePicker.openPicker({
            multiple: true,
            includeBase64: true,
            minCompressSize: 5,
            compressQuality:50,
            maxSize: 6,
            height: 500,
            width: 400
        }).then(images => {
            for (var i = 0; i < images.length; i++) {
                img.push('data:image/png;base64,' + images[i].data)
            }
            this.setState({item_pic: img})
        });
    }

// Navigate to Item details screen
    async next() {

        console.log("item pic count ", this.state.item_pic)
        if (this.state.itemTitle != '' && this.state.item_pic != '') {
            let postData = {
                "title": this.state.itemTitle,
                "pictures": this.state.item_pic,
            }
            this.props.navigation.navigate('PostItemDetails', {data: postData})
        } else {
            alert('Please fill all the required fields.')
        }

    }

    

// UI desigin
    render() {

        let shareOptions = {
            title: this.state.itemValue,
            message: "Category=" + this.state.categoryName + "\n" + "Item Title=" + this.state.itemTitle + "\n" + "Item Value=" + this.state.itemValue + "\n" + "Item Description=" + this.state.itemDescripton,
            url: this.state.item_pic,
            subject: this.state.itemTitle
        };
        return (
            <View style={{ flex: 1 }}>


                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <ScrollView style={styles.container}>
                    <KeyboardAvoidingView 
                            contentContainerStyle={{ flex: 1}} 
                            behavior='position'
                            keyboardVerticalOffset={-80}
                            enabled
                            >
                        {/* <View style={styles.container}> */}
                        <View style={{alignItems:'center'}}>
                            <ImageBackground style={styles.ImageContainer}>
                                {this.state.item_pic.length === 0 ?

                                    <View>
                                        <TouchableOpacity onPress={this.launchingCamera.bind(this)}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Image
                                                    source={require('../../assets/cam-dark.png')}
                                                    style={{height: 50, width: 50}}
                                                    resizeMode='contain'/>
                                                <BoldText style={{
                                                    color: '#585858',
                                                    fontSize: 21,
                                                    fontWeight: '700',
                                                    marginLeft: '5%',
                                                    marginTop: '3.5%'
                                                }}>Take Photo</BoldText>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={this.launchingGallery.bind(this)}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Image
                                                    source={require('../../assets/pictures.png')}
                                                    style={{height: 50, width: 50}}
                                                    resizeMode='contain'/>
                                                <BoldText style={{
                                                    color: '#585858',
                                                    fontSize: 21,
                                                    fontWeight: '700',
                                                    marginLeft: '5%',
                                                    marginTop: '3.5%'
                                                }}>Select Photo</BoldText>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <View style={{flex: 1, backgroundColor: 'white'}}>
                                        <View style={{flexDirection: 'row', height: '100%', width: '100%'}}>
    {/* Swiper for selected Images */}
                                            <Swiper
                                                horizontal={true}
                                                autoplay={false}
                                                // autoplayTimeout={1}
                                                showsPagination={true}
                                                loop={false}
                                                dot={<View style={{
                                                    backgroundColor: '#4191AA',
                                                    width: 10,
                                                    height: 10,
                                                    borderRadius: 5,
                                                    marginLeft: 3,
                                                    marginRight: 3,
                                                    marginTop: 3,
                                                    marginBottom: 3,
                                                }}/>}
                                                activeDot={<View style={{
                                                    backgroundColor: 'red',
                                                    width: 16,
                                                    height: 16,
                                                    borderRadius: 8,
                                                    marginLeft: 3,
                                                    marginRight: 3,
                                                    marginTop: 3,
                                                    marginBottom: 3,
                                                }}/>}
                                                buttonWrapperStyle={{
                                                    backgroundColor: 'transparent',
                                                    flexDirection: 'row',
                                                    position: 'absolute',
                                                    top: 0,
                                                    left: 0,
                                                    flex: 1,
                                                    paddingHorizontal: 10,
                                                    paddingVertical: 10,
                                                    justifyContent: 'space-between',
                                                    alignItems: 'center'
                                                }}
                                                nextButton={<View
                                                    style={[styles.triangle, {transform: [{rotate: '90deg'}]}]}/>}
                                                prevButton={<View
                                                    style={[styles.triangle, {transform: [{rotate: '-90deg'}]}]}/>}
                                                showsButtons
                                            >
                                                {this.state.item_pic.map((item, key) => {
                                                    return (
                                                        <View key={key}
                                                            style={{alignItems: 'center', flex: 1, height: '100%'}}>
                                                            <Image
                                                                style={{width: '95%', height: '100%'}}
                                                                source={{uri: item}}
                                                                resizeMode='contain'
                                                            />

                                                        </View>
                                                    )
                                                })}
                                            </Swiper>
                                        </View>
                                    </View>
                                }
                            </ImageBackground>
                            {
                                this.state.cameraImageCount > 0 && this.state.cameraImageCount < 6 ? (
                                    <TouchableOpacity style={{backgroundColor:'gray', width:160, height:40, marginHorizontal:10, 
                                                             justifyContent:'center', alignItems:'center'}} 
                                                      onPress={this.launchingCamera.bind(this)}>
                                                <Text>Take Photo ({this.state.cameraImageCount}/6)</Text>
                                    </TouchableOpacity>
                                ): (null)
                            }
                           
                        </View>
                        <View style={{height: 2, backgroundColor: '#7D7D7D', width: '100%', marginTop: '0%'}}>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: 5}}>
                            <BoldText style={{color: '#585858', fontSize: 18, fontWeight: '700', marginLeft: '5%'}}>
                                Title
                            </BoldText>

                        </View>
                        {/* item Title */}
                        <View style={{flexDirection: 'row', marginTop: '2%'}}>
                            <View style={{
                                width: '90%', borderRadius: 10, borderWidth: 1, borderColor: '#585858',
                                marginLeft: '5%'
                            }}>
                            
                                <TextInput
                                    style={{
                                        height: 50, marginTop: '-2%', marginLeft: '2%',
                                        width: '90%',
                                        fontSize: 18,
                                        fontFamily: "NolanNext-Regular"
                                    }}
                                    returnKeyType='done'
                                    maxLength={40}
                                    autoCorrect={false}
                                    keyboardType='default'
                                    placeholder="Your title here." onChangeText={(itemTitle) => this.setState({itemTitle})}
                                    placeholderTextColor="#585858" underlineColorAndroid='transparent'/>
                            </View>

                        </View>

                        {/* <Button
                            name={'Next'} fontSize={22}
                            onPress={this.next.bind(this)}
                            marginTop={'10%'}
                            marginBottom={30}
                            backgroundColor={'#1FDBB8'}
                            color={'white'}
                            textMarginTop={'3%'}
                            height={45}
                            marginLeft={'5%'}
                            marginRight={'5%'}
                        /> */}

                <TouchableOpacity
                        style={{
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#1FDBB8',
                            marginBottom:30,
                            height: 45,
                            marginLeft: '5%',
                            marginRight:'5%',
                            marginTop: 30,
                        }}
                        onPress={this.next.bind(this)}>
                        <Text style={{
                            fontSize: 22, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Regular"
                        }}>Next</Text>
                </TouchableOpacity>

                        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                            <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                                <View style={[styles.circleIcon, {backgroundColor: '#14DFC1'}]}><BoldText
                                    style={{alignSelf: 'center', color: 'white', fontSize: 24}}>1</BoldText></View>
                                <RegularText
                                    style={{alignSelf: 'center', fontWeight: '600', color: '#14DFC1'}}>Photo</RegularText>
                            </View>

                            <Text style={{marginTop: '3%', fontSize: 24, color: '#D9D9D9'}}>------</Text>

                            <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                                <View style={styles.circleIcon}><BoldText
                                    style={{alignSelf: 'center', color: 'white', fontSize: 24}}>2</BoldText></View>
                                <RegularText
                                    style={{alignSelf: 'center', fontWeight: '600', color: '#D9D9D9'}}>Details</RegularText>
                            </View>
                            <Text style={{marginTop: '4%', fontSize: 24, color: '#D9D9D9'}}>------</Text>
                            <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                                <View style={styles.circleIcon}><BoldText
                                    style={{alignSelf: 'center', color: 'white', fontSize: 24}}>3</BoldText></View>
                                <RegularText
                                    style={{alignSelf: 'center', fontWeight: '600', color: '#D9D9D9'}}>Finish</RegularText>
                            </View>

                        </View>


                        {this.state.loading ?
                            <ActivityIndicator
                                style={{zIndex: 2, position: 'absolute', bottom: 0, top: 0, alignSelf: 'center'}}
                                color='#00DBBB' size='large'/> : null
                        }
                        {/* </View> */}
                        </KeyboardAvoidingView>
                    </ScrollView>
                </TouchableWithoutFeedback>
                
            </View>

        );
    }
}


const styles = StyleSheet.create({
    container:
        {
            flex: 1,
            backgroundColor: 'white'
        },
    icon: {
        width: 23,
        height: 23,
    },
    ImageContainer: {
        width: '100%',
        height: 280,
        justifyContent: 'center',
        alignItems: 'center',
    },
    circleIcon: {
        borderRadius: 50 / 2,
        width: 50,
        height: 50,
        backgroundColor: '#D9D9D9',
        justifyContent: 'center'
    },
    triangle: {
        borderLeftWidth: 40,
        borderRightWidth: 40,
        borderBottomWidth: 25,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'white',
        opacity: 0.29
    }
});
