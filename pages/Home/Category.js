import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, FlatList} from 'react-native';
import api from '../../api';
import {
    BoldText, ItalicText, RegularText
} from '../../Components/styledTexts'

export default class Category extends Component {
    static navigationOptions = ({navigation}) => ({
        headerTitle: 'Select Category',
        tabBarLabel: 'Home',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../../assets/tab2.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => navigation.goBack(null)}
            >
                <Image
                    source={require('../../assets/back.png')}
                    style={{marginLeft: 15, height: 30, width: 18}}
                    resizeMode='stretch'
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity 
            style={{marginRight: 15, height: 40, width: 40}}
                activeOpacity={0.0}>
            </TouchableOpacity>
        ),
        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 5,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: 'white',
                fontFamily: "NolanNext-Bold",
            }
    });

    constructor(props) {
        super(props)
        this.state =
            {
                categories: []
            }
    }

    componentDidMount() {
        this.getCategories()
    }

    async getCategories() {
        var _this = this
        let response = await api.getRequest('/categories', 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    _this.setState({
                        categories: data.categories
                    });
                });
        }
    }

    selectedItem(item) {
        this.props.navigation.state.params.delegate.selectedCategory(item)
        this.props.navigation.goBack(null)
    }


    renderItem = ({item, index}) => {
        return (
            <TouchableOpacity onPress={() => this.selectedItem(item)}>
                <View style={{width: '100%', height: 40, borderBottomWidth: 1, justifyContent: 'center'}}><BoldText
                    style={{
                        color: '#585858',
                        fontWeight: '600',
                        fontSize: 22,
                        marginLeft: '5%',
                        height: '100%',
                        marginTop: '3%'
                    }}>{item.name}</BoldText></View>
            </TouchableOpacity>
        )
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={{width: '100%', height: '6%', marginTop: '1%', borderBottomWidth: 1}}><BoldText style={{
                    color: '#585858',
                    fontSize: 22,
                    fontWeight: '600',
                    marginLeft: '5%',
                    marginTop: '1%'
                }}>All</BoldText></View>
                <FlatList
                    data={this.state.categories}
                    renderItem={this.renderItem}
                />


            </View>
        )
    }
}
const styles = StyleSheet.create({
    container:
        {
            flex: 1,
            backgroundColor: 'white'
        },
    icon: {
        width: 23,
        height: 23,
    },
    ImageContainer: {
        width: '100%',
        height: 280,
        justifyContent: 'center',
        alignItems: 'center',
    },
    circleIcon: {
        borderRadius: 50 / 2,
        width: 50,
        height: 50,
        backgroundColor: '#D9D9D9',
        justifyContent: 'center'
    }
});
