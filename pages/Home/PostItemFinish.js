import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput, ActivityIndicator} from 'react-native';
import api from '../../api';
import Share from 'react-native-share';
import Swiper from 'react-native-swiper';
import {
    BoldText, ItalicText, RegularText
} from '../../Components/styledTexts'

import {
    shareOnFacebook,
    shareOnTwitter,
  } from 'react-native-social-share';


export default class PostItemFinish extends Component {

        //Create Top Navigation Bar 
    static navigationOptions = ({navigation}) => ({
        headerTitle: 'Post Item',
        tabBarLabel: 'Home',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../../assets/tab2.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => navigation.goBack(null)}
            >
                <Image
                    source={require('../../assets/back.png')}
                    style={{marginLeft: 15, height: 30, width: 18}}
                    resizeMode='stretch'
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity style={{marginRight: 15, height: 40, width: 40}}
                activeOpacity={0.0}>
            </TouchableOpacity>
        ),
        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 5,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#fff',
                fontFamily: "NolanNext-Bold",
            }
    });

    constructor(props) {
        super(props)
        this.state =
            {
                isFacebbok: false,
                isTwitter: false,
                isEmail: false, loading: false
            }
    }

    //Share post item
    sharePostitem(item) {

        const picture = item.pictures[0]
        let imageUrl = picture.picture;
    console.log('share testing')
        let shareOptions = {
            title: item.title,
            message: "Category=" + item.category.id + "\n" + "Item Title=" + item.title + "\n"
             + "Item Description=" + item.description,
            url: imageUrl,
            subject: item.title
        };

        if (this.state.isFacebbok) {
            shareOnFacebook({
            'link':imageUrl
            },
            (results) => {
                console.log("Facebook result .....");
                console.log(results);
            }
            );
        }
        if (this.state.isEmail) {
            setTimeout(() => {
                Share.shareSingle(Object.assign(shareOptions, {
                "social": "email"
                }));
            },300);
        }
       
        if (this.state.isTwitter) {
            console.log("description" + item.description)
            shareOnTwitter({
                'text': item.description,
                'link':imageUrl
              },
              (results) => {
                console.log("Twitter result .....");
                console.log(results);
              }
            );
        }
       
        // if (this.state.isFacebbok || this.state.isTwitter || this.state.isEmail) {

        //     let shareOptions = {
        //         title: this.props.navigation.state.params.postData.title,
        //         message: "Category=" + this.props.navigation.state.params.postData.category_id + "\n" + "Item Title=" + this.props.navigation.state.params.postData.title + "\n" + "Item Description=" + this.props.navigation.state.params.postData.description,
        //         // url: this.props.navigation.state.params.postData.picture,
        //         url: imageUrl,

        //         subject: this.props.navigation.state.params.postData.title
        //     };
        //     Share.open(shareOptions);
        // }
    }

    // post item update on sarver
    postItem = async () => {


        var _this = this
        _this.setState({
            loading: true
        })
console.log("pictures final count ", this.props.navigation.state.params.postData.pictures)
        let body = {
            "items": {
                "title": this.props.navigation.state.params.postData.title,
                "min_value": this.props.navigation.state.params.postData.min_value,
                "max_value": this.props.navigation.state.params.postData.max_value,
                "description": this.props.navigation.state.params.postData.description,
                "category_id": this.props.navigation.state.params.postData.category_id,
                "pictures": this.props.navigation.state.params.postData.pictures
            }
        };
        
        try {
            let response = await api.request('/items', 'POST', body);
           // console.log(JSON.stringify(response));

            _this.setState({loading: false})
            if (response.status === 200) {
                response.json().then((data) => {
                    // console.log("post response")
                    // console.log(data)
                   
                    //rajeev
                    this.sharePostitem(data.item)
                });
// navigate to home screen       
         
                this.props.navigation.navigate("TradeFindr")
            } else {

                await response.json().then((res) => {
                });
            }
        } catch (error) {

            _this.setState({loading: false});
            error.json().then((err) => {
            });
        }
    };

// UI Desigin
    render() {
        return (
            <View style={styles.container}>
                <View style={{flexDirection: 'row', height: '40%', width: '100%'}}>


                    <Swiper
                        horizontal={true}
                        autoplay={false}
                        // autoplayTimeout={1}
                        showsPagination={true}
                        loop={false}
                        dot={<View style={{
                            backgroundColor: '#4191AA',
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            marginLeft: 3,
                            marginRight: 3,
                            marginTop: 3,
                            marginBottom: 3,
                        }}/>}
                        activeDot={<View style={{
                            backgroundColor: 'red',
                            width: 16,
                            height: 16,
                            borderRadius: 8,
                            marginLeft: 3,
                            marginRight: 3,
                            marginTop: 3,
                            marginBottom: 3,
                        }}/>}
                        buttonWrapperStyle={{
                            backgroundColor: 'transparent',
                            flexDirection: 'row',
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            flex: 1,
                            paddingHorizontal: 10,
                            paddingVertical: 10,
                            justifyContent: 'space-between',
                            alignItems: 'center'
                        }}
                        nextButton={<View style={[styles.triangle, {transform: [{rotate: '90deg'}]}]}/>}
                        prevButton={<View style={[styles.triangle, {transform: [{rotate: '-90deg'}]}]}/>}
                        showsButtons
                    >
                        {this.props.navigation.state.params.postData.pictures.map((item, key) => {
                            return (
                                <View key={key} style={{alignItems: 'center'}}>
                                    <Image
                                        style={{width: '95%', height: '100%'}}
                                        source={{uri: item}}

                                        resizeMode='contain'
                                    />

                                </View>
                            )
                        })}
                    </Swiper>


                    {/* <Image source={this.props.navigation.state.params.postData.imageUrl}
                            style={{width:'100%', height: '100%' }}/> */}
                </View>
                <View style={{flexDirection: 'row', marginTop: '3%', height: '8%'}}>
                    <BoldText style={{color: '#585858', fontSize: 22, fontWeight: '700', marginLeft: '5%'}}>Share
                        Item</BoldText>
                </View>

                <View style={{flexDirection: 'column', height: '25%', marginTop: '1%'}}>
                    <TouchableOpacity style={{
                        width: '90%',
                        borderRadius: 5,
                        borderWidth: 1,
                        borderColor: '#585858',
                        marginLeft: '5%',
                        marginTop: '1%',
                        height: '25%'
                    }}
                                      onPress={() => this.setState({isFacebbok: !this.state.isFacebbok})}>

                        <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 2}}>
                            <Image source={require('../../assets/fb-gry.png')}
                                   style={{width: 25, height: 25,}}
                                   resizeMode='stretch'/>
                            <RegularText style={{
                                color: '#585858',
                                fontSize: 18,
                                marginLeft: '10%',
                                width: '70%'
                            }}>Facebook</RegularText>
                            <Image
                                source={(this.state.isFacebbok) ? require('../../assets/green-chec-cir.png') : require('../../assets/green-outlin-cir.png')}
                                style={{width: 25, height: 25,}}
                                resizeMode='stretch'/>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity style={{
                        width: '90%',
                        borderRadius: 5,
                        borderWidth: 1,
                        borderColor: '#585858',
                        marginLeft: '5%',
                        marginTop: '3%',
                        height: '25%'
                    }}
                                      onPress={() => this.setState({isTwitter: !this.state.isTwitter})}>

                        <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 2}}>
                            <Image source={require('../../assets/twitter-gry.png')}
                                   style={{width: 25, height: 25, justifyContent: 'center'}}
                                   resizeMode='stretch'
                            />
                            <RegularText style={{
                                color: '#585858',
                                fontSize: 18,
                                marginLeft: '10%',
                                width: '70%'
                            }}>Twitter</RegularText>
                            <Image
                                source={(this.state.isTwitter) ? require('../../assets/green-chec-cir.png') : require('../../assets/green-outlin-cir.png')}
                                style={{width: 25, height: 25,}}
                                resizeMode='stretch'/>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity style={{
                        width: '90%',
                        borderRadius: 5,
                        borderWidth: 1,
                        borderColor: '#585858',
                        marginLeft: '5%',
                        marginTop: '3%',
                        height: '25%'
                    }}
                                      onPress={() => this.setState({isEmail: !this.state.isEmail})}>

                        <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 2}}>
                            <Image source={require('../../assets/mail-gry.png')}
                                   style={{width: 25, height: 25,}}
                                   resizeMode='stretch'/>
                            <RegularText style={{
                                color: '#585858',
                                fontSize: 18,
                                marginLeft: '10%',
                                width: '70%'
                            }}>Email</RegularText>
                            <Image
                                source={(this.state.isEmail) ? require('../../assets/green-chec-cir.png') : require('../../assets/green-outlin-cir.png')}
                                style={{width: 25, height: 25,}}
                                resizeMode='stretch'/>
                        </View>
                    </TouchableOpacity>
                </View>
                {this.state.loading ?
                    <ActivityIndicator style={{zIndex: 2, position: 'absolute', bottom: 0, top: 0, alignSelf: 'center'}}
                                       color='#00DBBB' size='large'/> : null
                }

                {/* <Button
                    name={'Post'} fontSize={22}
                    onPress={() => this.postItem()}
                    marginTop={'2%'}
                    marginBottom={'3%'}
                    textMarginTop={'3%'}
                    backgroundColor={'#14DFC1'}
                    color={'white'}
                    height={45}
                    marginLeft={'5%'}
                    marginRight={'5%'}
                    fontFamily={"NolanNext-Regular"}
                /> */}

                <TouchableOpacity
                        style={{
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#14DFC1',
                            marginBottom:10,
                            marginTop: 10,
                            height: 45,
                            marginLeft: '5%',
                            marginRight:'5%',
                        }}
                        onPress={() => this.postItem()}>
                        <Text style={{
                            fontSize: 22, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Regular"
                        }}>Post</Text>
                </TouchableOpacity>

                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                    <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                        <View style={styles.circleIcon}>
                            <Image source={require('../../assets/white-check.png')}
                                   style={{width: 30, height: 30, marginLeft: '15%'}}
                                   resizeMode='contain'/>
                        </View>
                        <RegularText
                            style={{alignSelf: 'center', fontWeight: '600', color: '#D9D9D9'}}>Photo</RegularText>
                    </View>

                    <Text style={{marginTop: '3%', fontSize: 24, color: '#D9D9D9'}}>------</Text>

                    <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                        <View style={styles.circleIcon}>
                            <Image source={require('../../assets/white-check.png')}
                                   style={{width: 30, height: 30, marginLeft: '15%'}}
                                   resizeMode='contain'/>
                        </View>
                        <RegularText
                            style={{alignSelf: 'center', fontWeight: '600', color: '#D9D9D9'}}>Details</RegularText>
                    </View>
                    <Text style={{marginTop: '4%', fontSize: 24, color: '#D9D9D9'}}>------</Text>
                    <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                        <View style={[styles.circleIcon, {backgroundColor: '#14DFC1'}]}><BoldText
                            style={{alignSelf: 'center', color: 'white', fontSize: 24}}>3</BoldText></View>
                        <RegularText
                            style={{alignSelf: 'center', fontWeight: '600', color: '#14DFC1'}}>Finish</RegularText>
                    </View>
                </View>

            </View>
        )
    }

}

const styles = StyleSheet.create({
    container:
        {
            flex: 1,
            backgroundColor: 'white'
        },
    icon: {
        width: 23,
        height: 23,
    },
    ImageContainer: {
        width: '100%',
        height: 280,
        justifyContent: 'center',
        alignItems: 'center',
    },
    circleIcon: {
        borderRadius: 50 / 2,
        width: 50,
        height: 50,
        backgroundColor: '#D9D9D9',
        justifyContent: 'center'
    },
    triangle: {
        borderLeftWidth: 40,
        borderRightWidth: 40,
        borderBottomWidth: 25,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'white',
        opacity: 0.29
    }
});
