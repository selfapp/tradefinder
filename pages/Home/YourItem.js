import React, {Component} from 'react';
import {
    StyleSheet,
    Dimensions,
    FlatList,
    View,
    ActivityIndicator,
    Modal,Text,
    Image,
    TouchableOpacity,
    ScrollView,
    TouchableWithoutFeedback,
    AsyncStorage
} from 'react-native';
import StarRating from 'react-native-star-rating';
import MapView from 'react-native-maps';
import Button from '../../Components/button';
import navigate from '../../Components/navigate';
import Spinner from 'react-native-loading-spinner-overlay';
import api from '../../api';
import GridModalBrowse from '../../Components/GridModalBrowse';
import Swiper from 'react-native-swiper';
import {BoldText, RegularText, ItalicText} from '../../Components/styledTexts';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

export default class YourItem extends Component {
//   static navigationOptions = {
//     header: null
// }
    static navigationOptions = ({navigation}) => ({
        header: null
    });

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            starCount: null,
            itemId: this.props.navigation.state.params.itemId,
            userAddress: '',
            usersLatitude: null,
            usersLongitude: null,
            userName: '',
            allSelectedItemList: [],
            flagItem: false,
            item_pic_uri: [],
            itemDetailValues: [],
            itemName: '',
            itemDescription: '',
            itemPostedDays: '',
            mapRegion: null,
            lastLat: null,
            lastLong: null,
            gridModalVisible: false,
            userId: 0, min_value: 0, max_value: 0,
            item: null,
            user: '', fullImagePic:'', enableScrollViewScroll:true,
            currentUserid:'',
            imageIndex:0
        }
    }

    componentDidMount() {
        this.watchID = navigator.geolocation.watchPosition((position) => {
            let region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: 0.00922 * 1.5,
                longitudeDelta: 0.00421 * 1.5
            }
            this.onRegionChange(region, region.latitude, region.longitude);
        });
        this.getPageContent()
        this.getItemDetailValues()
        this.getUserDetailValues()
    }

    async getUserDetailValues() {
        await AsyncStorage.getItem('userDetailValues').then(data => {
            var detailvalue = JSON.parse(data);
            console.log("Current user ID" + detailvalue.user.id); 
             this.setState({currentUserid:detailvalue.user.id})
        });
    }

    onRegionChange(region, usersLatitude, usersLongitude) {
        this.setState({
            mapRegion: region,
            lastLat: usersLatitude || this.state.usersLatitude,
            lastLong: usersLongitude || this.state.usersLongitude
        });
    }


    componentWillUnmount() {
        this.setState({fullImagePic:''})
        navigator.geolocation.clearWatch(this.watchID);
    }

    setGridModalVisible = (isVisible) => {
        this.setState({
            gridModalVisible: isVisible
        })
    }


    async getPageContent() {
        var _this = this
        _this.setState({
            spinner: true
        })

        try {
            let response = await api.getRequest('/items/' + _this.state.itemId, 'GET');
            if (response.status === 200) {
                response.json()
                    .then(function (data) {
                      console.log("item details")
                      console.log(data)
                        let averageRating = data.user.averageRating !==null ? Math.round(data.user.averageRating) : 0;
                        _this.setState({
                            spinner: false,
                            starCount: averageRating,
                            item: data.item,
                            user: data.user,
                            flagItem: data.item.userFlag
                        });
                    })
            } else {
                _this.setState({
                    loading: false
                })
            }
        } catch (error) {
            _this.setState({
                loading: false
            })
        }
    }

    async removeItem() {
        var _this = this
        _this.setState({
            loading: true
        })
        try {
            let response = await api.getRequest('/item/' + _this.state.itemId + '/delete', 'DELETE');
            if (response.status === 200) {
                response.json()
                    .then(function (data) {
                        _this.setState({
                            loading: false
                        })
                        if (data.msg == 'Item is delete successfullly') {
                            // navigate.navigateWithReset(_this.props.navigation, 'TradeFindr')
                            _this.props.navigation.navigate("TradeFindr")
                        } else {
                            alert(data.msg);
                        }
                    })
            } else {
                _this.setState({
                    loading: false
                })
            }
        } catch (error) {
            _this.setState({
                loading: false
            })
        }
    }

    addToWishlist = async () => {

        var _this = this;
        _this.setState({
            loading: true
        })

        if (_this.state.allSelectedWishList !== null) {
            _this.setState({
                loading: true,

            });
            let body = {
                "wishlist": {
                    "item_id": this.state.itemId,
                    "name": "",
                    "category_id": ""
                }
            };
            try {
                let response = await api.request('/wishlist', 'POST', body);

                if (response.status === 200) {

                    _this.setState({loading: false});
                    alert('Item added to your wishlist.')
                    response.json().then((data) => {
                        _this.setState({
                            loading: false,
                        });

                    });
                } else {
                    await response.json().then((res) => {
                        }
                    );
                }
            } catch (error) {
            }
        } else {
            alert('Please select at least one item')
        }
    }

    flaggingItem = async (value) => {
        var _this = this
        var flagValue;
        _this.setState({
            loading: true
        })
        if (_this.state.flagItem) {
            flagValue = false
            _this.setState({
                flagItem: false
            })
        } else {
            flagValue = true
            _this.setState({
                flagItem: true
            })
        }
        if (flagValue) {
            try {
                let response = await api.request('/item/' + _this.state.item.id + '/flagItem', 'POST');
                if (response.status === 200) {
                    response.json().then((data) => {
                        _this.setState({
                            loading: false,
                            flagItem: data.item.flag
                        })
                    });
                } else {
                    _this.setState({spinner: false});
                    await response.json().then((res) => {
                        }
                    );
                }
            } catch (error) {
                _this.setState({loading: false});
            }
        } else {
            try {
                let response = await api.getRequest('/item/' + _this.state.item.id + '/unflagItem', 'DELETE');
                if (response.status === 200) {
                    response.json().then((data) => {
                        _this.setState({
                            loading: false,
                            flagItem: data.item.flag
                        })
                    });
                } else {
                    _this.setState({spinner: false});
                    await response.json().then((res) => {
                        }
                    );
                }
            } catch (error) {
                _this.setState({loading: false});
            }
        }
    }

    async getItemDetailValues() {
        var _this = this
        let response = await api.getRequest('/user/items', 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    _this.setState({
                        spinner: false,
                        itemDetailValues: data.items
                    })
                })
        } else {
            _this.setState({
                spinner: false
            });
            alert(response)
        }
    }


    onStarRatingPress = async (rating) => {
        if (!this.props.navigation.state.params.myItem) {
            var _this = this
            this.setState({loading: true});
            let body = {
                "rate": rating
            }
            try {
                let response = await api.request('/users/' + _this.state.user.id + '/rating', 'POST', body);
                if (response.status === 200) {
                    response.json().then((data) => {
                        console.log("star rating" + JSON.stringify(data))
                        _this.setState({loading: false, user: data.user})
                    });
                } else {
                    _this.setState({spinner: false});
                }
            } catch (error) {
                _this.setState({loading: false});
            }
        }
    }

    save_channel = async()=>{
        var _this = this;
        var channel_name = ""
        console.log("Current UserId " + this.state.currentUserid);
        console.log("Other user id " + this.state.user.id);
        if(this.state.currentUserid < this.state.user.id){
            channel_name = this.state.currentUserid + "-" + this.state.user.id;
          }else{
              channel_name = this.state.user.id + "-" + this.state.currentUserid;
          }

        let body = {
            "channel": [{"channel_name":channel_name,"item_with_user_id":this.state.user.id}]
        };
        console.log("channel data " + JSON.stringify(body))
        try {
            let response = await api.request('/channel', 'POST', body);
            if (response.status === 200) {
                response.json().then((data) => {
                    console.log("channel response " + JSON.stringify(data))

                    _this.setState({
                            spinner: false
                        })
                        // navigate.navigateWithReset(_this.props.navigation, 'TradeFindr')
                        setTimeout(function () {
                             _this.props.navigation.navigate('TradeFindr')
                             //navigate.navigateWithReset(_this.props.navigation, 'TradeFindr')
                        }, 1000);


                });
            } else {
                _this.setState({spinner: false});
            }
        } catch (error) {
            _this.setState({spinner: false});
            error.json().then((err) => {
            });
            _this.setState({spinner: false});
        }
    }

    saveTrade = async () => {
        var _this = this
        _this.setState({
            gridModalVisible: false,
            spinner: true
        })
        if (_this.state.allSelectedItemList[0] === undefined) {
            _this.setState({spinner: false});
            alert('Please select at least one item for trade')
        } else {
            let body = {
                "trade_item_id": _this.state.allSelectedItemList[0],
                "tradewith": [_this.state.itemId]
            }
            try {
                let response = await api.request('/trade', 'POST', body);

                if (response.status === 200) {
                    response.json().then((data) => {
                // ToDO  save channel
                        _this.save_channel();

                        // _this.setState({
                        //     spinner: false
                        // })
                        // navigate.navigateWithReset(_this.props.navigation, 'TradeFindr')

                    });
                } else {
                    _this.setState({spinner: false});
                }
            } catch (error) {
                _this.setState({spinner: false});
            }
        }
    }


    selectedItem = (selected_data) => {
        let index = this.state.allSelectedItemList.indexOf(selected_data);
        if (index === -1) {
            this.setState({allSelectedItemList: [selected_data]})
        } else {
            let array = [...this.state.allSelectedItemList]; // make a separate copy of the array
            array.splice(index, 1);
            this.setState({allSelectedItemList: array});
        }
    };

    showFullImage(picture, index){
        this.setState({fullImagePic:picture, imageIndex:index})
    }

    closeCodeView(){
        this.setState({fullImagePic:''})
    }

    makeOffer = () => {
        this.setState({gridModalVisible: true})
    };

    onStartShouldSetResponderCapture = () => {
        this.setState({ enableScrollViewScroll: true });
      };
      onStartShouldSetResponderCapture = () => {
        this.setState({ enableScrollViewScroll: false });
        if (
          this.refs.myList.scrollProperties.offset === 0 &&
          this.state.enableScrollViewScroll === false
        ) {
          this.setState({ enableScrollViewScroll: true });
    }
}

    render() {

        return (
            <View style={{flex: 1, backgroundColor: 'white'}}>
             {
                        this.state.fullImagePic.length > 1 ? (
                            
                                <Modal animationType="slide" transparent={true} visible= {true} onRequestClose={() => this.closeCodeView()}>
                                    <View style={{
                                        height:'100%',
                                        backgroundColor: 'rgba(0,0,0,0.90)',
                                    }}>     


                                    <TouchableOpacity style={{justifyContent:'flex-end', alignItems:'flex-end', height: 50, marginRight:10}}
                                            onPress={()=> this.closeCodeView()}
                                        >
                                             <Image style={{width: 20, height: 20, backgroundColor: 'black'}}
                                                    source={require('../../assets/close-white.png')}
                                                     resizeMode='contain'
                                                    />
                                         </TouchableOpacity>  

                                                <View style={{ flex:1}}>
                                                    <Swiper
                                                     index={this.state.imageIndex}
                                                     loop={false}
                                                     dot={<View style={{
                                                        backgroundColor: '#4191AA',
                                                        width: 10,
                                                        height: 10,
                                                        borderRadius: 5,
                                                        marginLeft: 3,
                                                        marginRight: 3,
                                                        marginTop: 3,
                                                        marginBottom: 3,
                                                    }}/>}
                                                    activeDot={<View style={{
                                                        backgroundColor: '#14DFC1',
                                                        width: 16,
                                                        height: 16,
                                                        borderRadius: 8,
                                                        marginLeft: 3,
                                                        marginRight: 3,
                                                        marginTop: 3,
                                                        marginBottom: 3,
                                                    }}/>}
                                                    > 
                                                        {
                                                    (this.state.item == null) ? (
                                                    <View></View>) : (this.state.item.pictures.map((item, key) => {
                                                        return (
                                                             <Image style={{width: '100%', height: '95%', backgroundColor: 'black'}}
                                                                   source={{uri: item.picture}}
                                                                   resizeMode='contain'
                                                            />
                                                            )
                                                       }))
                                                    }
                                                    </Swiper>

                                                </View>

                                        {/* <TouchableOpacity style={{justifyContent:'center', alignItems:'center'}}  onPress={()=> this.closeCodeView()}>
                                            <Image style={{width:width-40, height:height-80, marginTop:40}}
                                                        source={{uri: this.state.fullImagePic}}
                                                        resizeMode='contain'
                                                />
                                        </TouchableOpacity> */}
                                    </View>
                                </Modal> 
                        )
                         : (null)
             }
             <View style={{height: 64, backgroundColor: 'rgba(52, 219, 184, 1)', flexDirection: 'row'}}>
                    <View style={{flex: 1}}>
                        <TouchableOpacity activeOpacity={0.5}
                                          onPress={() => this.props.navigation.goBack(null)}>
                            <Image source={require('../../assets/back.png')}
                                   style={{marginLeft: 15, height: 30, width: 18, marginTop: 27}}
                                   resizeMode='stretch'
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={{flex: 1, marginTop: 15, justifyContent: 'center', alignItems: 'center'}}>
                        <BoldText style={{
                            fontWeight: '700',
                            fontSize: 20,
                            justifyContent: 'center',
                            textAlign: 'center',
                            alignSelf: 'center',
                            color: '#fff',
                            fontFamily: "NolanNext-Bold", width:180
                        }}>Item Details</BoldText>
                    </View>
                    <View style={{flex: 1, marginTop: 15, justifyContent: 'center', alignItems: 'flex-end'}}>
                        {/* <TouchableOpacity
                            onPress={() => {
                                this.flaggingItem(this.state.flagItem)
                            }}
                            activeOpacity={0.5}>
                            <View style={{flexDirection: 'row'}}>
                                <View>
                                    <RegularText style={{
                                        fontSize: 14,
                                        fontWeight: '700',
                                        color: '#fff'
                                    }}>{this.state.flagItem ? 'UnFlag \nItem' : 'Flag \nItem'}</RegularText>
                                </View>
                                {this.state.flagItem ?
                                    <Image
                                        source={require('../../assets/flag-color.png')}
                                        style={{marginRight: 15, height: 25, top: 3, width: 25, resizeMode: 'contain'}}
                                    />
                                    :
                                    <Image
                                        source={require('../../assets/flag-blank.png')}
                                        style={{
                                            marginRight: 15,
                                            height: 25,
                                            top: 3,
                                            width: 25,
                                            resizeMode: 'contain'
                                        }}/>
                                }
                            </View>
                        </TouchableOpacity> */}
                    </View>
                </View>
               
                <View style={styles.scrollContainer}
                    onStartShouldSetResponderCapture={() => {
                        this.setState({ enableScrollViewScroll: true });
                    }}
                    >
                <ScrollView scrollEnabled={this.state.enableScrollViewScroll}>
                    <View style={{ height:Dimensions.get('window').height + 100 , backgroundColor: 'white'}}>

                    <View style={{flex: 1, backgroundColor: 'white'}}>
                   
                                        <View style={{flexDirection: 'row', height: '100%', width: '100%'}}>
                                            <Swiper
                                                horizontal={true}
                                                autoplay={false}
                                               // autoplayTimeout={1}
                                                showsPagination={true}
                                                loop={false}
                                                dot={<View style={{
                                                    backgroundColor: '#4191AA',
                                                    width: 10,
                                                    height: 10,
                                                    borderRadius: 5,
                                                    marginLeft: 3,
                                                    marginRight: 3,
                                                    marginTop: 3,
                                                    marginBottom: 3,
                                                }}/>}
                                                activeDot={<View style={{
                                                    backgroundColor: 'red',
                                                    width: 16,
                                                    height: 16,
                                                    borderRadius: 8,
                                                    marginLeft: 3,
                                                    marginRight: 3,
                                                    marginTop: 3,
                                                    marginBottom: 3,
                                                }}/>}
                                                buttonWrapperStyle={{
                                                    backgroundColor: 'transparent',
                                                    flexDirection: 'row',
                                                    position: 'absolute',
                                                    top: 0,
                                                    left: 0,
                                                    flex: 1,
                                                    paddingHorizontal: 10,
                                                    paddingVertical: 10,
                                                    justifyContent: 'space-between',
                                                    alignItems: 'center'
                                                }}
                                                nextButton={<View
                                                    style={[styles.triangle, {transform: [{rotate: '90deg'}]}]}/>}
                                                prevButton={<View
                                                    style={[styles.triangle, {transform: [{rotate: '-90deg'}]}]}/>}
                                                showsButtons
                                            >
                                                {
                                                (this.state.item == null) ? (
                                                    <View></View>) : (this.state.item.pictures.map((item, key) => {
                                                    return (
                                                        <TouchableWithoutFeedback key={key} style={{alignItems: 'center'}} 
                                                                           onPress={()=>this.showFullImage(item.picture,key)}>
                                                            <Image style={{width: '96%', height: '100%', backgroundColor: 'black', marginHorizontal:'2%'}}
                                                                   source={{uri: item.picture}}
                                                                   resizeMode='contain'
                                                            />
                                                        </TouchableWithoutFeedback>
                                                    )
                                                }))
                                                }
                                            </Swiper>
                                        </View>
                    </View>
                    
                        <View style={{
                            width: '96%',
                            borderWidth: 1,
                            borderColor: 'gray',
                            borderRadius: 5,
                            borderTopWidth: 0,
                            marginLeft: '2%'
                        }}>
                            <View style={{flexDirection: 'row'}}>
                                  <TouchableOpacity style={{
                                top:0,
                                left:10,
                                    height: 66,
                                    width: 66,
                                    borderRadius: 33,
                                    backgroundColor: 'white',
                                    position:'absolute',
                                    justifyContent:'center',
                                    alignItems:'center'
                                }} onPress={() => {
                                    this.props.navigation.navigate("ProfileNew", {userId: this.state.user.id})
                                }}>
                                    {this.state.user ? (this.state.user.picture ? (
                                         (<Image source={{uri: this.state.user.picture}}
                                            style={{height: 60, width: 60, borderRadius: 30, margin: 0}}/>
                                    )
                                    ) : (<Image source={require('../../assets/userProfile.png')}
                                    style={{height: 60, width: 60, borderRadius: 30, margin: 0}}/>))
                                        :
                                        ((<Image source={require('../../assets/userProfile.png')}
                                                 style={{height: 60, width: 60, borderRadius: 30, margin: 0}}/>
                                        ))
                                    }
                                </TouchableOpacity>
                                <View style={{flexDirection: 'row', width: width - 100, marginLeft: 78}}>
                                    <View style={{width: '62%'}}>
                                        <BoldText style={{
                                            fontSize: 22,
                                            color: '#626464',
                                            marginTop: 0
                                        }}>{this.state.user ? this.state.user.first_name : ''}</BoldText>
                                        <RegularText style={{
                                            width: '100%',
                                            color: 'gray',
                                            marginTop: '1%'
                                        }}>{this.state.user ? this.state.user.city + ',' + this.state.user.state : ''}</RegularText>
                                    </View>
                                    <View style={{width: '30%', alignItems: 'flex-end'}}>
                                        <View style={{width: '100%', marginTop: '5%'}}>
                                            <StarRating
                                                disabled={(this.props.navigation.state.params.myItem) ? true : false}
                                                emptyStar={'ios-star-outline'}
                                                fullStar={'ios-star'}
                                                halfStar={'ios-star-half'}
                                                iconSet={'Ionicons'}
                                                maxStars={5}
                                                starSize={15}
                                                width={50}
                                                rating={this.state.user ? Number(this.state.user.averageRating) : 0}
                                                selectedStar={(rating) => this.onStarRatingPress(rating)}
                                                fullStarColor={'#14DFC1'}
                                            />
                                        </View>
                                        <View>
                                            <ItalicText numberOfLines={1} style={{
                                                fontSize: 9,
                                                fontStyle: 'italic',
                                                textAlign: 'right',
                                                marginTop: '3%'
                                            }}>posted {this.state.item ? this.state.item.days : ''} days
                                                ago</ItalicText>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={{ width: '96%', flexDirection: 'row', marginLeft: '2%', marginBottom:10, marginTop:20}}>
                                <View style={{flexDirection: 'column'}}>
                                    <View style={{flexDirection: 'row'}}>
                                        <BoldText style={{
                                            width: '50%',
                                            fontSize: 22,
                                            color: '#626464'
                                        }}>{this.state.item ? this.state.item.title : ''}</BoldText>
                                        <View style={{flexDirection: 'column', width:'50%'}}>
                                               <RegularText
                                                   style={{fontSize: 15, textAlign:'right', width:'100%'}}>{this.state.item ? 'Value:$' + this.state.item.min_value + ' - $' + this.state.item.max_value : 'Value:'}</RegularText>
                                               <RegularText
                                               style={{fontSize: 15, textAlign:'right', width:'100%'}}>{this.state.item ? this.state.item.category.name : 'No category'}</RegularText>
                                       </View>
                                    </View>
                                    <View
                                         onStartShouldSetResponderCapture={() => {
                                            this.setState({ enableScrollViewScroll: false });
                                            if (this.state.enableScrollViewScroll === false) {
                                            this.setState({ enableScrollViewScroll: true });
                                            }
                                        }}
                                        style={{
                                            width: '100%',
                                            height: 50,
                                        }}
                                        >
                                    <ScrollView>
                                        <RegularText style={{color: 'gray', marginTop: '1%'}}
                                                     >{this.state.item ? this.state.item.description : ''}</RegularText>
                                    </ScrollView>
                                    </View>
                                </View>
                            </View>
                        </View>


                        <View style={{
                            height: 50,
                            margin: '2%',
                            borderWidth: 1,
                            borderRadius: 5,
                            borderColor: 'gray',
                            alignItems: 'center',
                            flexDirection: 'row'
                        }}>
                            <View style={{width: '25%', height: '100%', justifyContent: 'center'}}>
                                <ItalicText numberOfLines={1} style={{
                                    fontSize: 12,
                                    fontStyle: 'italic',
                                    textAlign: 'left',
                                    marginLeft: '5%'
                                }}>Wishlist items:</ItalicText>
                            </View>
                            <View style={{width: '75%', height: '100%'}}>
                                {
                                    this.state.user ? (<FlatList style={{height: 110, width: '98%',}}
                                                                 horizontal={true}
                                                                 showsHorizontalScrollIndicator={false}
                                                                 data={this.state.user.wishlist}
                                                                 keyExtractor={(item) => item.id}
                                                                 renderItem={({item, index}) =>
                                                                     <View style={{
                                                                         justifyContent: 'center',
                                                                         backgroundColor: '#12BFDF',
                                                                         borderRadius: 5,
                                                                         margin: 5
                                                                     }}>
                                                                         <BoldText style={{
                                                                             fontSize: 15,
                                                                             height: '90%',
                                                                             marginLeft: 5,
                                                                             marginRight: 5,
                                                                             color: 'white'
                                                                         }}>{item.name}</BoldText>
                                                                     </View>
                                                                 }
                                    />) : (null)
                                }

                            </View>

                        </View>

                        <View style={{height: 150, marginLeft: '2%', marginRight: '2%', borderRadius: 5}}>
                            {this.state.user ? (
                                <MapView
                                    style={{alignSelf: 'stretch', height: '100%', borderRadius: 5}}
                                    region={{
                                        latitude: this.state.user.latitude ? parseFloat(this.state.user.latitude) : 0,
                                        longitude: this.state.user.longitude ? parseFloat(this.state.user.longitude) : 0,
                                        latitudeDelta: 0.0922,
                                        longitudeDelta: 0.0421,
                                    }}
                                >
                                    <MapView.Marker
                                        coordinate={{
                                            latitude: this.state.user.latitude ? parseFloat(this.state.user.latitude) : 0,
                                            longitude: this.state.user.longitude ? parseFloat(this.state.user.longitude) : 0,
                                        }}>
                                        <Image source={require('../../assets/star.png')} style={{width: 50, height: 50}}/>
                                    </MapView.Marker>


                                </MapView>
                            ) : (null)
                            }
                        </View>
                        {(this.props.navigation.state.params.myItem) ? (
                        <View style={{flex:0.15}}>

                        </View>
                        ) :(
                            <View style={{flexDirection: 'row', flex: 1}}>
                     <TouchableOpacity
                        style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#fff',
                            marginBottom:10,
                            marginTop: 20,
                            height: 45,
                            marginLeft: '5%',
                            borderColor:'#14DFC1',
                            width:'45%',
                            borderWidth:1
                        }}
                        onPress={this.addToWishlist.bind(this)}>
                        <Text style={{
                            fontSize: 18, color: '#14DFC1',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Bold"
                        }}>Add to Wishlist</Text>
                     </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#14DFC1',
                            marginBottom:10,
                            marginTop: 20,
                            height: 45,
                            marginRight: '5%',
                            marginLeft: '5%',
                            borderColor:'#14DFC1',
                            width:'40%',
                            borderWidth:1
                        }}
                        onPress={this.makeOffer.bind(this)}>
                        <Text style={{
                            fontSize: 18, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Bold"
                        }}>Make Offer</Text>
                </TouchableOpacity>
                </View>
                        )}

                        
                            {this.state.loading ?
                                <ActivityIndicator
                                    style={{zIndex: 2, position: 'absolute', bottom: 0, top: 0, alignSelf: 'center'}}
                                    color='#00DBBB' size='large'/> : null
                            }
                            <GridModalBrowse gridModalVisible={this.state.gridModalVisible}
                                             setGridModalVisible={this.setGridModalVisible}
                                             itemList={this.state.itemDetailValues}
                                             done={this.saveTrade}
                                             selectedGridValue={this.selectedItem}
                                             allSelectedItemList={this.state.allSelectedItemList}
                                             makeOffer
                            />

                            
                            <Spinner
                                visible={this.state.spinner}
                                textContent={"Loading..."}
                                color='#00DBBB'
                                overlayColor='rgba(0, 0, 0, 0.6)'
                                textStyle={{color: '#00DBBB'}}
                            />

                       
                    </View>
                </ScrollView>
                </View>
            </View>
        );
       
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#ffffff',
        height: Dimensions.get('window').height - 64,
        width: Dimensions.get('window').width
    },
    icon: {
        width: 23,
        height: 23,
    },
    mapsContainer: {
        top: 25,
        width: '90%',
        marginLeft: '5%',
        marginRight: '5%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ecf0f1',
        borderWidth: 1,
        borderColor: '#ecf0f1',
        borderRadius: 10
    }
});
