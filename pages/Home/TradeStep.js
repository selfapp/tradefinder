import React, {Component} from 'react';
import {
    StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, AsyncStorage,
    FlatList
} from 'react-native';
import StarRating from 'react-native-star-rating';
import {Icon} from 'native-base';
import navigate from '../../Components/navigate';
import Spinner from 'react-native-loading-spinner-overlay';
import DefaultImage from '../../Components/DefaultImage';

export default class TradeStep extends Component {

            //Create Top Navigation Bar 
    static navigationOptions = ({navigation}) => ({
        headerTitle: 'Trade Route',
        tabBarLabel: 'Home',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../../assets/tab2.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => navigate.navigateWithReset(navigation, 'TradeFindr')}
            >
                <Image
                    source={require('../../assets/back.png')}
                    style={{marginLeft: 15, height: 30, width: 18}}
                    resizeMode='stretch'
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity
                onPress={() => navigation.navigate('ProfileContainer')}
                activeOpacity={0.5}>
                <Image
                    source={require('../../assets/Profile.png')}
                    style={{marginRight: 15, height: 35, width: 35}}
                />
            </TouchableOpacity>
        ),
        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 2,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#fff'
            }
    });

    constructor(props) {
        super(props);
        this.state = {
            starCount: 3.5,
            itemChain: [],
            showItemChain: false,
            tradeLastItem: {},
            showlastItem: false,
            userDetailValues: {},
            showUserDetail: false,
            spinner: false,
            createNoOffer: false
        }
    }

    componentDidMount() {


        if (this.props.navigation.state.params.myTradeRouteItem.offer !== null) {
            if (this.props.navigation.state.params.myTradeRouteItem.id === this.props.navigation.state.params.myTradeRouteItem.offer.offer_created_with) {
                this.setState({
                    createNoOffer: true
                })
            }
        }
        if (this.props.navigation.state.params.item.tradematch !== undefined) {
            if (this.props.navigation.state.params.item.tradematch[0] != null) {
                if (this.props.navigation.state.params.item.tradematch[0][0] != null) {
                    var itemChain = this.props.navigation.state.params.item.tradematch[0]
                    itemChain = itemChain.slice(0, -1);
                    this.setState({
                        itemChain: itemChain,
                        showItemChain: true
                    })
                } else {
                }
            }
        }
        if (this.props.navigation.state.params.itemSend === 'item') {
            this.setState({
                showlastItem: true,
                tradeLastItem: this.props.navigation.state.params.item.item
            })
        } else if (this.props.navigation.state.params.itemSend === 'trade_with') {
            this.setState({
                showlastItem: true,
                tradeLastItem: this.props.navigation.state.params.item.trade_with
            });
        }
        this.getUserDetailFromLocalDatabase()
    }

    // Get user details from local 
    async getUserDetailFromLocalDatabase() {
        var _this = this
        _this.setState({
            spinner: true
        })
        await AsyncStorage.getItem('userDetail').then(data => {
            var detailvalue = JSON.parse(data);
            _this.setState({
                userDetailValues: detailvalue,
                showUserDetail: true,
                spinner: false
            })
        });
    }

    // Go to offer Detail screen
    goToOfferDetailScreen = (key, item) => {
        this.setState({
            spinner: true
        })
        let itemSend = ''
        if (this.props.navigation.state.params.itemSend === 'trade_with') {
            itemSend = 'trade_with';
        } else {
            if (key === 'my_item') {
                itemSend = 'my_item';
            } else {
                itemSend = (key == 'item' || key == 'my_item') ? key : (this.state.showItemChain) ? 'item_chain' : this.props.navigation.state.params.itemSend;
            }
        }
        let myTradeRouteItem = this.props.navigation.state.params.myTradeRouteItem
        this.setState({
            spinner: false
        })
        navigate.navigateTo(this.props.navigation, 'OfferDetails', {
            item: item,
            myTradeRouteItem: myTradeRouteItem,
            itemSend: itemSend
        })
    }

    // Desigin all steps
    _renderChain = () => {
        return (
            <FlatList
                data={this.state.itemChain}
                renderItem={({item, index}) => {
                    return (
                        <View>
                            <TouchableOpacity
                                onPress={() => {
                                    if (!this.state.createNoOffer) {
                                        alert('You can not complete all the steps.')
                                    } else {
                                        if (item.offer == null) {
                                            this.goToOfferDetailScreen('', item);
                                        } else {
                                            if (!item.offer.is_complete) {
                                                this.goToOfferDetailScreen('', item);
                                            } else {
                                                alert('No offer will be create for this item')
                                            }
                                        }
                                    }
                                }
                                }
                                style={{
                                    backgroundColor: 'white',
                                    width: '80%',
                                    height: 142,
                                    marginTop: '5%',
                                    marginLeft: '10%',
                                    marginRight: '10%',
                                    borderWidth: 0.5,
                                    borderRadius: 8,
                                }}>
                                <View style={{
                                    backgroundColor: '#FF555B',
                                    height: 20, alignItems: 'center',
                                    borderTopLeftRadius: 8,
                                    borderTopRightRadius: 8
                                }}>
                                    <Text style={{color: 'white'}}>
                                        Step {index + 1}
                                    </Text>
                                </View>
                                <View style={{flexDirection: 'row'}}>
                                    {
                                        item.trade_with.pictures[0].picture ?
                                            <Image
                                                resizeMode='contain'
                                                source={require('../../assets/arrow-dark.png')}
                                                source={{uri: item.trade_with.pictures[0].picture}}
                                                style={{marginLeft: '2%', marginTop: '5%', height: 40, width: 40}}
                                            /> :
                                            <DefaultImage
                                                defaultImageStyle={{
                                                    marginLeft: '2%',
                                                    marginTop: '5%',
                                                    height: 40,
                                                    width: 40
                                                }}
                                            />

                                    }

                                    <View>
                                        <Text style={{marginTop: 20, marginLeft: 20, fontWeight: '700', fontSize: 19}}>
                                            {item.trade_with.title}
                                        </Text>
                                        <View style={{flexDirection: 'row', marginTop: 20, marginLeft: 10}}>
                                            <View style={{width: '60%'}}>
                                                <Text
                                                    numberOfLines={1}
                                                >
                                                    {item.trade_with.user.first_name}
                                                </Text>
                                                <Text
                                                    numberOfLines={1}
                                                >
                                                    {item.trade_with.user.address}

                                                </Text>
                                            </View>
                                            <View style={{marginLeft: '7%'}}>
                                                {
                                                    item.trade_with.user.picture ?
                                                        <Image
                                                            source={{uri: item.trade_with.user.picture}}
                                                            style={{height: 46, width: 46, borderRadius: 23}}
                                                        /> :
                                                        <DefaultImage
                                                            defaultImageStyle={{
                                                                height: 46,
                                                                width: 46,
                                                                borderRadius: 23
                                                            }}
                                                        />
                                                }

                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            <View
                                style={{alignItems: 'center', marginTop: '2%'}}>
                                <Image
                                    source={require('../../assets/arrow-red.png')}
                                    style={{height: 18, width: 18}}
                                />
                            </View>
                        </View>
                    )
                }
                }
            />
        )
    }

    // Main acreen UI Desigin
    render() {
        return (
            <ScrollView style={styles.container}>
                <TouchableOpacity style={{
                    height: 50, marginLeft: '10%', marginRight: '10%', width: '80%',
                    marginTop: '5%', alignItems: 'center',
                    backgroundColor: '#FF555B', borderRadius: 8
                }}>
                    <Text style={{color: 'white', fontSize: 19, marginTop: '4%', height: 50}}>
                        Steps To Complete
                    </Text>
                </TouchableOpacity>
                {
                    this.state.showUserDetail ?
                        (<TouchableOpacity
                                onPress={
                                    () => {
                                        if (this.props.navigation.state.params.myTradeRouteItem.offer !== null) {
                                            if (!this.props.navigation.state.params.myTradeRouteItem.offer.is_complete) {
                                                this.goToOfferDetailScreen('my_item', this.props.navigation.state.params.myTradeRouteItem)
                                            } else {
                                                alert('Offer is already completed for your item.')
                                            }
                                        } else {
                                            alert('No offer is created for your item.')
                                        }
                                    }}
                                style={{
                                    backgroundColor: 'white',
                                    width: '80%',
                                    height: 142,
                                    marginTop: '5%',
                                    marginLeft: '10%',
                                    marginRight: '10%',
                                    borderWidth: 0.5,
                                    borderRadius: 8,
                                }}>
                                <View style={{
                                    backgroundColor: '#626262',
                                    height: 20, alignItems: 'center',
                                    borderTopLeftRadius: 8,
                                    borderTopRightRadius: 8
                                }}>
                                    <Text style={{color: 'white'}}>
                                        My Item
                                    </Text>
                                </View>
                                <View style={{flexDirection: 'row'}}>
                                    {
                                        this.props.navigation.state.params.myTradeRouteItem.picture ?
                                            <Image
                                                resizeMode='contain'
                                                source={{uri: this.props.navigation.state.params.myTradeRouteItem.picture}}
                                                style={{marginLeft: '2%', marginTop: '5%', height: 60, width: 60}}
                                            /> :
                                            <DefaultImage
                                                defaultImageStyle={{
                                                    marginLeft: '2%',
                                                    marginTop: '5%',
                                                    height: 60,
                                                    width: 60
                                                }}
                                            />
                                    }
                                    <View style={{marginLeft: 10}}>
                                        <Text style={{marginTop: '5%', fontWeight: '700', fontSize: 19}}>
                                            {this.props.navigation.state.params.myTradeRouteItem.title}
                                        </Text>
                                        <View style={{flexDirection: 'row', marginTop: 20, marginLeft: 10}}>
                                            <View style={{width: '60%'}}>
                                                <Text
                                                    numberOfLines={1}
                                                >{this.state.userDetailValues.first_name}</Text>
                                                <Text
                                                    numberOfLines={1}
                                                >{this.state.userDetailValues.address}</Text>
                                            </View>
                                            <View style={{marginLeft: '7%'}}>
                                                {
                                                    this.state.userDetailValues.picture ?
                                                        <Image
                                                            source={{uri: this.state.userDetailValues.picture}}
                                                            style={{height: 46, width: 46, borderRadius: 23}}
                                                        /> :
                                                        <DefaultImage
                                                            defaultImageStyle={{
                                                                height: 46,
                                                                width: 46,
                                                                borderRadius: 23
                                                            }}
                                                        />
                                                }

                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        ) : (null)
                }
                <View
                    style={{alignItems: 'center', marginTop: '2%'}}>
                    <Image
                        source={require('../../assets/arrow-dark.png')}
                        style={{height: 18, width: 18}}
                    />
                </View>
                {
                    this.state.showItemChain ? (this._renderChain()) : (null)
                }
                {
                    this.state.showlastItem ? (
                        <View>
                            <TouchableOpacity
                                onPress={() => {
                                    if (this.state.showItemChain) {
                                        if (this.props.navigation.state.params.item.offer !== null) {
                                            if (this.state.itemChain[this.state.itemChain.length - 1].offer !== null) {
                                                if (this.state.itemChain[this.state.itemChain.length - 1].offer.is_complete) {
                                                    this.goToOfferDetailScreen('item', this.props.navigation.state.params.item)
                                                } else {
                                                    alert('You can not complete this step for now')
                                                }
                                            } else {
                                                alert('You can not complete this step for now')
                                            }
                                        } else {
                                            alert('You can not complete this step for now')
                                        }
                                    } else {
                                        this.goToOfferDetailScreen('item', this.props.navigation.state.params.item)
                                    }
                                }
                                }

                                style={{
                                    backgroundColor: 'white',
                                    width: '80%',
                                    height: 142,
                                    marginTop: '5%',
                                    marginLeft: '10%',
                                    marginRight: '10%',
                                    borderWidth: 0.5,
                                    borderRadius: 8,
                                }}>
                                <View style={{
                                    backgroundColor: '#FF555B',
                                    height: 20, alignItems: 'center',
                                    borderTopLeftRadius: 8,
                                    borderTopRightRadius: 8
                                }}>
                                    {
                                        this.state.showItemChain ? (
                                            <Text style={{color: 'white'}}>
                                                Step {this.props.navigation.state.params.item.tradematch[0].length}
                                            </Text>
                                        ) : (
                                            <Text style={{color: 'white'}}>
                                                Step 1
                                            </Text>
                                        )
                                    }
                                </View>
                                <View style={{flexDirection: 'row'}}>

                                    {this.state.tradeLastItem.picture ?
                                        <Image
                                            resizeMode='contain'
                                            source={{uri: this.state.tradeLastItem.picture}}
                                            style={{marginLeft: '2%', marginTop: '5%', height: 60, width: 60}}
                                        /> : <DefaultImage
                                            defaultImageStyle={{
                                                marginLeft: '2%',
                                                marginTop: '5%',
                                                height: 60,
                                                width: 60
                                            }}
                                        />
                                    }

                                    <View>
                                        <Text style={{marginTop: 20, marginLeft: 20, fontWeight: '700', fontSize: 19}}>
                                            {this.state.tradeLastItem.title}
                                        </Text>
                                        <View style={{flexDirection: 'row', marginTop: 20, marginLeft: 10}}>
                                            <View style={{width: '60%'}}>
                                                <Text numberOfLines={1}>
                                                    {this.state.tradeLastItem.user.first_name}
                                                </Text>
                                                <Text
                                                    numberOfLines={1}
                                                >
                                                    {this.state.tradeLastItem.user.address}

                                                </Text>
                                            </View>
                                            <View style={{marginLeft: '7%'}}>
                                                {
                                                    this.state.tradeLastItem.user.picture ?
                                                        <Image
                                                            source={{uri: this.state.tradeLastItem.user.picture}}
                                                            style={{height: 46, width: 46, borderRadius: 23}}
                                                        /> : <DefaultImage
                                                            defaultImageStyle={{height: 46, width: 46, borderRadius: 23}}
                                                        />
                                                }

                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    ) : (
                        null
                    )
                }
                <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.6)'
                    textStyle={{color: '#00DBBB'}}
                />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    icon: {
        width: 23,
        height: 23,
    }
});
