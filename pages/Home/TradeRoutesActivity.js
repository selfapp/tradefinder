import React, {Component} from 'react';
import {
    StyleSheet,
    View, 
    TouchableOpacity, 
    ScrollView, 
    FlatList, 
    Image, 
    AsyncStorage,
} from 'react-native';
import TradeRouteComponent from '../../Components/TradeRouteComponent';
import navigate from '../../Components/navigate';
import ChatEngineProvider from "../Chat/ce-core-chatengineprovider";
import ChatEngineProviderMe from "../Chat/ce-core-chatengineproviderme";
import Spinner from 'react-native-loading-spinner-overlay';
import {RegularText, BoldText} from '../../Components/styledTexts'


export default class TradeRoutesActivity extends Component {

    constructor(props) {
        super(props);
        this.state = {
            myItemPic: '',
            tradedArray: [],
            tradeWithArray: [],
            selectedItemIndex: 0
        }
    }


    componentDidMount() {
        console.log("trade routr activity ....")
        this.getUserDetailValues()
    }

// Get user deatils Values from local storage
    getUserDetailValues = async () => {
        var _this = this
        await AsyncStorage.getItem('userDetailValues').then(data => {
            _this.setState({
                myId: JSON.parse(data).user.id
            })
        });
    }

    // Get Item details
    getItemdetailValues = (item, index) => {
        this.setState({
            myItemPic: item.pictures[0].picture,
            tradedArray: item.trade,
            tradeWithArray: item.trade_with,
            selectedItemIndex: index
        })
        this.props.setTradeRoutebackButton(true)
    }

    // Navigate to Trade steps
    navigateTradeStep = (item, myTradeRouteItem, itemSend, channel_name, other_id) => {

        ChatEngineProvider.getChatRoomModel()
        .connect(channel_name)
        .then(() => {
          ChatEngineProviderMe.getChatRoomModel()
            .connect('notify' + other_id.toString(), false)
            .then(() => {
              let chats = [ChatEngineProviderMe._chatRoomModel.state.chat];
              AsyncStorage.getItem('deviceToken').then((token) => {
                ChatEngineProviderMe._chatEngine.me.notifications.enable(chats, token, (error) => {
                  console.log("enter into push enable........", error);
                  if (error !== null) {
                    Alert.alert(
                      'Push Notification error',
                      `Unable to enable notifications for global: ${error.message}`,
                      [{text: 'OK'}],
                      {cancelable: true}
                    );
                  } else {
                    // Enable message publish button.
                    console.log("ENABLED PUSH NOTIFICATION FOR PERSONAL CHANNEL");
                  }
                });
              });
              AsyncStorage.setItem('otherUserChannelName',JSON.stringify(other_id))

              navigate.navigateTo(this.props.navigation, "TradeStep", {
                item: item,
                myTradeRouteItem: myTradeRouteItem,
                itemSend: itemSend,
                title: "#" + channel_name
            }, (reject) => {
                console.log("alkerhkjre")
                console.log(reject)
                alert(reject);
            })
            });
        });
        // ChatEngineProvider.getChatRoomModel().connect(channel_name).then(() => {

        //     var chat =  ChatEngineProvider._chatRoomModel.state.chat;
        
        //     var pushToken = ''
        //     AsyncStorage.getItem('deviceToken').then((token)=> {
        //        // console.log("shjkdfkjhsaj")
        //         pushToken = token
        //         console.log("chat sjhfdkhsdfk jsh j")
        //         console.log(chat)
        //         console.log("Device token is "+ pushToken)
        //         let chats = [chat];
    
        //     ChatEngineProvider._chatEngine.me.notifications.enable(chats, pushToken, (error) => {
        //         console.log("enter into push enable........")
        //       if (error !== null) {
        //         Alert.alert(
        //           'Push Notification error',
        //           `Unable to enable notifications for global: ${error.message}`,
        //           [{ text: 'OK' }],
        //           { cancelable: true }
        //         );
        //       } else {
        //         // Enable message publish button.
        //         console.log("enavble push notification........")
        //       }
        //     });
        // })
        //     navigate.navigateTo(this.props.navigation, "TradeStep", {
        //         item: item,
        //         myTradeRouteItem: myTradeRouteItem,
        //         itemSend: itemSend,
        //         title: "#" + channel_name
        //     }, (reject) => {
        //         console.log("alkerhkjre")
        //         console.log(reject)
        //         alert(reject);
        //     })
        // });

    };

    // Navigate to offer detail screen
    gotoOfferDetailScreen = (item, itemSend) => {
        let myTradeRouteItem = this.props.myTradeRouteItem[this.state.selectedItemIndex];
        let channel_name = "";
        let my_id = this.state.myId;
        let other_id = (itemSend === 'item') ? item.item.user.id : item.trade_with.user.id;
        if (my_id < other_id) {
            channel_name = my_id + "-" + other_id;
            this.navigateTradeStep(item, myTradeRouteItem, itemSend, channel_name, other_id);
        } else {
            channel_name =  other_id + "-" + my_id;
            this.navigateTradeStep(item, myTradeRouteItem, itemSend, channel_name, other_id)
        }
    };

    // Ui Desigin 
    render() {
        console.log("wreh whrejkew hwejk")
        return (
            <ScrollView ref='scrollView' style={styles.scrollContainer}>
                <View style={styles.mainContainer}>
                    {this.props.showMyTradeRouteItemsDetail ?
                        (
                            <TradeRouteComponent
                                myItemPic={this.state.myItemPic}
                                tradeWithArray={this.state.tradeWithArray}
                                tradedArray={this.state.tradedArray}
                                offerDetailScreen={this.gotoOfferDetailScreen}
                            />
                        ) :
                        (
                            <FlatList
                                data={this.props.myTradeRouteItem}
                                renderItem={({item, index}) =>
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => {
                                                if (item.trades_count > 0) {
                                                    this.props.navigation.navigate('TradeRoutesList', {
                                                        tradeRoutes: this.props.myTradeRouteItem,
                                                        index: index
                                                    })
                                                }
                                            }}
                                            style={{
                                                height: 70,
                                                marginTop: '2%',
                                                marginLeft: '5%',
                                                marginRight: '5%',
                                                borderRadius: 8,
                                                borderColor: 'gray',
                                                borderWidth: 1,
                                                width: '90%',
                                                flexDirection: 'row'
                                            }}>
                                            <View style={{width: '65%', height: '100%', flexDirection: 'row'}}>
                                                {item.item ?
                                                    <View style={{
                                                        width: '30%',
                                                        height: '90%',
                                                        justifyContent: 'center',
                                                    }}>
                                                        <Image style={{margin: '10%', height: '80%', width: '80%'}}
                                                               source={{uri: item.item.pictures[0].picture}}/>
                                                    </View>
                                                    : (null)
                                                }
                                                <View
                                                    style={{marginLeft: '3%', width: '60%', justifyContent: 'center'}}>
                                                    <RegularText numberOfLines={1}
                                                                 style={{fontSize: 12}}>{item.category.name}</RegularText>
                                                    <BoldText numberOfLines={1} style={{
                                                        marginTop: '1%',
                                                        fontSize: 12,
                                                        color: '#000000',
                                                        fontWeight: '600'
                                                    }}>{item.name}</BoldText>
                                                </View>
                                            </View>
                                            <View style={{width: '35%', flexDirection: 'row', alignItems: 'flex-end',}}>
                                                <RegularText style={{fontSize: 10, alignSelf: 'center'}}>
                                                    See Trade Routes
                                                </RegularText>
                                                <RegularText style={{
                                                    fontSize: 18,
                                                    marginLeft: '5%',
                                                    color: 'red',
                                                    alignSelf: 'center'
                                                }}>
                                                    {item.trades_count}
                                                </RegularText>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                }/>
                        )
                    }
                </View>
                <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.6)'
                    textStyle={{color: '#00DBBB'}}
                />
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#ffffff',
        flex: 1
    }
});
