import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    AsyncStorage,
    Platform,
} from 'react-native';
import TradesList from './TradesList';
import navigate from '../Components/navigate';
import api from '../api';


export default class Trades extends Component {

    static navigationOptions = ({navigation}) => ({
        headerTitle: 'Trades',
        tabBarLabel: 'Trades',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../assets/trades.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() =>{
                                  AsyncStorage.getItem('profileCompleted').then((value) => {
                                      value === '1' ? navigation.navigate('PostItem') : alert('Please create a profile before posting the item.')
                                  });
                                  }}
            >
                <Image
                    source={require('../assets/white_plus_icon.png')}
                    style={{marginLeft: 15, height: 28, width: 28}}
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity
                onPress={() => navigation.navigate('Offers')}
                activeOpacity={0.5}>
                <Image
                    source={require('../assets/chat.png')}
                    style={{marginRight: 15, height: 35, width: 35}}
                />
            </TouchableOpacity>
        ),
        
        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                    fontSize: 20,
                    marginTop: 2,
                    flex: 1,
                    justifyContent: 'center',
                    textAlign: 'center',
                    alignSelf: 'center',
                    color: '#fff',
                    fontFamily: "NolanNext-Bold",
            }
    });

constructor(props){
    super(props)
    this.state = {
    completeData: '',
    userId: '',
}
}
componentDidMount(){
    this.getUserDetailValues();
}

// Get User Details From Local Storage 
async getUserDetailValues() {
    var _this = this;
    _this.setState({
        spinner: true
    });
    await AsyncStorage.getItem('userDetailValues').then(data => {
        var detailvalue = JSON.parse(data);
        _this.state.userId = detailvalue.user.id;
        _this.getPageContent(this.state.userId);
    });
}

 // Get User Full Details 
 async getPageContent(userId) {
    var _this = this;
    let response = await api.getRequest('/users/' + userId, 'GET');
    if (response.status === 200) {
        response.json().then(function (data) {
            _this.setState({
                spinner: false,
                completeData: data,
            });
            console.log(data.user)
            if(data.user.latitude !== null) {
                // Store Flag Value for profile completion 
                AsyncStorage.setItem('profileCompleted', '1');
                _this.setState({
                    profileCompleted: true
                });
            }
        });
    } else {
        _this.setState({
            spinner: false
        });
    }
}

render(){

   return(                 
        <View>
                        <TradesList
                            navigateToTradeStep={(obj) => navigate.navigateTo(this.props.navigation, 'TradeDetails', {
                                object: obj,
                                completeUserData: this.state.completeData
                            })}

                            navigateToOfferDetails={(obj, index) => navigate.navigateTo(this.props.navigation, 'OfferDetails', {
                                myItem: obj,
                                isOfferCreated: index
                            })}
                        />
                    </View>
   );
}

}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#ffffff'
    },
   
    container: {
        flex: 1,
        width: null,
        height: null,
    },
    icon: {
        resizeMode: 'contain'
    },
    tabStyle: {
        borderColor: '#D52C43'
    },
    activeTabStyle: {
        backgroundColor: '#D52C43'
    },
    tabTextStyle: {
        color: '#D52C43'
    },
    modalBackground: {
        flex: 1,
        backgroundColor: '#00000040'
    },
});