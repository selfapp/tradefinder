import React, {Component} from 'react';
import {
    StyleSheet, FlatList, Dimensions, View, Text,
    ActivityIndicator, Alert, Modal,
    Image, TouchableOpacity, ScrollView, TouchableWithoutFeedback
} from 'react-native';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
import StarRating from 'react-native-star-rating';
import MapView from 'react-native-maps';
import Button from '../Components/button';
import navigate from '../Components/navigate';
import Spinner from 'react-native-loading-spinner-overlay';
import api from '../api';
import GridModalBrowse from '../Components/GridModalBrowse';
import Swiper from 'react-native-swiper';
import {BoldText, RegularText, ItalicText} from '../Components/styledTexts';

export default class ItemDetails extends Component {
    static navigationOptions = ({navigation}) => ({
        headerTitle: 'Item Details',
        tabBarLabel: 'Item Detail',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../assets/tab2.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => navigation.goBack(null)}
            >
                <Image
                    source={require('../assets/back.png')}
                    style={{marginLeft: 15, height: 30, width: 18}}
                    resizeMode='stretch'
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity style={{marginRight: 15, height: 40, width: 40}}
                activeOpacity={0.0}>
            </TouchableOpacity>
        ),

        // headerRight: (
        //     navigation.state.params.myItem ?
        //         <TouchableOpacity
        //             // onPress={() => navigation.navigate('PostItem')}
        //             activeOpacity={0.5}>
        //             <Image
        //                 source={require('../assets/pencil.png')}
        //                 style={{marginRight: 15, height: 30, width: 30}}
        //             />
        //         </TouchableOpacity> : null
        // ),
        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 2,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#fff',
                fontFamily: "NolanNext-Bold",
            }
    });

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            starCount: 0,
            itemId: this.props.navigation.state.params.itemId,
            userAddress: '',
            usersLatitude: 0,
            usersLongitude: 0,
            userName: '',
            allSelectedItemList: [],
            item_pic_uri: "",
            user_profile_pic_uri: require('../assets/userProfile.png'),
            itemDetailValues: [],
            itemName: '',
            itemDescription: '',
            itemPostedDays: '',
            mapRegion: null,
            lastLat: 0,
            lastLong: 0,
            gridModalVisible: false,
            userId: 0,
            item: props.navigation.state.params.item,
            min_value: 0, max_value: 0,
            itemImage: "",
            data: null, swipeImageIndex:0, fullImagePic:'', enableScrollViewScroll:true,
            imageIndex:0
        }
    }

    componentDidMount() {

        this.watchID = navigator.geolocation.watchPosition((position) => {
            let region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: 0.00922 * 1.5,
                longitudeDelta: 0.00421 * 1.5
            };
            this.onRegionChange(region, region.latitude, region.longitude);
        });
        this.getPageContent();
        this.getItemDetailValues();
    }

    onRegionChange(region, usersLatitude, usersLongitude) {
        this.setState({
            mapRegion: region,
            lastLat: usersLatitude || this.state.usersLatitude,
            lastLong: usersLongitude || this.state.usersLongitude
        });
    }

    componentWillUnmount() {
        this.setState({fullImagePic:''})
        navigator.geolocation.clearWatch(this.watchID);
    }

    setGridModalVisible = (isVisible) => {
        this.setState({
            gridModalVisible: isVisible
        })
    }

    async getPageContent() {
        var _this = this
        _this.setState({
            spinner: true
        })
        try {
            let response = await api.getRequest('/items/' + _this.state.itemId, 'GET');
            if (response.status === 200) {
                response.json()
                    .then(function (data) {
                        let averageRating = Math.round(data.user.averageRating);
                        _this.setState({spinner: false, starCount: Number(averageRating), data: data});
                    });
            } else {
                _this.setState({
                    loading: false
                });
            }
        } catch (error) {
            _this.setState({
                loading: false
            });
        }
    }

    async removeItem() {
        var _this = this;
        _this.setState({
            loading: true
        });
        try {
            let response = await api.getRequest('/item/' + _this.state.itemId + '/delete', 'DELETE');
            if (response.status === 200) {
                response.json()
                    .then(function (data) {
                        _this.setState({
                            loading: false
                        });
                        alert(data.msg);
                         _this.props.navigation.navigate("TradeFindr");
                        // _this.props.navigation.navigate("ProfileContainer");
                    })
            } else {
                _this.setState({
                    loading: false
                });
            }
        } catch (error) {
            _this.setState({
                loading: false
            });
        }
    }

    confirmDeleteItem() {
        Alert.alert(
            'Delete',
            'Are you sure you want to delete this item?',
            [
                {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {
                    text: 'Yes', onPress: () => {
                        this.removeItem()
                    }
                }
            ],
            {cancelable: false})
    }

    async getItemDetailValues() {
        var _this = this
        let response = await api.getRequest('/user/items', 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    _this.setState({
                        spinner: false,
                        itemDetailValues: data.items
                    });
                });
        } else {
            _this.setState({
                spinner: false
            });
            alert(response);
        }
    }


    pressSelectItem() {
        this.props.navigation.state.params.delegate.onSelectedItem(this.state.item);
        this.props.navigation.goBack(null)
    }

    saveTrade = async () => {
        var _this = this
        _this.setState({
            gridModalVisible: false,
            spinner: true
        });
        if (_this.state.allSelectedItemList[0] === undefined) {
            alert('Please select at least one item for trade')
        } else {
            let body = {
                "trade_item_id": _this.state.allSelectedItemList[0],
                "tradewith": [_this.state.itemId]
            };
            try {
                let response = await api.request('/trade', 'POST', body);

                if (response.status === 200) {
                    response.json().then((data) => {
                        _this.setState({
                            spinner: false
                        })
                        navigate.navigateWithReset(_this.props.navigation, 'TradeFindr')

                    });
                } else {
                    _this.setState({spinner: false});
                }
            } catch (error) {
                _this.setState({spinner: false});
            }
        }
    };


    selectedItem = (selected_data) => {
        let index = this.state.allSelectedItemList.indexOf(selected_data);
        if (index === -1) {
            this.setState({allSelectedItemList: [selected_data]});
        } else {
            let array = [...this.state.allSelectedItemList]; // make a separate copy of the array
            array.splice(index, 1);
            this.setState({allSelectedItemList: array});
        }
    };

    makeOffer = () => {
        this.setState({gridModalVisible: true})
    };

    showFullImage(picture, index){
        this.setState({fullImagePic:picture, imageIndex:index})
    }

    closeCodeView(){
        this.setState({fullImagePic:''})
    }

    onStartShouldSetResponderCapture = () => {
        this.setState({ enableScrollViewScroll: true });
      };
      onStartShouldSetResponderCapture = () => {
        this.setState({ enableScrollViewScroll: false });
        if (
          this.refs.myList.scrollProperties.offset === 0 &&
          this.state.enableScrollViewScroll === false
        ) {
          this.setState({ enableScrollViewScroll: true });
    }
}

    render() {
    
        return (
            <View style={{flex: 1}}>
             {
                        this.state.fullImagePic.length > 1 ? (
                                <Modal animationType='slide' transparent={true} visible= {true} onRequestClose={() => this.closeCodeView()}>
                                        <View style={{
                                            height:'100%',
                                            backgroundColor: 'rgba(0,0,0,0.90)'
                                            }}>  
                                            <TouchableOpacity style={{justifyContent:'flex-end', alignItems:'flex-end', height: 50, marginRight:10}}
                                            onPress={()=> this.closeCodeView()}
                                            >
                                             <Image style={{width: 20, height: 20, backgroundColor: 'black'}}
                                                    source={require('../assets/close-white.png')}
                                                     resizeMode='contain'
                                                    />
                                                </TouchableOpacity>  

                                                <View style={{ flex:1}}>
                                                    <Swiper
                                                    index={this.state.imageIndex}
                                                    loop={false}
                                                    dot={<View style={{
                                                        backgroundColor: '#4191AA',
                                                        width: 10,
                                                        height: 10,
                                                        borderRadius: 5,
                                                        marginLeft: 3,
                                                        marginRight: 3,
                                                        marginTop: 3,
                                                        marginBottom: 3,
                                                    }}/>}
                                                    activeDot={<View style={{
                                                        backgroundColor: '#14DFC1',
                                                        width: 16,
                                                        height: 16,
                                                        borderRadius: 8,
                                                        marginLeft: 3,
                                                        marginRight: 3,
                                                        marginTop: 3,
                                                        marginBottom: 3,
                                                    }}/>}
                                                    > 
                                                        {
                                                    (this.state.data == null) ? (
                                                    <View></View>) : (this.state.data.item.pictures.map((item, key) => {
                                                        return (
                                                             <Image style={{width: '100%', height: '95%', backgroundColor: 'black'}}
                                                                   source={{uri: item.picture}}
                                                                   resizeMode='contain'
                                                            />
                                                            )
                                                       }))
                                                    }
                                                    </Swiper>

                                                </View>



                                                {/* <TouchableOpacity style={{justifyContent:'center', alignItems:'center'}} onPress={()=> this.closeCodeView()}>
                                                        <Image style={{width:width-40, height:height-80, marginTop:40}} resizeMode='contain'
                                                        source={{uri: this.state.fullImagePic}}/>
                                                </TouchableOpacity> */}
                                        </View>
                                </Modal> 
                      
                        ) : (null)
             }
               <View style={styles.scrollContainer}
                    onStartShouldSetResponderCapture={() => {
                        this.setState({ enableScrollViewScroll: true });
                    }}
                    >
                <ScrollView  scrollEnabled={this.state.enableScrollViewScroll}>
                    <View style={{ height:Dimensions.get('window').height + 65 , backgroundColor: 'white', marginBottom:20}}>

                    <View style={{flex: 1, backgroundColor: 'white'}}>
                   
                                        <View style={{flexDirection: 'row', height: '100%', width: '100%'}}>
                                            <Swiper
                                                horizontal={true}
                                                autoplay={false}
                                               // autoplayTimeout={1}
                                                showsPagination={true}
                                                loop={false}
                                                dot={<View style={{
                                                    backgroundColor: '#4191AA',
                                                    width: 10,
                                                    height: 10,
                                                    borderRadius: 5,
                                                    marginLeft: 3,
                                                    marginRight: 3,
                                                    marginTop: 3,
                                                    marginBottom: 3,
                                                }}/>}
                                                activeDot={<View style={{
                                                    backgroundColor: 'red',
                                                    width: 16,
                                                    height: 16,
                                                    borderRadius: 8,
                                                    marginLeft: 3,
                                                    marginRight: 3,
                                                    marginTop: 3,
                                                    marginBottom: 3,
                                                }}/>}
                                                buttonWrapperStyle={{
                                                    backgroundColor: 'transparent',
                                                    flexDirection: 'row',
                                                    position: 'absolute',
                                                    top: 0,
                                                    left: 0,
                                                    flex: 1,
                                                    paddingHorizontal: 10,
                                                    paddingVertical: 10,
                                                    justifyContent: 'space-between',
                                                    alignItems: 'center'
                                                }}
                                                nextButton={<View
                                                    style={[styles.triangle, {transform: [{rotate: '90deg'}]}]}/>}
                                                prevButton={<View
                                                    style={[styles.triangle, {transform: [{rotate: '-90deg'}]}]}/>}
                                                showsButtons
                                            >
                                                {
                                                (this.state.data == null) ? (
                                                    <View></View>) : (this.state.data.item.pictures.map((item, key) => {
                                                    return (
                                                        <TouchableWithoutFeedback key={key} style={{alignItems: 'center'}} 
                                                                           onPress={()=>this.showFullImage(item.picture, key)}>
                                                            <Image style={{width: '96%', height: '100%', backgroundColor: 'black', marginHorizontal:'2%'}}
                                                                   source={{uri: item.picture}}
                                                                   resizeMode='contain'
                                                            />
                                                        </TouchableWithoutFeedback>
                                                    )
                                                }))
                                                }
                                            </Swiper>
                                        </View>
                    </View>
                    
                        <View style={{
                           // height: '27%',
                            width: '96%',
                            borderWidth: 1,
                            borderColor: 'gray',
                            borderRadius: 5,
                            borderTopWidth: 0,
                            marginLeft: '2%'
                        }}>
                            <View style={{flexDirection: 'row'}}>
                                {/* <View style={{
                                    height: 66,
                                    width: 66,
                                    borderRadius: 33,
                                    backgroundColor: 'white',
                                    position:'absolute'
                                }}>
                                    {this.state.data ?
                                        (<Image source={{uri: this.state.data.user.picture}}
                                                style={{height: 60, width: 60, borderRadius: 30, margin: 0}}/>
                                        ) :
                                        ((<Image source={require('../assets/userProfile.png')}
                                                 style={{height: 60, width: 60, borderRadius: 30, margin: 3}}/>
                                        ))
                                    }
                                </View> */}
                                  <View style={{
                                top:0,
                                left:10,
                                    height: 66,
                                    width: 66,
                                    borderRadius: 33,
                                    backgroundColor: 'white',
                                    position:'absolute',
                                    justifyContent:'center',
                                    alignItems:'center'
                                }}>
                                    {this.state.data ?
                                        (<Image source={{uri: this.state.data.user.picture}}
                                                style={{height: 60, width: 60, borderRadius: 30, margin: 0}}/>
                                        ) :
                                        ((<Image source={require('../assets/userProfile.png')}
                                                 style={{height: 60, width: 60, borderRadius: 30, margin: 0}}/>
                                        ))
                                    }
                                </View>
                                <View style={{flexDirection: 'row', width: width - 100, marginLeft: 78}}>
                                    <View style={{width: '62%'}}>
                                        <BoldText style={{
                                            fontSize: 22,
                                            color: '#626464',
                                            marginTop: 0
                                        }}>{this.state.data ? this.state.data.user.first_name : ''}</BoldText>
                                        <RegularText style={{
                                            width: '100%',
                                            color: 'gray',
                                            marginTop: '1%'
                                        }}>{this.state.data ? this.state.data.user.city + ',' + this.state.data.user.state : ''}</RegularText>
                                    </View>
                                    <View style={{width: '30%', alignItems: 'flex-end'}}>
                                        <View style={{width: '100%', marginTop: '5%'}}>
                                            <StarRating
                                                disabled={true}
                                                emptyStar={'ios-star-outline'}
                                                fullStar={'ios-star'}
                                                halfStar={'ios-star-half'}
                                                iconSet={'Ionicons'}
                                                maxStars={5}
                                                starSize={15}
                                                width={50}
                                                rating={this.state.data ? Number(this.state.data.user.averageRating) : 0}
                                                fullStarColor={'#14DFC1'}
                                            />
                                        </View>
                                        <View>
                                            <ItalicText numberOfLines={1} style={{
                                                fontSize: 9,
                                                fontStyle: 'italic',
                                                textAlign: 'right',
                                                marginTop: '3%'
                                            }}>posted {this.state.data ? this.state.data.item.days : ''} days
                                                ago</ItalicText>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={{ width: '96%', flexDirection: 'row', marginLeft: '2%', marginBottom:10, marginTop:20}}>
                                <View style={{flexDirection: 'column'}}>
                                    <View style={{flexDirection: 'row'}}>
                                        <BoldText style={{
                                            width: '50%',
                                            fontSize: 22,
                                            color: '#626464'
                                        }}>{this.state.data ? this.state.data.item.title : ''}</BoldText>
                                        <View style={{flexDirection: 'column', width:'50%'}}>
                                               <RegularText
                                                   style={{fontSize: 15, textAlign:'right', width:'100%'}}>{this.state.data ? 'Value:$' + this.state.data.item.min_value + ' - $' + this.state.data.item.max_value : 'Value:'}</RegularText>
                                               <RegularText
                                               style={{fontSize: 15, textAlign:'right', width:'100%'}}>{this.state.data ? this.state.data.item.category.name : 'No category'}</RegularText>
                                       </View>
                                    </View>
                                  
                                    <View
                                         onStartShouldSetResponderCapture={() => {
                                            this.setState({ enableScrollViewScroll: false });
                                            if (this.state.enableScrollViewScroll === false) {
                                            this.setState({ enableScrollViewScroll: true });
                                            }
                                        }}
                                        style={{
                                            width: '100%',
                                            height: 50,
                                        }}
                                        >
                                            <ScrollView >
                                                <RegularText style={{color: 'gray', marginTop: '1%'}}
                                                            >{this.state.data ? this.state.data.item.description : ''}</RegularText>
                                            </ScrollView>
                                    </View>
                                </View>
                            </View>
                        </View>


                        <View style={{
                            height: 50,
                            margin: '2%',
                            borderWidth: 1,
                            borderRadius: 5,
                            borderColor: 'gray',
                            alignItems: 'center',
                            flexDirection: 'row'
                        }}>
                            <View style={{width: '25%', height: '100%', justifyContent: 'center'}}>
                                <ItalicText numberOfLines={1} style={{
                                    fontSize: 12,
                                    fontStyle: 'italic',
                                    textAlign: 'left',
                                    marginLeft: '5%'
                                }}>Wishlist items:</ItalicText>
                            </View>
                            <View style={{width: '75%', height: '100%'}}>
                                {
                                    this.state.data ? (<FlatList style={{height: 110, width: '98%',}}
                                                                 horizontal={true}
                                                                 showsHorizontalScrollIndicator={false}
                                                                 data={this.state.data.user.wishlist}
                                                                 keyExtractor={(item) => String(item.id)}
                                                                 renderItem={({item, index}) =>
                                                                     <View style={{
                                                                         justifyContent: 'center',
                                                                         backgroundColor: '#12BFDF',
                                                                         borderRadius: 5,
                                                                         margin: 5
                                                                     }}>
                                                                         <BoldText style={{
                                                                             fontSize: 15,
                                                                             height: '90%',
                                                                             marginLeft: 5,
                                                                             marginRight: 5,
                                                                             color: 'white'
                                                                         }}>{item.name}</BoldText>
                                                                     </View>
                                                                 }
                                    />) : (null)
                                }

                            </View>

                        </View>

                        <View style={{height: 150, marginLeft: '2%', marginRight: '2%', borderRadius: 5}}>
                            {this.state.data ? (
                                <MapView
                                    style={{alignSelf: 'stretch', height: '100%', borderRadius: 5}}
                                    region={{
                                        latitude: parseFloat(this.state.data.user.latitude),
                                        longitude: parseFloat(this.state.data.user.longitude),
                                        latitudeDelta: 0.0922,
                                        longitudeDelta: 0.0421,
                                    }}
                                >
                                    <MapView.Marker
                                        coordinate={{
                                            latitude: parseFloat(this.state.data.user.latitude),
                                            longitude: parseFloat(this.state.data.user.longitude),
                                        }}>
                                        <Image source={require('../assets/star.png')} style={{width: 50, height: 50}}/>
                                    </MapView.Marker>


                                </MapView>
                            ) : (null)
                            }
                        </View>
                        {/* <View> */}

                            {/* <Button name=
                                        {
                                            this.props.navigation.state.params.myItem ?
                                                'Remove Item' : 'Select Item'
                                        }
                                    fontSize={16}
                                    onPress=
                                        {
                                            this.props.navigation.state.params.myItem ?
                                                this.confirmDeleteItem.bind(this) : this.pressSelectItem.bind(this)
                                        }
                                    marginTop={'3%'}
                                    marginLeft={"5%"}
                                    backgroundColor={'#14DFC1'}
                                    color={'white'}
                                    marginBottom={10}
                                    marginRight={"5%"}
                                    textMarginTop={15}
                                    height={50}
                                    fontFamily={"NolanNext-Bold"}
                            /> */}
                             <TouchableOpacity
                        style={{
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#14DFC1',
                            marginBottom:10,
                            marginTop: 10,
                            height: 50,
                            marginLeft: '5%',
                            marginRight: '5%',
                        }}
                        onPress= {
                            this.props.navigation.state.params.myItem ?
                                this.confirmDeleteItem.bind(this) : this.pressSelectItem.bind(this)
                        }>
                        <Text style={{
                            fontSize: 16, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Bold"
                        }}> {
                            this.props.navigation.state.params.myItem ?
                                'Remove Item' : 'Select Item'
                        }</Text>
                </TouchableOpacity>


                            {this.state.loading ?
                                <ActivityIndicator
                                    style={{zIndex: 2, position: 'absolute', bottom: 0, top: 0, alignSelf: 'center'}}
                                    color='#00DBBB' size='large'/> : null
                            }
                            <GridModalBrowse gridModalVisible={this.state.gridModalVisible}
                                             setGridModalVisible={this.setGridModalVisible}
                                             itemList={this.state.itemDetailValues}
                                             done={this.saveTrade}
                                             selectedGridValue={this.selectedItem}
                                             allSelectedItemList={this.state.allSelectedItemList}
                                             makeOffer
                            />

                            {/* <View style={{
                                top:-340,
                                left:10,
                                    height: 66,
                                    width: 66,
                                    borderRadius: 33,
                                    backgroundColor: 'white',
                                    position:'absolute',
                                    justifyContent:'center',
                                    alignItems:'center'
                                }}>
                                    {this.state.data ?
                                        (<Image source={{uri: this.state.data.user.picture}}
                                                style={{height: 60, width: 60, borderRadius: 30, margin: 0}}/>
                                        ) :
                                        ((<Image source={require('../assets/userProfile.png')}
                                                 style={{height: 60, width: 60, borderRadius: 30, margin: 0}}/>
                                        ))
                                    }
                                </View> */}
                            <Spinner
                                visible={this.state.spinner}
                                textContent={"Loading..."}
                                color='#00DBBB'
                                overlayColor='rgba(0, 0, 0, 0.6)'
                                textStyle={{color: '#00DBBB'}}
                            />

                        {/* </View> */}
                    </View>
                </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#ffffff',
        height: Dimensions.get('window').height - 64,
        width: Dimensions.get('window').width
    },
    icon: {
        width: 23,
        height: 23,
    },
    mapsContainer: {
        top: 25,
        width: '90%',
        marginLeft: '5%',
        marginRight: '5%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ecf0f1',
        borderWidth: 1,
        borderColor: '#ecf0f1',
        borderRadius: 10
    }
});
