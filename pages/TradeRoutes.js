import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    AsyncStorage,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import _ from 'underscore'
import TradeRoutesActivity from './Home/TradeRoutesActivity';
import navigate from '../Components/navigate';
import api from '../api';
import {RegularText} from '../Components/styledTexts'

export default class TradeRoutes extends Component {

    static navigationOptions = ({navigation}) => ({
        title: 'Trade Routes',
        tabBarLabel: 'Routes',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../assets/tradeRoutes.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() =>{
                                  AsyncStorage.getItem('profileCompleted').then((value) => {
                                      value === '1' ? navigation.navigate('PostItem') : alert('Please create a profile before posting the item.')
                                  });
                              }}
            >
                <Image
                    source={require('../assets/white_plus_icon.png')}
                    style={{marginLeft: 15, height: 28, width: 28}}
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity
                onPress={() => navigation.navigate('Offers')}
                activeOpacity={0.5}>
                <Image
                    source={require('../assets/chat.png')}
                    style={{marginRight: 15, height: 35, width: 35}}
                />
            </TouchableOpacity>
        ),
        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 2,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#fff',
                fontFamily: "NolanNext-Bold",
            }
    });

    constructor(props) {
        super(props);
        this.state = {
            customStyleIndex: 0,
            spinner: false,
            userId: '',
            tradingItems: false,
            itemDetailValues: '',
            tradeRouteItems: [],
            myTradeRouteItem: [],
            mySelectedItem: '',
            matchedItemlists: [0, 0, 0, 0, 0],
            TradeRoutebackButton: false,
            showMyTradeRouteItemsDetail: false,
            TradeDetailArray: [{
                title: 'Your Item',
                file: require('../assets/box.png'),
                color: "#00C0EF"
            },
                {
                    title: 'Trade1',
                    file: require('../assets/question.png'),
                    color: "gray"
                },
                {
                    title: 'Trade2',
                    file: require('../assets/question.png'),
                    color: 'gray'
                },
                {
                    title: 'Trade3',
                    file: require('../assets/question.png'),
                    color: 'gray'
                },
                {
                    title: 'Trade4',
                    file: require('../assets/question.png'),
                    color: 'gray'
                },
                {
                    title: 'Trade5',
                    file: require('../assets/question.png'),
                    color: 'gray'
                }
            ]
        };
        this.channel_detail = []
        this.onReceived = this.onReceived.bind(this);
    }

    componentDidMount() {
        this.getWishlistTradeRoutes();
        this.getUserDetailfromLocalDatbase()
    }

    onReceived(notification) {
    }

    save_channel = async () => {
        var _this = this;
        let body = {
            "channel": this.channel_detail
        }
        try {
            let response = await api.request('/channel', 'POST', body);
            if (response.status === 200) {
                response.json().then((data) => {
                    _this.setState({
                        spinner: false
                    });
                    navigate.navigateTo(this.props.navigation, 'Offers')
                    _this.channel_detail = [];
                });
            } else {
                _this.setState({spinner: false});
            }
        } catch (error) {
            _this.setState({spinner: false});
            error.json().then((err) => {
            });
            _this.setState({spinner: false});
        }
    }

    set_channel_detail = async (other_user_id) => {
        var channel_name = "";
        if (this.state.userId < other_user_id) {
            channel_name = this.state.userId + "-" + other_user_id;
        } else {
            channel_name = other_user_id + "-" + this.state.userId;
        }
        this.channel_detail.push({
            channel_name: channel_name,
            item_with_user_id: other_user_id
        })
    }


    setMyItem = item => {
        console.log("set my item in trade routes " + item)
        const TradeDetailArrayCopy = [...this.state.TradeDetailArray];
        const index = this.state.matchedItemlists.indexOf(0);
        if (this.state.tradingItems) {
            this.set_channel_detail(item.user.id)
            if (index != -1) {
                const matchedItemlistsCopy = [...this.state.matchedItemlists]
                matchedItemlistsCopy[index] = item.id
                TradeDetailArrayCopy[index + 1].file = {uri: item.picture}
                this.setState({TradeDetailArray: TradeDetailArrayCopy, matchedItemlists: matchedItemlistsCopy})
            } else {
                alert('You can select only 5 items')
            }
        } else {
            TradeDetailArrayCopy[0].file = {uri: item.picture}
            this.setState({TradeDetailArray: TradeDetailArrayCopy, mySelectedItem: item.id})
        }
    }

    getToInitialState = () => {
        this.getmyItemList()
        this.setState(
            {
                mySelectedItem: '',
                matchedItemlists: [0, 0, 0, 0, 0],
                TradeDetailArray: [{
                    title: 'Your Item',
                    file: require('../assets/box.png'),
                    color: "#00C0EF"
                },
                    {
                        title: 'Trade1',
                        file: require('../assets/question.png'),
                        color: "gray"
                    },
                    {
                        title: 'Trade2',
                        file: require('../assets/question.png'),
                        color: 'gray'
                    },
                    {
                        title: 'Trade3',
                        file: require('../assets/question.png'),
                        color: 'gray'
                    },
                    {
                        title: 'Trade4',
                        file: require('../assets/question.png'),
                        color: 'gray'
                    },
                    {
                        title: 'Trade5',
                        file: require('../assets/question.png'),
                        color: 'gray'
                    }
                ]
            });
    }


    async getUserDetailfromLocalDatbase() {
        var _this = this
        _this.setState({
            spinner: true
        })
        await AsyncStorage.getItem('userDetailValues').then(data => {
            var detailvalue = JSON.parse(data);
            _this.state.userId = detailvalue.user.id
            _this.getmyItemList()
            _this.getUserDetail()
        });
    }

    async getUserDetail() {
        var _this = this
        let response = await api.getRequest('/users/' + _this.state.userId, 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    _this.setState({
                        spinner: false,
                        tradingItems: false
                    })
                    let userDetail = _.omit(data.user, "item", "trade_chain", "tradeChain")
                    AsyncStorage.setItem('userDetail', JSON.stringify(userDetail));
                    myTradeRouteItem = _.filter(data.user.item, function (trade_item) {
                        return (trade_item.trade.length > 0 || trade_item.trade_with.length > 0);
                    });
                    // _this.setState({
                    //     myTradeRouteItem: myTradeRouteItem
                    // })
                })
        } else {
            _this.setState({
                spinner: false
            })
        }
    }

    async getWishlistTradeRoutes() {
        var _this = this
        _this.setState({
            spinner: true
        })
        try {
            let response = await api.getRequest('/traderoute', 'GET');
            if (response.status === 200) {
                response.json()
                    .then(function (data) {
                        _this.setState({
                            spinner: false,
                            myTradeRouteItem: data.traderoute

                        })
                    })
            } else {
                _this.setState({
                    spinner: false
                })
                console.log("wrong resposne =====")
                console.log(response)
                alert(response)
            }
        } catch (error) {
        }

    }

    saveTrade = async () => {
        var _this = this
        let matchedIdList = [];

        if (_this.state.matchedItemlists === [0, 0, 0, 0, 0]) {
            alert('Please select at least one item for trade')
        } else {
            for (var i = 0; i < _this.state.matchedItemlists.length; i++) {
                if (_this.state.matchedItemlists[i] != 0) {
                    matchedIdList.push(_this.state.matchedItemlists[i])
                } else {
                    break;
                }
            }
            let body = {
                "trade_item_id": _this.state.mySelectedItem,
                "tradewith": matchedIdList
            }
            try {
                let response = await api.request('/trade', 'POST', body);
                if (response.status === 200) {
                    response.json().then((data) => {
                        _this.setState({
                            spinner: false
                        });
                        _this.getToInitialState()
                        _this.getUserDetail()
                        _this.save_channel()
                    });
                } else {
                    _this.setState({spinner: false});
                }
            } catch (error) {
                _this.setState({spinner: false});
            }
        }
    }

    getSearchedResults = async () => {
        var _this = this
        _this.setState({
            spinner: true
        })
        let body = {
            "item": {
                "name": '',
                "min_value": '',
                "max_value": '',
                "distance_in_miles": '',
                "category_id": ''
            }
        }
        try {
            let response = await api.request('/items/search', 'POST', body);
            if (response.status === 200) {
                response.json().then((data) => {
                    _this.setState({
                        spinner: false,
                        itemDetailValues: data.items,
                        tradingItems: true
                    })
                });
            } else {
                _this.setState({spinner: false});
            }
        } catch (error) {
            _this.setState({spinner: false});
        }
    }


    getmyItemList = async () => {
        var _this = this
        _this.setState({
            spinner: true
        });
        let response = await api.getRequest('/user/items', 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    _this.setState({
                        itemDetailValues: data.items,
                        tradingItems: false,
                        spinner: false
                    });
                });
        } else {
            _this.setState({
                spinner: false
            })
        }
    };

    handleCustomIndexSelect = (index) => {
        this.setState({
            ...this.state,
            customStyleIndex: index,
        });
    };

    setTradeRoutebackButton = (isShow) => {
        if (isShow) {
            this.setState({
                TradeRoutebackButton: true,
                showMyTradeRouteItemsDetail: true
            })
        } else {
            this.setState({
                TradeRoutebackButton: false,
                showMyTradeRouteItemsDetail: false
            })
        }
    };


    render() {
        return (
            <ScrollView ref='scrollView' style={styles.scrollContainer}>
                <View>
                    <RegularText
                        style={styles.text}>{'We generate trade routes by \n matching your items to your wishlist'}</RegularText>
                </View>
                <View>
                    <TradeRoutesActivity
                        myTradeRouteItem={this.state.myTradeRouteItem}
                        setTradeRoutebackButton={this.setTradeRoutebackButton}
                        showMyTradeRouteItemsDetail={this.state.showMyTradeRouteItemsDetail}
                        navigation={this.props.navigation}
                    />
                </View>
                {/* } */}
                <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.6)'
                    textStyle={{color: '#00DBBB'}}
                />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#ffffff',
        flex: 1
    },
    icon: {
        resizeMode: 'contain'
    },
    text: {
        fontSize: 15,
        textAlign: 'center',
        color: 'black',
        marginTop: 8,
        marginBottom: 10,
        marginLeft: '10%',
        marginRight: '10%',
    },
    tabStyle: {
        borderColor: '#D52C43'
    },
    activeTabStyle: {
        backgroundColor: '#D52C43',
    },
    tabTextStyle: {
        color: '#D52C43'
    }
});
