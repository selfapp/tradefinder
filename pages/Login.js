import React, {Component} from "react";
import {
    StyleSheet,
     Text,
    View,
    TouchableOpacity,
    Image,
    AsyncStorage,
    TextInput,
    KeyboardAvoidingView
} from 'react-native';
import FBSDK,{ 
    LoginManager,
    AccessToken,
    } from 'react-native-fbsdk'
import Spinner from 'react-native-loading-spinner-overlay';
import ChatEngineProvider from './Chat/ce-core-chatengineprovider';
import navigate from '../Components/navigate';
import Background from '../Components/background';
import Button from '../Components/button';
import api from '../api';
import ChatEngineProviderMe from "./Chat/ce-core-chatengineproviderme";


const Constants = {
    //Dev Parse keys
    TWITTER_COMSUMER_KEY: "dx75CBgedl44HtWusIkD1ah03",
    TWITTER_CONSUMER_SECRET: "4GH9Kv1v4jHnxfHR6SRGApoICsUv06mtb5JTDfg8Rrf4SJxtT2",
}


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            // phoneNumber: '',
            email:''
        }
    }

    validate = text => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
          return false;
        } else {
          return true;
        }
      };

    // Login Method
    async onLogin() {
        console.log("email address is ", this.state.email)
        var _this = this;
        // if (_this.state.phoneNumber.length === 10) {
            if(this.state.email.length > 0){

                if (this.validate(this.state.email)) {
                    _this.setState({loading: true});
                    let body = {
                        "user": {
                            // "country_code": "+1",
                            // "mobile": _this.state.phoneNumber,
                            "email": _this.state.email,
                            "signup_by": "email"
                        }
                    };
                    console.log("body")
                    console.log(body)
                    try {
                        let response = await api.request('/signup', 'post', body, null);
                        console.log("repsonse for email login /....")
                        console.log(response)
                        _this.setState({loading: false});
                        if (response.status === 200) {
                            try {
                                response.json().then((data) => {
                                   
                                    // Navigate to OTP Verification Screen
                                    navigate.navigateWithReset(_this.props.navigation, 'Verification', {email: _this.state.email})
                                });
                            } catch (error) {
                            }
                        } else {
                            await response.json().then((res) => {
                                    alert(error)
                                }
                            );
                            _this.setState({loading: false});
                        }
                    } catch (error) {
                        _this.setState({loading: false});
                        alert(error)
                    }
                }else{
                    alert('Please enter correct email address.');
                }
           
        } else {
            alert('Please enter the email address.');
        }
    }

// Login With facebook 

    facebookLogin = async (accessToken) => {
        var _this = this
        _this.setState({loading: true});
        let body = {
            "user": {
                "signup_by": "facebook",
                "token": accessToken
            }
        };
        try {
            let response = await api.request('/signin', 'post', body, null);
            if (response.status === 200) {
                try {
                    response.json().then((data) => {

                        console.log("Login data details ..")
                        console.log(data)
                        let userId = data.user.id;
                        // let userName = data.user.first_name;
                        // ChatEngineProvider.connect(userId.toString()).then(() => {
                        //     try {
                        //         AsyncStorage.multiSet(
                        //             [['isLoggedIn', 'true'],
                        //                 ['userDetailValues', JSON.stringify(data)],
                        //                 [ChatEngineProvider.ASYNC_STORAGE_USERDATA_KEY, userId.toString()]]
                        //         ).then(() => {
                        //             _this.setState({loading: false});
                        //             // Navigate to Home Screen
                        //             navigate.navigateWithReset(_this.props.navigation, 'TradeFindr')
                        //         })
                        //     } catch (error) {
                        //         console.log(error);
                        //     }
                        // });

                        ChatEngineProviderMe.connect(
                            userId,
                            // userName,
                          ).then(() => {
                           
                            ChatEngineProvider.connect(
                              userId,
                            //   userName,
                            ).then(async () => {

                                console.log('RUNNING ASYNC SIMPLE');
                                try {                                  
                                  AsyncStorage.multiSet(
                                                [['isLoggedIn', 'true'],
                                                ['userDetailValues', JSON.stringify(data)],
                                                [ChatEngineProvider.ASYNC_STORAGE_USERDATA_KEY, userId.toString()],
                                               ])
                                _this.setState({loading: false});
                                            // Navigate to Home Screen
                                 navigate.navigateWithReset(_this.props.navigation, 'TradeFindr')
                                  AsyncStorage.getItem('deviceToken').then((token) => {
                                   
                                    ChatEngineProviderMe.getChatRoomModel()
                                      .connect("notify" + userId.toString(), true)
                                      .then(() => {
                                        var chat = ChatEngineProviderMe.getChatRoomModel().state.chat;
                                        let chats = [chat];
                                        ChatEngineProviderMe._chatEngine.me.notifications.enable(chats, token, (error) => {
                                          console.log("enter into push enable........", error);
                                          if (error !== null) {
                                            this.setState({loading: false})
                                            Alert.alert(
                                              'Push Notification error',
                                              `Unable to enable notifications for global: ${error.message}`,
                                              [{text: 'OK'}],
                                              {cancelable: true}
                                            );
                                          } else {
                                            console.log("ENABLED PUSH NOTIFICATION FOR PERSONAL CHANNEL");
                                          }
                                        });
                                      });
                                  });
                                } catch
                                  (error) {
                                    this.setState({loader: false})
                                  console.log("error during chat initialise " + error);
                                }
                              }
                            );
                          });
                  
                    });
                } catch (error) {
                }
            } else {
                await response.json().then((res) => {
                        alert(error)
                    }
                );
                _this.setState({loading: false});
            }
        } catch (error) {
            alert(error);
            _this.setState({loading: false});
        }
    };

// Get Access Token with permissions on Facebook 

    getAccessTokenFromFacebook = () => {
        LoginManager.logInWithReadPermissions(['public_profile', 'email'])
            .then((result) => {
                if (result.isCancelled) {
                    alert('The user cancelled the request')
                    return Promise.reject(new Error('The user cancelled the request'));
                }
                // Retrieve the access token
                return AccessToken.getCurrentAccessToken();
            })
            .then((data) => {
                this.facebookLogin(data.accessToken);
            })
            .catch((error) => {
                const {code, message} = error;
                alert('error' + error)
            });
    };


 // UI Desigin    
    render() {
        return (
            <Background>
                <KeyboardAvoidingView contentContainerStyle={{alignItems: 'center', flex: 1}} behavior='position'
                                      enabled>
                    <Image
                        style={styles.imageStyle}
                        source={require('../assets/logo.png')} resizeMode='contain'
                    />
                    <Text style={styles.textStyle}>
                        TradeFindr
                    </Text>
                    <View style={{
                        width: '90%', height: 50, backgroundColor: 'white', borderRadius: 8,
                        marginTop: '35%', flexDirection: 'row', justifyContent:'center'
                    }}>
                        <Image
                            style={{marginTop:15, height: 20, width: 28}}
                            source={require('../assets/mail.png')} resizeMode='contain'
                            // source={require('../assets/phone.png')} resizeMode='contain'
                        />
                         <TextInput underlineColorAndroid='transparent'
                                   autoCorrect={false}
                                   placeholderTextColor='gray'
                                   returnKeyType='done'
                                   keyboardType='email-address'
                                   placeholder="Enter Email"
                                   onChangeText={(email) => this.setState({email})}
                                   style={{
                                       marginLeft: '8%', height: 40, marginTop: '1.5%',
                                       width: '80%'
                                   }}
                        />
                        {/* <TextInput underlineColorAndroid='transparent'
                                   autoCorrect={false}
                                   placeholderTextColor='gray'
                                   returnKeyType='done'
                                   keyboardType='numeric'
                                   placeholder="Enter Phone No"
                                   onChangeText={(phoneNumber) => this.setState({phoneNumber})}
                                   style={{
                                       marginLeft: '8%', height: 40, marginTop: '1.5%',
                                       width: '80%'
                                   }}
                        /> */}
                    </View>
                    {/* <Button
                        name={'Login'} paddingHorizontal={10} fontSize={16}
                        onPress={this.onLogin.bind(this)}
                        marginTop={30}
                        backgroundColor={'#14DFC1'}
                        color={'white'}
                        textMarginTop={15}
                        width={'100%'}
                        marginLeft={'5%'}
                        marginRight={'5%'}
                        height={50}
                    /> */}

<TouchableOpacity
                        style={{
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#14DFC1',
                            marginTop: 30,
                            height: 50,
                            marginLeft: '5%',
                            marginRight:'5%',
                            width:'100%'
                        }}
                        onPress= {
                            this.onLogin.bind(this)
                        }>
                        <Text style={{
                            fontSize: 16, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Bold"
                        }}>Login</Text>
                </TouchableOpacity>


                    {/* <View
                        style={{
                            flexDirection: 'row', marginTop: '6%', height: 50,
                            marginLeft: '0%'
                        }}
                    >
                        <View style={{
                            height: 2, width: '25%', backgroundColor: '#00DBBB'
                        }}>
                        </View>
                        <Text
                            style={{
                                color: '#7A7A7A',
                                fontSize: 18,
                                marginTop: '-3%',
                                marginLeft: '2%',
                                fontWeight: '500'
                            }}>
                            Or login in with
                        </Text>
                        <View style={{
                            height: 2, width: '25%', backgroundColor: '#00DBBB',
                            marginLeft: '2%'
                        }}>
                        </View>
                    </View> */}
                    {/* <View
                        style={{
                            flexDirection: 'row', marginTop: '-5%',
                            marginLeft: '0%'
                        }}
                    >
                        <TouchableOpacity
                            onPress={this.getAccessTokenFromFacebook}
                        >
                            <Image
                                style={{marginLeft: '2%'}}
                                source={require('../assets/facebook.png')} resizeMode='contain'
                            />
                        </TouchableOpacity>
                    </View> */}
                    <Spinner
                        visible={this.state.loading}
                        textContent={"Loading..."}
                        color='#00DBBB'
                        overlayColor='rgba(0, 0, 0, 0.6)'
                        textStyle={{color: '#00DBBB'}}
                    />
                </KeyboardAvoidingView>
            </Background>
        );
    }
}

const styles = StyleSheet.create({
    imageStyle:
        {
            marginTop: '20%'
        },
    textStyle:
        {
            marginTop: '5%',
            fontSize: 18,
            fontWeight: '600',
            color: '#A7A8AA'
        }
});
export default Login;
