import React, { Component } from 'react';
import {Text, View, Image, TouchableOpacity, FlatList } from 'react-native';
import {
  BoldText,ItalicText,RegularText
} from '../../Components/styledTexts'

export default class Items extends Component {

  constructor(props) {
    super(props);
  }

  // Row Desigin for Item
  _renderItem = ({ item,index }) => (
    <TouchableOpacity
       onPress={() => this.props.goToYourItemScreen(item.id,index)}
        style={{height: 70, marginTop: '2%',marginLeft: '5%', marginRight: '5%',borderRadius: 8, borderColor: 'gray', borderWidth: 1,width: '90%',flexDirection: 'row'}}>
      <View style={{width:'20%',height:'90%',justifyContent:'center'}}>
          <Image
              style={{ margin: '10%', height: '80%', width: '80%', }}
               source={{ uri: item.pictures[0].picture }}
          />
      </View>

      <View style={{marginLeft:'5%',width:'45%',justifyContent:'center'}}>
      <RegularText numberOfLines={1} style={{fontSize: 12 }}>{item.category.name}</RegularText>
      <BoldText numberOfLines={1} style={{marginTop:'1%',fontSize: 12, color: '#000000',fontWeight: '600'}}>{item.title}</BoldText>
      </View>


      <View style={{ width: '30%',justifyContent:'center'}}>
        <RegularText style={{ fontSize: 12,textAlign:'right',marginRight:'5%' }}>{item.view_count} views</RegularText>
        <ItalicText numberOfLines={1} style={{ fontSize: 9 ,fontStyle: 'italic',textAlign:'right',marginRight:'5%',marginTop:'3%'}}>posted {item.days} days ago</ItalicText>
      </View>

    </TouchableOpacity>
  );

  // UI Desigin
  render() {
    currentNavigation = this.props.navigation;

    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            // Check profile is completed 
              if(this.props.profileCompleted) {
                  this.props.goToPostItemScreen()
              } else {
                  alert('Please create a profile before posting the item.');
              }
          }}
          style={{
            width: '90%', marginTop: '2%', marginBottom: '1%',
            marginLeft: '5%', marginRight: '5%', height: 50, borderWidth: 1, borderRadius: 5,
            borderColor: '#14DFC1',
            alignItems: 'center'
          }}
        >
          <BoldText style={{
            marginTop: 8, fontWeight: '700', color: '#14DFC1',
            height: 30,
            fontSize: 22
          }}>
            Post An Item
            </BoldText>
        </TouchableOpacity>
        {/* Show self Items in list */}
        {
          this.props.itemDetailValues.length ? (<FlatList
            removeClippedSubviews={true}
            keyExtractor={(item, index) => index.toString()}
            data={this.props.itemDetailValues}
            renderItem={this._renderItem}
          />) : (
              <View style={{ marginTop: '15%', alignItems: 'center' }} >
                <Text
                  style={{
                    justifyContent: 'center',
                    color: '#888888', fontWeight: 'bold', fontSize: 20,
                  }}
                >
                  No Items Found
              </Text>
              </View>
            )
        }
      </View>
    );
  }
}
