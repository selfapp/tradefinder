import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    FlatList,
    Alert
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import GridModal from '../../Components/GridModal'
import {
    BoldText, RegularText
} from '../../Components/styledTexts'
import api from '../../api';

export default class Wishlist extends Component {

    constructor() {
        super();
        this.state = {
            gridModalVisible: false,
            spinner: false,
            wishListDetailValues: [],
            itemForWishList: [],
            allSelectedWishList: [],
            selectedCategory: null, keyWord: '',
            error: ''
        }
    }

    componentDidMount() {
        // Get Wish list item details
        this.getWishListDetailValues();
    }

    setGridModalVisible = (isVisible) => {
        this.setState({
            gridModalVisible: isVisible
        })
    }

// Get my Wish List Item
    getWishListDetailValues = async () => {
        var _this = this
        _this.setState({
            spinner: true
        })
        try {
            let response = await api.getRequest('/mywishList', 'GET');
            if (response.status === 200) {
                response.json()
                    .then(function (data) {
                        _this.setState({
                            spinner: false,
                            wishListDetailValues: data.user.wishlist
                        });
                    });
            } else {
                _this.setState({
                    spinner: false
                });
                console.log(JSON.stringify(response))
            }
        } catch (error) {
        }
    };

    // Get Confirmatio for delete Item
    confirmDeleteItem = (selectedId) => {
        Alert.alert(
            'Delete',
            'Are you sure you want to delete this item?',
            [
                {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {
                    text: 'Yes', onPress: () => {
                        this.deleteWishList(selectedId)
                    }
                }
            ],
            {cancelable: false})
    }

    // Delete item from wish list on server
    deleteWishList = async (selectedId) => {
        var _this = this
        _this.setState({
            loading: true
        })
        try {
            let response = await api.getRequest('/wishlist/' + selectedId + '/delete', 'DELETE');
            if (response.status === 200) {
                response.json()
                    .then(function (data) {
                        _this.setState({
                            loading: false
                        });
                        _this.getWishListDetailValues()
                    });
            } else {
                _this.setState({
                    loading: false
                });
            }
        } catch (error) {
            _this.setState({
                loading: false
            });
        }
    };

    //Ge Items For wish list from server
    getItemForWishList = async () => {
        var _this = this
        _this.setState({
            spinner: true
        })
        try {
            let response = await api.getRequest('/itemforWishList', 'GET');
            if (response.status === 200) {
                response.json()
                    .then(function (data) {
                        _this.setState({
                            spinner: false,
                            itemForWishList: data.items,
                            gridModalVisible: true,
                            selectedCategory: null,
                            error: ''
                        })
                    })
            } else {
                _this.setState({
                    spinner: false
                });
            }
        } catch (error) {
        }
    };

    textcategory(keyWordName) {
        var _this = this
        _this.setState({keyWord: keyWordName})
    }

    // Callback after selecting category
    selectedCategory(item) {
        this.setState({gridModalVisible: true})
        this.setState({selectedCategory: item, error: ''})
    }

    // Navigate to Category List for searching
    selectCategory() {
        this.setState({gridModalVisible: false})
        this.props.navigation.navigate('Category', {delegate: this})
    }

    // Add a item in wishlist 
    createWishList = async () => {
        var _this = this;
        _this.setState({
            spinner: true
        })

        if (_this.state.allSelectedWishList !== null) {
            _this.setState({
                loading: true,
                visibleSecondModal: false
            });

            if (this.state.selectedCategory === null) {
                this.setState({error: 'please select a category', spinner: false})
                return
            } else {
                this.setState({gridModalVisible: false, error: ''})
                let body = {
                    "wishlist": {
                        "item_id": "",
                        "name": this.state.keyWord,
                        "category_id": this.state.selectedCategory.id
                    }
                }
                try {
                    let response = await api.request('/wishlist', 'POST', body);
                    if (response.status === 200) {

                        _this.setState({
                            spinner: false, loading: false,
                        });
                        response.json().then((data) => {
                            _this.setState({
                                spinner: false, loading: false,
                            });
                            _this.getWishListDetailValues();
                        });
                    } else {
                        await response.json().then((res) => {
                            }
                        );
                    }
                } catch (error) {
                }
            }
        } else {
            alert('Please select at least one item')
        }
    }

// Selected wishlist items
    selectedWishlist = (selected_data) => {
        let index = this.state.allSelectedWishList.indexOf(selected_data);
        if (index === -1) {
            this.setState({allSelectedWishList: [...this.state.allSelectedWishList, selected_data]})
        } else {
            let array = [...this.state.allSelectedWishList]; // make a separate copy of the array
            array.splice(index, 1);
            this.setState({allSelectedWishList: array});
        }
    };

// Wishlist item desigin 
    _renderItem = ({item}) => (
        <TouchableOpacity
            style={{
                height: 70,
                marginTop: '2%',
                marginLeft: '5%',
                marginRight: '5%',
                borderRadius: 8,
                borderColor: 'gray',
                borderWidth: 1,
                width: '90%',
                flexDirection: 'row'
            }}>

            <View style={{width: '70%', height: '90%', marginLeft: '2%', justifyContent: 'center'}}>
                <RegularText numberOfLines={1}
                             style={{fontSize: 13}}>{(item.category) ? item.category.name : ''}</RegularText>
                <BoldText numberOfLines={1}
                          style={{fontSize: 15, fontWeight: 'bold', marginTop: '1%'}}>{item.name}</BoldText>
            </View>

            <View style={{width: '30%', height: '90%', justifyContent: 'center'}}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{justifyContent: 'center'}}>
                        <RegularText style={{fontSize: 12}}>Remove</RegularText>
                    </View>
                    <TouchableOpacity onPress={() => this.confirmDeleteItem(item.id)}
                                      style={{marginLeft: '5%'}}>
                        <Image style={{height: 30, width: 30,}}
                               source={require('../../assets/remove_circle.png')}
                               resizeMode='contain'/>
                    </TouchableOpacity>
                </View>
            </View>

        </TouchableOpacity>
    );

    //UI Deasigin
    render() {

        return (
            <ScrollView ref='scrollView' style={styles.scrollContainer}>
                <TouchableOpacity
                    onPress={() => this.getItemForWishList()}
                    style={{
                        width: '90%', marginTop: '2%', marginBottom: '1%',
                        marginLeft: '5%', marginRight: '5%', height: 50, borderWidth: 1, borderRadius: 5,
                        borderColor: '#14DFC1',
                        alignItems: 'center'
                    }}
                >
                    <BoldText style={{
                        marginTop: 8, fontWeight: '700', color: '#14DFC1',
                        height: 30,
                        fontSize: 22
                    }}>
                        Add to Wishlist
                    </BoldText>
                </TouchableOpacity>
                {/* Show wish list items */}
                {this.state.wishListDetailValues.length ? (<FlatList
                    removeClippedSubviews={true}
                    data={this.state.wishListDetailValues}
                    renderItem={this._renderItem}
                />) : (
                    <View style={{marginTop: '15%', alignItems: 'center'}}>
                        <RegularText
                            style={{
                                justifyContent: 'center',
                                color: '#888888', fontWeight: 'bold', fontSize: 20,
                            }}
                        >
                            No Items Found
                        </RegularText>
                    </View>
                )
                }
                {/* Show item for adding on wishlist */}
                <GridModal gridModalVisible={this.state.gridModalVisible}
                           setGridModalVisible={this.setGridModalVisible}
                           itemList={this.state.itemForWishList}
                           done={this.createWishList}
                           selectedGridValue={this.selectedWishlist}
                           allSelectedItemList={this.state.allSelectedWishList}
                           selectedCat={this.state.selectedCategory}
                           navigatecategory={this.selectCategory.bind(this)}
                           textcategory={this.textcategory.bind(this)}
                           error={this.state.error}
                />
                <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.6)'
                    textStyle={{color: '#00DBBB'}}
                />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#ffffff'
    }
});
