import React, {Component} from 'react';
import {
    StyleSheet, View, ScrollView, Image, TouchableOpacity, FlatList,
    TouchableWithoutFeedback,
    Platform, Modal, ActivityIndicator
} from 'react-native';
import SegmentedControlTab from 'react-native-segmented-control-tab'
import {Icon} from 'native-base';
import StarRating from 'react-native-star-rating';
import Items from './Items';
import Trades from './Trades';
import Wishlist from './Wishlist';
import TradeRoutes from '../TradeRoutes';
import {NavigationActions} from 'react-navigation'
import TradeRouteComponent from '../../Components/TradeRouteComponent';

var ImagePicker = require('react-native-image-picker');
import api from '../../api';
import Spinner from 'react-native-loading-spinner-overlay';
import Button from '../../Components/button';
import EditProfileModal from '../../Components/EditProfileModal';
import navigate from '../../Components/navigate';
import TradeRoutesActivity from '../Home/TradeRoutesActivity';
import {BoldText, ItalicText, RegularText} from '../../Components/styledTexts'

export default class ProfileNew extends Component {
    static navigationOptions = ({navigation}) => ({
        headerTitle: 'Profile',
        tabBarLabel: 'Browse',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../../assets/tab2.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => navigation.goBack(null)}>
                <Image
                    source={require('../../assets/back.png')}
                    style={{marginLeft: 15, height: 30, width: 18}}
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity
                onPress={() =>
                     navigation.state.params.openOptionModel()
                }
                activeOpacity={0.5}>
                <Image
                    source={require('../../assets/no-user.png')}
                    style={{marginRight: 15, height: 40, width: 40}}
                />
            </TouchableOpacity>
            // <Image
            //     source={require('../../assets/no-user.png')}
            //     style={{marginRight: 15, height: 30, width: 30}}
            // />

        ),
        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 5,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: 'white',
                fontFamily: "NolanNext-Bold",
            }
    });

    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
            username: '',
            visibleModal: false,
            customStyleIndex: 0,
            starCount: 3.5,
            latitude: '',
            longitude: '',
            loading: false,
            address: '',
            userId: props.navigation.state.params.userId,
            spinner: false,
            profile_pic: '',
            profile_pic_uri: require('../../assets/userProfile.png'),
            itemDetailValues: [],
            wishListDetailValues: [],
            userData: '',
            optionModel: false,

        }
    }


    handleCustomIndexSelect = (index) => {
        this.setState({
            ...this.state,
            customStyleIndex: index,
        });
    }


    componentDidMount() {

        this.getUserDetailValues(this.state.userId)
        this.props.navigation.setParams({
            openOptionModel:this.optionModel.bind(this)
        });
    }
// Option Model
optionModel(){
    this.setState({
        optionModel:true
    })
}
    async getUserDetailValues(userId) {
        var _this = this
        let response = await api.getRequest('/users/' + userId, 'GET');
        if (response.status === 200) {
            response.json().then(function (data) {

                console.log("new user details ....")
                console.log(data)

                console.log("widhlist details ...")
                console.log(data.user.wishlist)

                _this.setState({
                    spinner: false,
                    itemDetailValues: data.user.item,
                    wishListDetailValues: data.user.wishlist, username: data.user.first_name,
                    userData: data
                });
            })
        } else {
            _this.setState({
                spinner: false
            });
        }

    }


    onStarRatingPress = async (rating) => {
        var _this = this
        this.setState({spinner: true});
        let body = {
            "rate": rating
        }
        try {
            let response = await api.request('/users/' + this.state.userId + '/rating', "POST", body);
            if (response.status === 200) {
                response.json().then((data) => {
                    _this.setState({spinner: false, userData: data})
                });
            } else {
                _this.setState({spinner: false});
            }
        } catch (error) {
            _this.setState({spinner: false});
        }

    }

    reportUser = async () => {
        this.setState({spinner: true});
        let body = {
            "report_user_id": this.state.userId
        }
        try {
            let response = await api.request('/report-user', 'POST', body);
            console.log("report response ....")
            console.log(response)
            this.setState({spinner: false})
            if (response.status === 200) {
               console.log("enter .........")
                    
                    response.json().then((data) => {
                        console.log("report data ....")
                        console.log(data.message)
                        alert(data.message)
                    });
               
                // navigate.goBack(null);
            } else {
                // this.setState({spinner: false});
                alert('an error occurred please try again later')
            }
        } catch (error) {
            this.setState({spinner: false});
            alert('an error occurred please try again later')
        }

    }


    optionModal() {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.optionModel}
                onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                <TouchableWithoutFeedback onPress={() => this.setState({optionModel: false})}>
                    <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.1)'}}>
                        <View style={{
                            backgroundColor: '#909090', borderRadius: 7, width: 120, padding: 10, alignSelf: 'flex-end',
                            top: 54, right: '5%'
                        }}>
                            <TouchableOpacity onPress={() => {
                                this.setState({optionModel: false})
                                this.reportUser()}
                                } >
                                <ItalicText style={{marginLeft: 10,marginTop:5,fontSize: 16}}>Report</ItalicText>
                             </TouchableOpacity>
                            {/* </View> */}
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }

    render() {


        return (
            <View style={{flex:1}}>
                  {this.state.spinner ? <ActivityIndicator
                                            style={{zIndex: 2, position: 'absolute', bottom: 0, top: 0, alignSelf: 'center'}}
                                            color='#00DBBB' size='large'/> : null
                                        }
            <ScrollView ref='scrollView' style={styles.scrollContainer}>
                  
                <View style={styles.container}>

                {/* {this.state.spinner ? <ActivityIndicator
                                            style={{zIndex: 2, position: 'absolute', bottom: 0, top: 0, alignSelf: 'center'}}
                                            color='#00DBBB' size='large'/> : null
                                        } */}
             
                    <View style={{height: 100, flexDirection: 'row'}}>

                        {this.state.userData ?
                            (<Image source={{uri: this.state.userData.user.picture}}
                                    style={{marginLeft: 20, height: 70, width: 70, borderRadius: 35, marginTop: 15}}/>
                            ) :
                            ((<Image source={require('../../assets/userProfile.png')}
                                     style={{
                                         marginLeft: 20, height: 70, width: 70,
                                         borderRadius: 35,
                                         marginTop: 15
                                     }}/>
                            ))
                        }

                        <View style={{marginLeft: 20, marginTop: 10, width: '55%', justifyContent: 'center'}}>
                            <BoldText style={{fontSize: 19, color: '#626464'}}>
                                {this.state.username}
                            </BoldText>
                            <ItalicText style={{width: '100%', color: 'gray', marginTop: '1%'}}>
                                {this.state.userData ? this.state.userData.user.city + ',' + this.state.userData.user.state : ''}
                            </ItalicText>
                            <View style={{width: 80, marginTop: '1%'}}>
                                <StarRating
                                    disabled={false}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    starSize={15}
                                    width={50}
                                    rating={this.state.userData ? Number(this.state.userData.user.averageRating) : 0}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    fullStarColor={'#14DFC1'}
                                />
                            </View>
                        </View>
                        <View style={{justifyContent: 'center'}}>
                            {/* <TouchableOpacity onPress={() => {
                            }}
                                              style={{marginLeft: '5%'}}>
                                <Image style={{height: 25, width: 25,}}
                                       source={require('../../assets/question-circle.png')}
                                       resizeMode='contain'/>
                            </TouchableOpacity> */}
                        </View>
                    </View>
                    <SegmentedControlTab
                        values={['Items', 'Wishlist']}
                        selectedIndex={this.state.customStyleIndex}
                        onTabPress={this.handleCustomIndexSelect}
                        borderRadius={0}
                        tabsContainerStyle={{
                            height: 50,
                            backgroundColor: '#ffffff',
                            borderWidth: 0.5,
                            borderColor: '#ffffff'
                        }}
                        tabStyle={{backgroundColor: '#ffffff', borderWidth: 0, borderColor: 'transparent'}}
                        activeTabStyle={{
                            backgroundColor: '#ffffff', marginTop: 0, borderWidth: 1,
                            borderColor: '#ffffff', borderBottomWidth: 1,
                            borderBottomColor: 'rgba(52, 219, 184, 1)'
                        }}
                        tabTextStyle={{
                            color: '#888888', fontWeight: 'bold', fontSize: 20,
                        }}
                        activeTabTextStyle={{color: '#888888'}}/>
                    {this.state.customStyleIndex === 0 &&
                    <View>
                        {
                            this.state.itemDetailValues.length ? (<FlatList
                                removeClippedSubviews={true}
                                data={this.state.itemDetailValues}
                                renderItem={({item}) => <TouchableOpacity
                                    style={{
                                        height: 70,
                                        marginTop: '2%',
                                        marginLeft: '5%',
                                        marginRight: '5%',
                                        borderRadius: 8,
                                        borderColor: 'gray',
                                        borderWidth: 1,
                                        width: '90%',
                                        flexDirection: 'row',
                                        marginBottom:'1%'
                                    }}>
                                    <Image
                                        style={{margin: 12, height: 50, width: 50,}}
                                        source={{uri: item.pictures[0].picture}}
                                    />
                                    <View style={{marginLeft: '2%', width: '45%', justifyContent: 'center'}}>
                                        <RegularText numberOfLines={1}
                                                     style={{fontSize: 12}}>{item.category.name}</RegularText>
                                        <BoldText numberOfLines={1} style={{
                                            marginTop: '1%',
                                            fontSize: 12,
                                            color: '#000000',
                                            fontWeight: '600'
                                        }}>{item.title}</BoldText>
                                    </View>
                                    <View style={{width: '30%', justifyContent: 'center'}}>
                                        <ItalicText numberOfLines={1} style={{
                                            fontSize: 10,
                                            fontStyle: 'italic',
                                            textAlign: 'right',
                                            marginRight: '11%',
                                            marginTop: '3%'
                                        }}>posted {item.days} days ago</ItalicText>
                                    </View>
                                </TouchableOpacity>}
                            />) : (
                                <View style={{marginTop: '15%', alignItems: 'center'}}>
                                    <RegularText
                                        style={{
                                            justifyContent: 'center',
                                            color: '#888888', fontWeight: 'bold', fontSize: 20,
                                        }}
                                    >
                                        No Items Found
                                    </RegularText>
                                </View>
                            )
                        }
                    </View>}
                    {this.state.customStyleIndex === 1 &&
                    <View>
                        {
                            this.state.wishListDetailValues.length ? (<FlatList
                                removeClippedSubviews={true}
                                data={this.state.wishListDetailValues}
                                renderItem={({item}) =>

                                    <TouchableOpacity
                                        style={{
                                            height: 70,
                                            marginTop: '2%',
                                            marginLeft: '5%',
                                            marginRight: '5%',
                                            borderRadius: 8,
                                            borderColor: 'gray',
                                            borderWidth: 1,
                                            width: '90%',
                                            flexDirection: 'row'
                                        }}>
                                        {item.item ?
                                            <View style={{width: 55, height: 70, justifyContent: 'center',}}>
                                                {
                                                    item.item.pictures ? (
                                                        <Image style={{margin: '10%', height: 50, width: 50}}
                                                       source={{uri: item.item.pictures[0].picture}}/>
                                                    ) : (null)
                                                }
                                                
                                            </View>
                                            : (null)
                                        }
                                        <View style={{
                                            width: '70%',
                                            height: '90%',
                                            marginLeft: '2%',
                                            justifyContent: 'center'
                                        }}>
                                            <RegularText numberOfLines={1}
                                                         style={{fontSize: 13}}>{(item.category) ? item.category.name : ''}</RegularText>
                                            <BoldText numberOfLines={1} style={{
                                                fontSize: 15,
                                                fontWeight: 'bold',
                                                marginTop: '1%'
                                            }}>{item.name}</BoldText>
                                        </View>

                                    </TouchableOpacity>

                                }
                            />) : (
                                <View style={{marginTop: '15%', alignItems: 'center'}}>
                                    <RegularText
                                        style={{
                                            justifyContent: 'center',
                                            color: '#888888', fontWeight: 'bold', fontSize: 20,
                                        }}
                                    >
                                        No Items Found
                                    </RegularText>
                                </View>
                            )
                        }
                    </View>}


                </View>
                {this.optionModal()}
                {/* <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0,0,0,0.1)'
                    textStyle={{color: '#00DBBB'}}
                /> */}
               
            </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#ffffff'
    },
    modalContent: {
        ...Platform.select({
            android: {
                top: '17%',
                width: '90%',
                marginLeft: '5%',
                marginRight: '5%',
                height: '55%',
                marginTop: '18%',
                backgroundColor: 'white',
                borderWidth: 1,
                borderRadius: 4,
                borderColor: 'rgba(0, 0, 0, 0.1)',
            },
            ios: {
                width: '90%',
                marginLeft: '5%',
                marginRight: '5%',
                marginTop: '-10%',
                top: '30%',
                height: '75%',
                backgroundColor: 'white',
                padding: 0,
                borderWidth: 1,
                borderRadius: 4,
                borderColor: 'rgba(0, 0, 0, 0.1)',
            }
        })
    },
    container: {
        flex: 1,
        width: null,
        height: null
    },
    icon: {
        width: 23,
        height: 23,
    },
    tabStyle: {
        borderColor: '#D52C43'
    },
    activeTabStyle: {
        backgroundColor: '#D52C43'
    },
    tabTextStyle: {
        color: '#D52C43'
    },
    modalBackground: {
        flex: 1,
        backgroundColor: '#00000040'
    },
});
