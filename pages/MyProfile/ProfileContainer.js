import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    AsyncStorage,
    Platform,
    Modal,
    TouchableWithoutFeedback,
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';

import SegmentedControlTab from 'react-native-segmented-control-tab'
import StarRating from 'react-native-star-rating';
import ImagePicker from 'react-native-image-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import Items from './Items';
import Wishlist from './Wishlist';
import TradesList from '../TradesList'
import api from '../../api';
import navigate from '../../Components/navigate';
import EditProfileModal from '../../Components/EditProfileModal';
import ChatEngineProvider from "../Chat/ce-core-chatengineprovider";

import {
    BoldText,
    ItalicText
} from '../../Components/styledTexts';


export default class ProfileContainer extends Component {

    //Create Top Navigation Bar 
    static navigationOptions = ({navigation}) => ({
        headerTitle: 'My Profile',
        tabBarLabel: 'Home',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../../assets/tab2.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() =>{
                                  AsyncStorage.getItem('profileCompleted').then((value) => {
                                      value === '1' ? navigation.navigate('PostItem') : alert('Please create a profile before posting the item.')
                                  });
                                  }}
            >
                <Image
                    source={require('../../assets/white_plus_icon.png')}
                    style={{marginLeft: 15, height: 28, width: 28}}
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity
                onPress={() =>{
                     navigation.state.params.openOptionModel()
                }}
                activeOpacity={0.5}>
                <Image
                    source={require('../../assets/logo.png')}
                    style={{marginRight: 15, height: 40, width: 40}}
                />
            </TouchableOpacity>
        ),
        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 5,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: 'white',
                fontFamily: "NolanNext-Bold",
            }
    });

    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
            username: '',
            email:'',
            visibleModal: false,
            customStyleIndex: 0,
            starCount: 3.5,
            latitude: 0.0,
            longitude: 0.0,
            loading: false,
            address: '',
            city: '',
            state: '',
            country: '',
            userId: '',
            spinner: false,
            profile_pic: '',
            profile_pic_uri: require('../../assets/edit-profile.png'),
            itemDetailValues: [],
            wishListDetailValues: [],
            completeData: '',
            listTradeData: "",
            profileCompleted: false,
            optionModel: false,
        }
        
    }

    hideModal() {
        this.setState({visibleModal: false});
    }

    // Get User Details From Local Storage 
    async getUserDetailValues() {
        console.log("getUserDetailValues called")
        var _this = this;
        _this.setState({
            spinner: true
        });
        await AsyncStorage.getItem('userDetailValues').then(data => {
            console.log("data jkdl jslksjd")
            console.log(data)
            // if(data !== null){
                var detailvalue = JSON.parse(data);
                _this.state.userId = detailvalue.user.id;
                _this.getPageContent(this.state.userId);
                _this.getItemDetailValues();
            // }
           
        });
    }

// Get user Item List 
    async getItemDetailValues() {

        var _this = this;
        let response = await api.getRequest('/user/items', 'GET');
        if (response.status === 200) {
            response.json().then(function (data) {
                console.log("response getItemDetailValues")
                console.log(data.items)
                _this.setState({
                    spinner: false,
                    itemDetailValues: data.items
                });
            });
        } else {
            _this.setState({
                spinner: false
            });
            alert('Error on geting Items.')
            this.getItemDetailValues();
        }
    }

    // Get User Full Details 
    async getPageContent(userId) {
        console.log("getPageContent called")
        var _this = this;
        let response = await api.getRequest('/users/' + userId, 'GET');
        if (response.status === 200) {
            response.json().then(function (data) {
                if (data.user.picture) {
                    _this.setState({
                        profile_pic_uri: {uri: data.user.picture}
                    });
                }
                _this.setState({
                    spinner: false,
                    address: data.user.address,
                    username: data.user.first_name,
                    email:data.user.email,
                    starCount: Number(data.user.averageRating),
                    completeData: data,
                    city: data.user.city,
                    state: data.user.state,
                    country: data.user.country
                });
                console.log(data.user)
                if(data.user.latitude !== null) {
                    // Store Flag Value for profile completion 
                    AsyncStorage.setItem('profileCompleted', '1');
                    _this.setState({
                        profileCompleted: true
                    });
                }
            });
        } else {
            _this.setState({
                spinner: false
            });
            // alert('Unable to get User Data')
            this.getPageContent(this.state.userId);

        }
    }

    componentDidMount() {
        const {params} = this.props.navigation.state;

        if (params && params.completeTradeData) {
            this.setState({listTradeData: params.completeTradeData})
        }
        this.props.navigation.setParams({
            openModal: this.modal.bind(this),
            openOptionModel:this.optionModel.bind(this)
        });
        // Get Latitude & Longitude of Current Device
        this.getLatLongt();
        //Get User Detail Value
        this.getUserDetailValues();
    }

// Get Latitude & Longitude of Current Device
    async getLatLongt() {
        var _this = this;
        navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log("Location updated " + position.coords.latitude)
                _this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                });
            },
            (error) => {
                console.log("Error in fatching location  " + JSON.stringify(error));
            },
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000},
        );
        console.log("Get location method")
        Geolocation.getCurrentPosition(
            (position) => {
                console.log(position);
                _this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                });
            },
            (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );

        
    }


    // Image Picker For select profile image from Device
    openImagePicker() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                let source = {uri: response.uri};
                this.setState({
                    profile_pic: 'data:image/png;base64,' + response.data, // get image Data in base64 format
                    profile_pic_uri: source
                })
            }
        });
    }

    // Option Model
    optionModel(){
        console.log('inside option model');
        this.setState({
            optionModel:!this.state.optionModel
        })
    }

// Edit Profile model
    modal() {
        this.setState({
            visibleModal: true
        })
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }

// Open Edit Profile View for change details
    setEditProfileModalVisible = (isVisible) => {
        this.setState({
            visibleModal: false
        })
    };

//Validate email
    validateEmail(text) {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        if(reg.test(text) === false)
        {return false}
        else {return true}
    }

// Save user Profile Details on sarver
    async submitProfileDetail() {
        var _this = this;
        if (_this.state.address !== '' && _this.state.username !== '' && _this.state.email !== '' &&
             _this.state.address !== null && _this.state.username !== null && _this.state.email !== null) {
                
                let checkEmail = this.validateEmail(_this.state.email)
                if (checkEmail){
                    this.setState({loading: true});
                    let body = {};
                    if (this.state.profile_pic) {
                        body = {
                            "user": {
                                "first_name": _this.state.username,
                                "email": _this.state.email,
                                "address": _this.state.address,
                                "latitude": _this.state.latitude,
                                "longitude": _this.state.longitude,
                                "picture": _this.state.profile_pic,
                                "city": _this.state.city,
                                "state": _this.state.state,
                                "country": _this.state.country
                            }
                        };
                    } else {
                        body = {
                            "user": {
                                "first_name": _this.state.username,
                                "email": _this.state.email,
                                "address": _this.state.address,
                                "latitude": _this.state.latitude,
                                "longitude": _this.state.longitude,
                                "city": _this.state.city,
                                "state": _this.state.state,
                                "country": _this.state.country
                            }
                        };
                    }
                    console.log("Body...")
                    console.log(body)
                    try {
                        let response = await api.request('/users/' + this.state.userId, 'PUT', body);
                        console.log("resopponse edit")
                        console.log(response)
                        
                        if (response.status === 200) {
                            let { state, city } = _this.state;
                            if(state && city) {
                                _this.setState({
                                    profileCompleted: true,
                                    visibleModal: false
                                });
                                AsyncStorage.setItem('username', _this.state.username)
                                AsyncStorage.setItem('profileCompleted', '1');
                            }
                            _this.setState({loading: false, visibleModal: false});
                            response.json().then((data) => {
                            });
                        } else {
                            await response.json().then((res) => {});
                            _this.setState({loading: false, visibleModal: false});
                        }
                    } catch (error) {
                        _this.setState({loading: false, visibleModal: false});
                    }

                }else {
                    alert('Please enter valid email id.')
                }
           
        } else {
            alert('Please fill all the required fields.')
        }
    }

    navigationFromModal() {
        this.props.navigation.navigate('Category')
    }

// Get Detail address by Google of current Latitude & Longitude 

    async getLocation() {
        let _this = this;
        if (this.state.latitude && this.state.longitude) {
            this.setState({loading: true})
            let url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='
            let response = await api.getLocationRequest(url + this.state.latitude + ',' + this.state.longitude + '&key=AIzaSyBy4SEL5Madg88ynd8ZbqARpsGMeBTT1As');
            if (response.status === 200) {
                try {
                    response.json()
                        .then(function (data) {
                            let addressData = data['results'][data['results'].length - 3]['formatted_address']
                            let addressArray = addressData.split(',')
                            _this.setState({
                                loading: false,
                                city: addressArray[0],
                                state: addressArray[1],
                                country: addressArray[2],
                                address: data['results'][0]['formatted_address']
                            })
                        })
                } catch (error) {
                    this.setState({loading: false})
                    alert("There was an error saving your contract.")
                }
            } else {
            }
        } else {
            this.getLatLongt();
            alert('Please wait trying to fetch your location.')
        }
    }

    handleCustomIndexSelect = (index) => {
        this.setState({
            ...this.state,
            customStyleIndex: index,
        });
    };

    // Read address from Local storage 
    getAddress() {
        let { state, city } = this.state;
        if(state && city) {
            return `${city}, ${state}`;
        }
        return '';
    }

    logout(){
        console.log("Logout called ......")
        ChatEngineProvider.logout();
        this.props.navigation.navigate('Login')
    }

    optionModal() {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.optionModel}
                onRequestClose={() => {
                    this.setState({optionModel: false})
                }}>
                <TouchableWithoutFeedback onPress={() => this.setState({optionModel: false})}>
                    <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.1)'}}>
                        <View style={{
                            backgroundColor: '#909090', borderRadius: 7, width: 100, padding: 10, alignSelf: 'flex-end',
                            top: 64, right: '3%'
                        }}>
                            <TouchableOpacity style={{alignItems:'center'}}
                            onPress={() => {
                                this.setState({optionModel: false})
                                this.logout()
                                }} >
                                <ItalicText style={{marginTop:0,fontSize: 16, textAlign:'center'}}>Logout</ItalicText>
                             </TouchableOpacity>
                            {/* </View> */}
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }


    // UI Desigin
    render() {
        return (
            <ScrollView ref='scrollView' style={styles.scrollContainer}>
                <View style={styles.container}>
            
                    <View style={{height: 100, flexDirection: 'row'}}>
                       <TouchableOpacity 
                       onPress={() =>
                        this.modal()
                        }>
                        <Image source={this.state.profile_pic_uri}
                               style={{
                                   marginLeft: 20, height: 70, width: 70,
                                   borderRadius: 35,
                                   marginTop: 15
                               }}
                        />
                        </TouchableOpacity>
                        <View style={{marginLeft: 20, marginTop: 10, width: '55%', justifyContent: 'center'}}>
                            <BoldText style={{fontSize: 19, color: '#626464'}}>
                                {this.state.username}
                            </BoldText>
                            <ItalicText style={{width: '100%', color: 'gray', marginTop: '1%'}}>
                                {this.getAddress()}
                            </ItalicText>
                            <View style={{width: 80, marginTop: '1%'}}>

                        {/* Show User current Rasting */}
                                <StarRating
                                    disabled={true}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    starSize={15}
                                    width={50}
                                    emptyStarColor={'#14DFC1'}
                                    rating={this.state.starCount}
                                    fullStarColor={'#14DFC1'}
                                />
                            </View>
                        </View>
                        <View style={{justifyContent: 'center'}}>
                        </View>
                    </View>

                    {/* Show Three Tab For ' Items, Wishlist, Trades' */}
                    <SegmentedControlTab
                        values={['Items', 'Wishlist']}
                        selectedIndex={this.state.customStyleIndex}
                        onTabPress={this.handleCustomIndexSelect}
                        borderRadius={0}
                        tabsContainerStyle={{
                            height: 50,
                            backgroundColor: '#ffffff',
                            borderWidth: 0.5,
                            borderColor: '#ffffff',
                        }}
                        tabStyle={{backgroundColor: '#ffffff', borderWidth: 0, borderColor: 'transparent'}}
                        activeTabStyle={{
                            backgroundColor: '#ffffff', marginTop: 0, borderWidth: 1,
                            borderColor: '#ffffff', borderBottomWidth: 2,
                            borderBottomColor: 'rgba(52, 219, 184, 1)'
                        }}
                        tabTextStyle={{
                            color: '#888888', fontFamily: 'NolanNext-Medium', fontSize: 20,
                        }}
                        activeTabTextStyle={{color: '#888888'}}/>
                        {/* Item List */}
                    {this.state.customStyleIndex === 0 &&
                    <View>
                        <Items
                        //Create New Item
                            goToPostItemScreen={() => navigate.navigateTo(this.props.navigation, 'PostItem')}
                            //See Item full Details
                            goToYourItemScreen={(itemId, index) => navigate.navigateTo(this.props.navigation, 'ItemDetails', {
                                itemId: itemId,
                                index: index,
                                myItem: true,
                                item: this.state.completeData,
                                detailData: this.state.itemDetailValues
                            })}
                            itemDetailValues={this.state.itemDetailValues}
                            profileCompleted={this.state.profileCompleted}
                        />
                    </View>}
                    {/* WishList  */}
                    {this.state.customStyleIndex === 1 &&
                    <View>
                        <Wishlist test={this.navigationFromModal.bind(this)}
                                  navigation={this.props.navigation}
                        />
                    </View>}
                    {/* Trade List */}
                    {/* {this.state.customStyleIndex === 2 &&
                    <View>
                        <TradesList
                            navigateToTradeStep={(obj) => navigate.navigateTo(this.props.navigation, 'TradeDetails', {
                                object: obj,
                                completeUserData: this.state.completeData
                            })}

                            navigateToOfferDetails={(obj, index) => navigate.navigateTo(this.props.navigation, 'OfferDetails', {
                                myItem: obj,
                                isOfferCreated: index
                            })}
                        />
                    </View>} */}
                    {/* Edit Profile Popup UI */}
                    <EditProfileModal
                        visibleModal={this.state.visibleModal}
                        onChangeText={(username) => this.setState({username})}
                        onChangeTextEmail={(email) => this.setState({email})}
                        setEditProfileModalVisible={this.setEditProfileModalVisible}
                        address={this.getAddress()}
                        username={this.state.username}
                        email={this.state.email}
                        getLocation={this.getLocation.bind(this)}
                        openImagePicker={this.openImagePicker.bind(this)}
                        profile_pic_uri={this.state.profile_pic_uri}
                        submitProfileDetail={this.submitProfileDetail.bind(this)}
                        loading={this.state.loading}
                    />

        {this.optionModal()}

                </View>
                {/* Loader  */}
                <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.6)'
                    textStyle={{color: '#00DBBB'}}
                />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#ffffff'
    },
    modalContent: {
        ...Platform.select({
            android: {
                top: '17%',
                width: '90%',
                marginLeft: '5%',
                marginRight: '5%',
                height: '55%',
                marginTop: '18%',
                backgroundColor: 'white',
                borderWidth: 1,
                borderRadius: 4,
                borderColor: 'rgba(0, 0, 0, 0.1)',
            },
            ios: {
                width: '90%',
                marginLeft: '5%',
                marginRight: '5%',
                marginTop: '-10%',
                top: '30%',
                height: '75%',
                backgroundColor: 'white',
                padding: 0,
                borderWidth: 1,
                borderRadius: 4,
                borderColor: 'rgba(0, 0, 0, 0.1)',
            }
        })
    },
    container: {
        flex: 1,
        width: null,
        height: null,
    },
    icon: {
        width: 23,
        height: 23,
    },
    tabStyle: {
        borderColor: '#D52C43'
    },
    activeTabStyle: {
        backgroundColor: '#D52C43'
    },
    tabTextStyle: {
        color: '#D52C43'
    },
    modalBackground: {
        flex: 1,
        backgroundColor: '#00000040'
    },
});
