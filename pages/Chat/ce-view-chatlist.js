import React from 'react';
import {ListItem, Text} from "react-native-elements";
import {SafeAreaView, ScrollView} from "react-native";
import ChatEngineProvider from "./ce-core-chatengineprovider";
import styles from "./ce-theme-style";

class ChatList extends React.Component {
    PUBLIC_CHANNEL_PREFIX = "chat-engine#chat#public.#";


    constructor(props) {
        super(props);


        this.state = {
            users: [],
            first_name: '',
            address: '',
            username: '',
            userId: '',
            id: '',
            name: '',
            mobile: '',
            profile_pic: '',
            profile_pic_uri: require('../../assets/userProfile.png'),
        }


        this._username = ''
        this._name = ''
        this._password = ''
    }


    componentDidMount() {
        this.getUserDetailValues()
        this.getUDetailValues()
        this.getUsers()
    }

    async getUserDetailValues() {
        var _this = this
        _this.setState({})
        await AsyncStorage.getItem('userDetailValues').then(data => {
            var detailvalue = JSON.parse(data);
            _this.state.userId = detailvalue.user.id
            _this.state.access_token = detailvalue.access_token;
            _this.getPageContent(this.state.userId)
            _this.getUsers(detailvalue.access_token)
        });
    }


    async getPageContent(userId) {
        var _this = this
        let response = await api.getRequest('/users/' + userId, 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    if (data.user.picture) {
                        _this.setState({
                            profile_pic: {uri: data.user.picture}
                        })
                    }
                    _this.setState({
                        userId: data.user.id,
                        address: data.user.address,
                        username: data.user.first_name,
                        mobile: data.user.mobile,
                        id: data.user.id,
                        name: data.user.name,
                        first_name: data.user.first_name

                    })
                })
        } else {
            _this.setState({})
            alert(response)
        }
    }


    async getUsers(access_token) {
        var _this = this
        let response = await api.getRequest('/users', 'GET', 'Bearer ' + access_token);

        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    _this.setState({
                        users: data.users
                    });
                })
        } else {
        }
    }

    getUDetailValues = (value) => {
        this.setState({
            userId: value
        })
    };

    emptyState() {
        var channels = {};

        if (this.props && this.props.defaultChannels) {
            this.props.defaultChannels.split(",").forEach((x) => {
                channels[x] = {};
            });
        }

        return {
            chatList: channels
        };
    }

    refresh() {
        let ChatEngine = ChatEngineProvider.get();
        let state = this.emptyState();

        if (ChatEngine && ChatEngine.chats) {
            Object.keys(ChatEngine.chats).forEach((x) => {
                if (x.indexOf(ChatList.PUBLIC_CHANNEL_PREFIX) === 0) {
                    let friendly = x.substring(ChatList.PUBLIC_CHANNEL_PREFIX);
                    this.emptyState().channels[friendly] = {};
                }
            });
        }

        this.setState(() => {
            return state;
        });
    }

    componentDidMount() {
        var self = this;

        this._sub = this.props.navigation.addListener('didFocus', () => {
            self.refresh();
        });
    }

    componentWillUnmount() {
        this._sub.remove();
    }

    gotoChat(channel) {
        var self = this;

        ChatEngineProvider.getChatRoomModel().connect(channel).then(() => {
            self.props.navigation.navigate('Application', {
                title: "#" + channel
            });
        }, (reject) => {
            alert(reject);
        });
    }

    renderChatList() {
        let chatList = this.emptyState().chatList;

        return Object.keys(chatList).map(channelFriendly =>
            <ListItem
                key={channelFriendly}

                title={channelFriendly.replace(this.props.navigation.state.params.mobile, '')}
                subtitle=""
                leftIcon={{name: "lock-open"}}
                onPress={() => this.gotoChat(channelFriendly)}
            />
        )
    }

    render() {


        return (
            <SafeAreaView>
                <ScrollView style={styles.list}>


                    <Text h4 style={{paddingBottom: 10}}>Select User To Chat</Text>

                    {this.renderChatList()}

                </ScrollView>
            </SafeAreaView>
        );
    }
}

export default ChatList;
