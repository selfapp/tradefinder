import {
    TextInput,
    View,
    Image,
    TouchableOpacity,
    Platform,
    Dimensions
} from 'react-native';
import React from "react";
import NameTypingIndicator from "./ce-view-nametypingindicator";
import ChatEngineProvider from "./ce-core-chatengineprovider";
import ChatEngineProviderMe from './ce-core-chatengineproviderme';
import ThemeColors from "./ce-theme-colors";

const d = Dimensions.get("window")

const isiPhoneX = Platform.OS === "ios" && (d.height > 800 || d.width > 800) ? true : false

class MessageEntry extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            chatInput: "",
        };

        this.setChatInput = this.setChatInput.bind(this);
    }

    sendChat() {
        var self = this;

        if (self.state.chatInput) {
            var chatMessage = this.props.chatRoomModel.state.chat;
            var chat = ChatEngineProviderMe.getChatRoomModel().state.chat;

            chatMessage.emit("message", {
                text: self.state.chatInput,
                sentAt: new Date().toISOString(),
                from: {
                    uuid: ChatEngineProviderMe._uuid,
                    email: ChatEngineProviderMe._username,
                    name: ChatEngineProviderMe._name
                }
            });

            chat.emit("message", {
                text: self.state.chatInput,
                sentAt: new Date().toISOString(),
                from: {
                  uuid: ChatEngineProviderMe._uuid,
                  email: ChatEngineProviderMe._username,
                  name: ChatEngineProviderMe._name
                }
              });

            this.setState({chatInput: ""});
        }
    }

    setChatInput(value) {
        this.setState({chatInput: value});
        if (this.props.typingIndicator) {
            var chat = this.props.chatRoomModel.state.chat;

            if (value !== "") {
                chat.typingIndicator.startTyping();
            } else {
                chat.typingIndicator.stopTyping();
            }
        }
    }

    onTypingIndicator() {
        return (<NameTypingIndicator chatRoomModel={ChatEngineProvider.getChatRoomModel()}/>);
    }

    render() {

        // console.log("is iphpone 10", isiPhoneX);
        var marginbottom = 0
        if(isiPhoneX) {
            marginbottom = 30
        }
        return (
           
            <View>
                {this.onTypingIndicator()}
                <View style={{ 
                    backgroundColor: ThemeColors.footer,
                    flexDirection: 'row',
                    width: '100%', height: 50,marginBottom:marginbottom
                  }}>
                    <TextInput
                        autoCorrect={false}
                        value={this.state.chatInput}
                        style={{ fontSize: 18,
                            height: 40,
                            backgroundColor: ThemeColors.input_bg,
                            color: ThemeColors.instructions,
                            marginLeft: '8%', marginTop: '1.5%',
                            width: '80%'
                        }}
                        multiline={true}
                        returnKeyType={'next'}
                        underlineColorAndroid="transparent"
                        placeholder="Type Message"
                        onChangeText={this.setChatInput}
                        // onSubmitEditing={() => {
                        //     this.sendChat();
                        // }}
                    />
                    <TouchableOpacity
                    onPress={()=>{
                        this.sendChat();    
                    }}
                    >
                    <Image
                    resizeMode='contain'    
                    style={{ marginTop: '45%', height: 25, width: 25 }}
                    source={require('../../assets/navigation.png')} resizeMode='cover'
                    />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default MessageEntry;