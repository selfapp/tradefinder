import ChatList from "./ce-view-chatlist";
import React from 'react';
import styles from "./ce-theme-style";
import {
    Platform,
    StyleSheet,
    ActivityIndicator,
    ImageBackground,
    Button,
    FlatList,
    Image,
    KeyboardAvoidingView,
    TextInput,
    TouchableOpacity,
    View,
    Text
} from 'react-native';
import ChatEngineProvider from "./ce-core-chatengineprovider";
import util from "util";
import ThemeStyle from "./ce-theme-style";
import ThemeColors from "./ce-theme-colors";
import api from '../../api';

class ChatListScreen extends React.Component {
    static navigationOptions = {
        title: 'Chat List',
    };


    constructor(props) {
        super(props);


        this.state = {
            array1: [],
            array2: [],
            users: [],
            address: '',
            username: '',
            userId: '',
            id: '',
            name: '',
            mobile: '',
            first_name: '',
            profile_pic: '',
            profile_pic_uri: require('../../assets/userProfile.png'),
        };

        this._username = '';
        this._name = '';
        this._password = '';
    }


    componentDidMount() {
        this.getUserDetailValues()
        this.getUDetailValues()
        this.getUsers()
    }

    async getUserDetailValues() {
        var _this = this
        _this.setState({})
        await AsyncStorage.getItem('userDetailValues').then(data => {
            var detailvalue = JSON.parse(data);
            _this.state.userId = detailvalue.user.id
            _this.state.access_token = detailvalue.access_token;
            _this.getPageContent(this.state.userId)
            _this.getUsers(detailvalue.access_token)
        });
    }


    async getPageContent(userId) {
        var _this = this
        let response = await api.getRequest('/users/' + userId, 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    if (data.user.picture) {
                        _this.setState({
                            profile_pic: {uri: data.user.picture}
                        })
                    }
                    _this.setState({
                        userId: data.user.id,
                        address: data.user.address,
                        username: data.user.first_name,
                        mobile: data.user.mobile,
                        id: data.user.id,
                        name: data.user.name,
                        first_name: data.user.first_name

                    })
                })
        } else {
            _this.setState({})
            alert(response)
        }
    }


    async getUsers(access_token) {
        var _this = this
        let response = await api.getRequest('/users', 'GET', 'Bearer ' + access_token);

        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    _this.setState({
                        users: data.users
                    })
                })
        } else {
        }
    }


    getUDetailValues = (value) => {
        this.setState({
            userId: value
        })
    }


    render() {
        var id = this.state.users.map(function (item) {
            return item.id
        });

        let userId = this.state.userId;

        let name2 = this.state.users.map((data) => {
            if (this.props.navigation.state.params.mobile > data.mobile) {
                return this.props.navigation.state.params.mobile + data.mobile
            } else {
                return data.mobile + this.props.navigation.state.params.mobile
            }

            if (data.mobile == null)
                return data.mobile
        });

        let phone = this.state.users.map((data) => {
            return data.mobile
        });


        let arr = name2.filter(Boolean).join(",");
        let arr2 = phone.filter(Boolean).join(",");

        this.state.array1 = arr;
        this.state.array2 = arr2;


        let mobile = this.state.users.map((data) => {
            return data.mobile
        });

        this.state.mobile = mobile;

        let self = this;
        let props = {
            defaultChannels: arr
        };

        return (
            <View style={ChatListStyles.chatlist_container}>
                <ImageBackground style={ChatListStyles.chatlist_wallpaper}
                                 source={require('./images/login_gradient.png')}>
                    <ChatList  {...props}
                               navigation={self.props.navigation}/>
                </ImageBackground>
            </View>
        );
    }
}

const ChatListStyles = StyleSheet.create({
    chatlist_container: {
        flex: 1,
        flexDirection: 'column',
        padding: 0
    },
    chatlist_wallpaper: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        width: ThemeStyle.DEVICE_WIDTH,
        height: ThemeStyle.DEVICE_HEIGHT,
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 20,
        paddingRight: 20
    },
    chatlist_logo: {
        width: ThemeStyle.DEVICE_WIDTH - 40,
        alignItems: 'center'
    },
    chatlist_input: {
        width: ThemeStyle.DEVICE_WIDTH - 40,
        color: ThemeColors.input_dark_textcolor,
        borderRadius: 20,
        paddingHorizontal: 20,
        fontSize: 18,
        height: 40,
        backgroundColor: ThemeColors.input_bg,
        marginBottom: 10,
        padding: 10
    },
    chatlist_button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: ThemeColors.button_background,
        paddingVertical: 15,
        borderRadius: 20
    },
    chatlist_buttonText: {
        color: ThemeColors.button_text,
        textAlign: 'center',
        fontWeight: '700'
    }
});

export default ChatListScreen;


class B extends React.Component {
    render() {
        return (
            <View>
                <Text>{this.props.array2}
                </Text>
            </View>
        );
    }
}
