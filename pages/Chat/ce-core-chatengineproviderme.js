import React from "react";
// import {connect} from 'react-redux';
import ChatEngineCore from "chat-engine";
import ChatRoomModelMe from "./ce-model-chat-chatroomme";
import pnSettings from "./ex-config-pnsettings";
import {plugin} from 'chat-engine-notifications';

import sha1 from "sha1";
import gravatar from "gravatar";
import {Alert, AsyncStorage, Platform} from "react-native";
import { showMessage } from "react-native-flash-message";
/**
 * The central point for obtaining the ChatEngine instance from around the application.
 * We maintain a single chat room model consistent with a simple one-chat-at-a-time application.
 */
class ChatEngineProviderMe {
  static ASYNC_STORAGE_USERDATA_KEY = '@ChatEngineReactNativeSampleMe:_userdata_json';

  static _username = null;
  static _uuid = null;
  static _name = null;
  static _chatRoomModel = new ChatRoomModelMe();
  static _connected = false;
  static _chatEngine = ChatEngineProviderMe.__createChatEngine();

  static __createChatEngine() {
    return ChatEngineCore.create({
      publishKey: pnSettings.publishKey,
      subscribeKey: pnSettings.subscribeKey
    }, {
    //   debug: true
    });
  }

  static get() {
    return ChatEngineProviderMe._chatEngine;
  }

  static connect(username, name) {
    if (ChatEngineProviderMe._connected) {
      ChatEngineProviderMe.logout();
    }

    ChatEngineProviderMe._username = username;
    ChatEngineProviderMe._name = name || username;
    ChatEngineProviderMe._uuid = sha1(username);

    ChatEngineProviderMe._chatEngine = ChatEngineProviderMe.__createChatEngine();

    ChatEngineProviderMe._chatEngine.connect(
      ChatEngineProviderMe._uuid,
      {
        name: ChatEngineProviderMe._name,
        email: ChatEngineProviderMe._username,
        avatar_url: gravatar.url(ChatEngineProviderMe._username, {s: '100', r: 'x', d: 'retro'}, true),
        signedOnTime: new Date().getTime()
      },
      "auth-key"
    );

    return new Promise((resolve) => {

      console.log("chat engine ready .....");
      var ownerName = '';
      var userID ='';
      AsyncStorage.getItem('username').then((ownername) => {
        console.log("owner name", ownername);
        ownerName = ownername
      });

      AsyncStorage.getItem('userid').then((data)=> {
        let userId = JSON.parse(data);
        // userID = userObject.user.id;
        userID = "notify"+userId
        console.log("notify user id is ++++")
        console.log(userID)
      });

      ChatEngineProviderMe._chatEngine.on("$.ready", (data) => {
        console.log('chat engine start');

        ChatEngineProviderMe._chatEngine.me.plugin(plugin({
          events: ['message'],
          platforms: {ios: true, android: true},
          messageKey: 'text',
          formatter: (event) => {
            let payload = null;
            console.log("event details");
            console.log(event);
            const {chat, sender, data} = event;
            console.log("channel name " + chat.channel);

            let title = ownerName;
            let ticker = 'New message like';
            let body = data.text;

            payload = {
              apns: {aps: {alert: {title, body}, sound: "chime.aiff"}},
              gcm: {data: {contentTitle: title, contentText: body, ticker, defaults: [1]}}
            };
            console.log('INSTANCE FOR NOTIFICATION', data.to);
            return payload;
          }
        }));
        console.log("going for notification register.....");
        ChatEngineProviderMe._chatEngine.me.notifications.on('$notifications.registered', (token) => {
          // Store token, because we will need it later to enable push notifications on chat.
          console.log("app js device token");
          console.log(token);
          AsyncStorage.setItem("deviceToken", token);
        });

        ChatEngineProviderMe._chatEngine.me.notifications.on('$notifications.registration.fail', (error) => {
          let errorMessage = error.message || error.error.message;

          Alert.alert(
            'Device registration error',
            `Something went wrong during device registration: ${errorMessage}`,
            [{text: 'OK'}],
            {cancelable: true}
          );
        });

        ChatEngineProviderMe._chatEngine.me.notifications.on('$notifications.received', (notification) => {
          /**
           * We received remote notification. Store it for this moment and we cam mark it as seen
           * later with buttons.
           */
          console.log("notification received ...");
          console.log(notification)
          console.log("my user id is", userID)
         
          if(notification.notification.cepayload.chat.includes(userID)){
           
            AsyncStorage.getItem('otherUserChannelName').then((otherUserChannelName) => {
              console.log("otherUserChannelName is ", otherUserChannelName);
              console.log("notification.notification.cepayload.data.from.email", notification.notification.cepayload.data.from.email)

              if(parseInt(otherUserChannelName) !== parseInt(notification.notification.cepayload.data.from.email)){
              
                if(Platform.OS === "ios"){
                  showMessage({
                    message: notification.notification.aps.alert.title,
                    description: notification.notification.aps.alert.body,
                    type: "default",
                    backgroundColor: "white",
                    color: "#606060",
                  });
                }else {
                  showMessage({
                    message: notification.notification.contentTitle,
                    description: notification.notification.contentText,
                    type: "default",
                    backgroundColor: "white",
                    color: "#606060",
                  });
                }
                
              }
              
            });
           
          }else{
            console.log("Message send my same user ......not show top bar notification");


          }
          

        });

        ChatEngineProviderMe._chatEngine.me.notifications.registerNotificationChannels([
          {id: 'cennotifications', name: 'CENNotifications Channel'}
        ]);

        ChatEngineProviderMe._chatEngine.me.notifications.requestPermissions({
          alert: true,
          badge: false,
          sound: true
        })
          .then(permissions => console.log('Granted with permissions:', JSON.stringify(permissions)))
          .catch(error => console.log('Permissions request did fail:', error));


        ChatEngineProviderMe._connected = true;
        resolve(true);
      });
    });
  }

  static logout() {
    return AsyncStorage.removeItem(ChatEngineProviderMe.ASYNC_STORAGE_USERDATA_KEY).then(() => {

      var chat = this._chatRoomModel.state.chat;
      console.log("chat list .........")
      console.log(chat)
      console.log("jsdkfkjlsdskdf")

      AsyncStorage.getItem('deviceToken').then((token) => {
        console.log("inside token ChatEngineProviderMe+", token)

        if (token) {
          ChatEngineProviderMe._chatEngine.me.notifications.disableAll(token, (errorStatuses) => {
            if (errorStatuses) {
              // Handle push notification unregister error statuses.
              console.log("error status")
              console.log(errorStatuses)
            } else {
              // Device has been unregistered from push notification.
              console.log("Push notiifcation disabled .......");
            //  ChatEngineProviderMe._chatRoomModel.disconnect();

              //
              // FIXME - this isn't implemented but it should be?
              // https://www.pubnub.com/docs/chat-engine/reference/chatengine#disconnect
              //
              // ChatEngineProviderMe._chatEngine.disconnect();

              ChatEngineProviderMe._chatEngine = ChatEngineProviderMe.__createChatEngine();

              ChatEngineProviderMe._username = null;
              ChatEngineProviderMe._name = null;
              ChatEngineProviderMe._uuid = null;
            //  ChatEngineProviderMe._chatRoomModel = new ChatRoomModelMe();
              ChatEngineProviderMe._connected = false;
              ChatEngineProviderMe._chatEngine = null;
              AsyncStorage.getAllKeys()
              .then(keys => {
                keys = keys.filter((value, index, arr) => {
                  if (value !== 'deviceToken' && value != 'playerId') {
                    return value;
                  }
                });
                console.log("keys", keys)
                AsyncStorage.multiRemove(keys);
              });
              
            }
          });
        }else{
          ChatEngineProviderMe._username = null;
              ChatEngineProviderMe._name = null;
              ChatEngineProviderMe._uuid = null;
            //  ChatEngineProviderMe._chatRoomModel = new ChatRoomModelMe();
              ChatEngineProviderMe._connected = false;
              ChatEngineProviderMe._chatEngine = null;
          AsyncStorage.getAllKeys()
                .then(keys => {
                  keys = keys.filter((value, index, arr) => {
                    if (value !== 'deviceToken' && value != 'playerId') {
                      return value;
                    }
                  });
                  console.log("keys", keys)
                  AsyncStorage.multiRemove(keys);
                });
        }
      })
    });
  }

  static getChatRoomModel() {
    return this._chatRoomModel;
  }
}

export default ChatEngineProviderMe;
