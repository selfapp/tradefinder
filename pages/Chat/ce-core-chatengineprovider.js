import React from "react";
import {Alert } from 'react-native';
import ChatEngineCore from "chat-engine";
import ChatRoomModel from "./ce-model-chat-chatroom";
import pnSettings from "./ex-config-pnsettings";
import { plugin } from 'chat-engine-notifications';

import sha1 from "sha1";
import gravatar from "gravatar";
import {AsyncStorage} from "react-native";
import ChatEngineProviderMe from "./ce-core-chatengineproviderme"

let notifications = [];

class ChatEngineProvider {
    static ASYNC_STORAGE_USERDATA_KEY = '@ChatEngineReactNativeSample:_userdata_json';
    static ASYNC_STORAGE_USERDATA_NAME_KEY = '@ChatEngineReactNativeSample:_userdata_name_json';


    static _username = null;
    static _uuid = null;
    static _name = null;
    static _chatRoomModel = new ChatRoomModel();
    static _connected = false;
    static _chatEngine = ChatEngineProvider.__createChatEngine();

    static __createChatEngine() {
        return ChatEngineCore.create({
            publishKey: pnSettings.publishKey,
            subscribeKey: pnSettings.subscribeKey
        });
    }

    static get() {
        return ChatEngineProvider._chatEngine;
    }

    static connect(username, name) {
        
       
        if (ChatEngineProvider._connected) {
            ChatEngineProvider.logout();
        }

        ChatEngineProvider._username = username;
        ChatEngineProvider._name = name || username;
        ChatEngineProvider._uuid = sha1(username);

        ChatEngineProvider._chatEngine = ChatEngineProvider.__createChatEngine();
        
        ChatEngineProvider._chatEngine.connect(
           
          
            ChatEngineProvider._uuid,
            {
                name: ChatEngineProvider._name,
                email: ChatEngineProvider._username,
                avatar_url: gravatar.url(ChatEngineProvider._username, {s: '100', r: 'x', d: 'retro'}, true),
                signedOnTime: new Date().getTime()
            },
            "auth-key"
        );

        var engineReady = new Promise((resolve) => {
            console.log("chat engine ready .....")
            var ownerName = ''
        AsyncStorage.getItem('username').then((ownername)=> {
            console.log("owner name", ownerName)
            ownerName = ownername
        })
            ChatEngineProvider._chatEngine.once("$.ready", (data) => {
                    console.log('chat engine start')
                // ChatEngineProvider._chatEngine.me.plugin(plugin({
                //     events: ['message'],
                //     platforms: { ios: true, android: true },
                //     messageKey: 'text',
                //     formatter: (event) => {
                //         let payload = null;
                //         console.log("event details")
                //         console.log(event)
                //           const { chat, sender, data } = event;
                //           console.log("channel name "+ chat.channel)
                           
                //             let title = ownerName;
                //             let ticker = 'New message like';
                //             let body = data.text;
                            
                //             payload = {
                //             //   apns: { aps: { alert: { title, body } } },
                //             //   gcm: { data: { contentTitle: title, contentText: body, ticker } }
                //                 apns: { aps: { alert: { title, body }, sound: "chime.aiff"} },
                //                 gcm: { data: { contentTitle: title, contentText: body, ticker, defaults: [1] } }  
                //             };
                //             return payload;                         
                //       }
                //   }));
        //        console.log("going for notification register.....")
        //           ChatEngineProvider._chatEngine.me.notifications.on('$notifications.registered', (token) => {   
        //             // Store token, because we will need it later to enable push notifications on chat.
        //             console.log("app js device token")
        //             console.log(token)
        //             // Alert.alert(token)
        //             //deviceToken = token;  
        //             AsyncStorage.setItem("deviceToken", token);  
        //           });
        //                  ChatEngineProvider._chatEngine.me.notifications.on('$notifications.registration.fail', (error) => {
        //     let errorMessage = error.message || error.error.message;
          
        //     Alert.alert(
        //       'Device registration error',
        //       `Something went wrong during device registration: ${errorMessage}`,
        //       [{ text: 'OK' }],
        //       { cancelable: true }
        //     );
        //   });
          
        //   ChatEngineProvider._chatEngine.me.notifications.on('$notifications.received', (notification) => {
        //     /**
        //      * We received remote notification. Store it for this moment and we cam mark it as seen
        //      * later with buttons.
        //      */
        //     console.log("notification received ...")
        //     console.log(notification)
        //     notifications.push(notification);
        //   });
      
        //   ChatEngineProvider._chatEngine.me.notifications.registerNotificationChannels([
        //     { id: 'cennotifications', name: 'CENNotifications Channel' }
        //   ]);
      
        //   ChatEngineProvider._chatEngine.me.notifications.requestPermissions({ alert: true, badge: false, sound: true })
        // .then(permissions => console.log('Granted with permissions:', JSON.stringify(permissions)))
        // .catch(error => console.log('Permissions request did fail:', error));
               
    ChatEngineProvider._connected = true;
                resolve(true);
            });
        });

        return engineReady;
    }

    static logout() {
        return AsyncStorage.removeItem(ChatEngineProvider.ASYNC_STORAGE_USERDATA_KEY).then(() => {
          var chat = this._chatRoomModel.state.chat;
          console.log("chat list .........")
          console.log(chat)
          console.log("jsdkfkjlsdskdf")
          AsyncStorage.getItem('deviceToken').then((token)=> {
            console.log("inside token +", token)

            // if(token){

              // ChatEngineProvider._chatEngine.me.notifications.disableAll(token, (errorStatuses) => {
              //   if (errorStatuses) {
              //     // Handle push notification unregister error statuses.
              //     console.log("error status")
              //     console.log(errorStatuses)
              //   } else {
              //     // Device has been unregistered from push notification.
              //     console.log("Push notiifcation disabled .......");
              //     ChatEngineProvider._chatRoomModel.disconnect();
  
                  //
                  // FIXME - this isn't implemented but it should be?
                  // https://www.pubnub.com/docs/chat-engine/reference/chatengine#disconnect
                  //
                  // ChatEngineProvider._chatEngine.disconnect();
      
                  ChatEngineProvider._chatEngine = ChatEngineProvider.__createChatEngine();
      
                  ChatEngineProvider._username = null;
                  ChatEngineProvider._name = null;
                  ChatEngineProvider._uuid = null;
                  ChatEngineProvider._chatRoomModel = new ChatRoomModel();
                  ChatEngineProvider._connected = false;
                  ChatEngineProvider._chatEngine = null;
                  ChatEngineProviderMe.logout();
                  // AsyncStorage.clear();

            //     }
            //   });
            // }

            // AsyncStorage.getAllKeys()
            // .then(keys => {
            //     keys = keys.filter((value, index, arr) => {
            //         if(value !== 'deviceToken') {
            //             return value;
            //         }
            //     });
            //     console.log("keys", keys)
            //     AsyncStorage.multiRemove(keys);
            //   });
            // if(chat){
         
              // ChatEngineProvider._chatEngine.me.notifications.disable([chat], token, (errorStatuses) => {
                
                  // Push notification for global has been disabled.
                  
                 
              
            // }else {
            //   ChatEngineProvider._chatRoomModel.disconnect();
  
            //   //
            //   // FIXME - this isn't implemented but it should be?
            //   // https://www.pubnub.com/docs/chat-engine/reference/chatengine#disconnect
            //   //
            //   // ChatEngineProvider._chatEngine.disconnect();
  
            //   ChatEngineProvider._chatEngine = ChatEngineProvider.__createChatEngine();
  
            //   ChatEngineProvider._username = null;
            //   ChatEngineProvider._name = null;
            //   ChatEngineProvider._uuid = null;
            //   ChatEngineProvider._chatRoomModel = new ChatRoomModel();
            //   ChatEngineProvider._connected = false;
            //   ChatEngineProvider._chatEngine = null;
            //   // AsyncStorage.clear();
            //   AsyncStorage.getAllKeys()
            //   .then(keys => {
            //       keys = keys.filter((value, index, arr) => {
            //           if(value !== 'deviceToken') {
            //               return value;
            //           }
            //       });
            //       console.log("keys", keys)
            //       AsyncStorage.multiRemove(keys);
            //   });
            // }
           
          })
         
            
        });
    }

    static getChatRoomModel() {

        // if (this._chatRoomModel.state.chat){
        //     var chat = this._chatRoomModel.state.chat;
        
        //     var pushToken = ''
        //     AsyncStorage.getItem('deviceToken').then((token)=> {
        //        // console.log("shjkdfkjhsaj")
        //         pushToken = token
        //         console.log("chat sjhfdkhsdfk jsh j")
        //         console.log(chat)
        //         console.log("Device token is "+ pushToken)
        //         let chats = [chat];
    
        //     ChatEngineProvider._chatEngine.me.notifications.enable(chats, pushToken, (error) => {
        //         console.log("enter into push enable........")
        //       if (error !== null) {
        //         Alert.alert(
        //           'Push Notification error',
        //           `Unable to enable notifications for global: ${error.message}`,
        //           [{ text: 'OK' }],
        //           { cancelable: true }
        //         );
        //       } else {
        //         // Enable message publish button.
        //         console.log("enavble push notification........")
        //       }
        //     });
        // })
        // }
       
        return this._chatRoomModel;
    }
}

export default ChatEngineProvider;
