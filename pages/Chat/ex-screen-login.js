import React from "react";

import {
    AsyncStorage,
    ImageBackground,
    KeyboardAvoidingView,
    Platform,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    View,
    Image
} from 'react-native';

import {Icon, Text} from "react-native-elements";
import ChatEngineProvider from "./ce-core-chatengineprovider";
import ThemeStyle from "./ce-theme-style";
import ThemeColors from "./ce-theme-colors";
import {NavigationActions, StackActions} from 'react-navigation';

import api from '../../api';

class LoginScreen extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            users: [],
            address: '',
            username: '',
            userId: '',
            id: '',
            name: '',
            mobile: '8188879702',
            profile_pic: '',
            profile_pic_uri: require('../../assets/userProfile.png'),
        }


        this._username = ''
        this._name = ''
        this._password = ''
    }


    componentDidMount() {
        this.getUserDetailValues()
        this.getUDetailValues()
        this.getUsers()
    }

    async getUserDetailValues() {
        var _this = this
        _this.setState({})
        await AsyncStorage.getItem('userDetailValues').then(data => {
            var detailvalue = JSON.parse(data);
            _this.state.userId = detailvalue.user.id
            _this.state.access_token = detailvalue.access_token;
            _this.getPageContent(this.state.userId)
            _this.getUsers(detailvalue.access_token)
        });
    }


    async getPageContent(userId) {
        var _this = this
        let response = await api.getRequest('/users/' + userId, 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    if (data.user.picture) {
                        _this.setState({
                            profile_pic_uri: {uri: data.user.picture}
                        })
                    }
                    _this.setState({
                        userId: data.user.id,
                        address: data.user.address,
                        username: data.user.first_name,
                        mobile: data.user.mobile,
                        id: data.user.id,
                        name: data.user.name

                    })
                })
        } else {
            _this.setState({})
            alert(response)
        }
    }


    async getUsers(access_token) {
        var _this = this
        let response = await api.getRequest('/users', 'GET', 'Bearer ' + access_token);

        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    _this.setState({
                        users: data.users
                    });
                });
        } else {
        }
    }

    getUDetailValues = (value) => {
        this.setState({
            userId: value
        });
    };

    _onButtonPress = () => {
        ChatEngineProvider.connect(this.state.mobile).then(async () => {
            try {
                await AsyncStorage.setItem(ChatEngineProvider.ASYNC_STORAGE_USERDATA_KEY, JSON.stringify({
                    username: this.state.mobile
                }));
            } catch (error) {
            }

            this.props.navigation.navigate('ChatList', {mobile: this.state.mobile},
                {username: this.state.username});
        });
    };

    renderContents() {
        return (
            <ImageBackground style={LoginStyles.login_wallpaper} source={require('./images/login_gradient.png')}>

                <Icon style={LoginStyles.login_logo} name="chat" size={220} color={ThemeColors.foreground_bright}/>


                <TextInput style={LoginStyles.login_input}
                           autoCapitalize="none"
                           onChangeText={(text) => {
                               this.state.mobile = text;
                           }}
                           editable={false}
                           autoCorrect={false}
                           defaultValue={this.state.mobile}
                           keyboardType='email-address'
                           returnKeyType="next"
                           placeholder='Email or Username'
                           placeholderTextColor={ThemeColors.input_dark_placeholder_textcolor}/>

                <TextInput style={LoginStyles.login_input}
                           autoCapitalize="none"
                           onChangeText={(text) => {
                               this._name = text;
                           }}
                           autoCorrect={false}
                           keyboardType='default'
                           returnKeyType="next"
                           placeholder='Display Name'
                           placeholderTextColor={ThemeColors.input_dark_placeholder_textcolor}/>


                <TouchableOpacity style={LoginStyles.login_button}
                                  onPress={this._onButtonPress}>
                    <Text style={LoginStyles.login_buttonText}>LOGIN</Text>
                </TouchableOpacity>

                <Text>{this.state.userId}</Text>


            </ImageBackground>
        );
    }

    render() {

        name = this.state.users.map(function (item) {
            return item['name']
        })


        userId = this.state.users.map(function (item) {
            return item['id']
        });

        picture = this.state.users.map(function (item) {
            return item['picture']
        });

        if (Platform.OS === 'ios') {
            return (
                <KeyboardAvoidingView style={LoginStyles.login_container} behavior="padding" enabled>
                    {this.renderContents()}
                </KeyboardAvoidingView>
            );
        }

        return (
            <View style={LoginStyles.login_container}>
                {this.renderContents()}


            </View>
        );
    }
}

const LoginStyles = StyleSheet.create({
    login_container: {
        flex: 1,
        flexDirection: 'column',
        padding: 0
    },
    login_wallpaper: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        width: ThemeStyle.DEVICE_WIDTH,
        height: ThemeStyle.DEVICE_HEIGHT,
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 20,
        paddingRight: 20
    },
    login_logo: {
        width: ThemeStyle.DEVICE_WIDTH - 40,
        alignItems: 'center',
    },
    login_input: {
        width: ThemeStyle.DEVICE_WIDTH - 40,
        color: ThemeColors.input_dark_textcolor,
        borderRadius: 20,
        paddingHorizontal: 20,
        fontSize: 18,
        height: 40,
        backgroundColor: ThemeColors.input_bg,
        marginBottom: 20,
        padding: 10
    },
    login_button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: ThemeColors.button_background,
        paddingVertical: 15,
        borderRadius: 20,
        marginBottom: 120
    },
    login_buttonText: {
        color: ThemeColors.button_text,
        textAlign: 'center',
        fontWeight: '700'
    }
});

export default LoginScreen;
