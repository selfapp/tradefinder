import React, {Component} from 'react';
import {
    StyleSheet, Text, View, ScrollView, Image, TouchableOpacity,
    FlatList,
} from 'react-native';
import StarRating from 'react-native-star-rating';
import api from '../api';
import Spinner from 'react-native-loading-spinner-overlay';
import DefaultImage from '../Components/DefaultImage';
import {BoldText, RegularText, ItalicText} from '../Components/styledTexts'

export default class TradeRoutesList extends Component {
    static navigationOptions = ({navigation}) => ({
        headerTitle: 'Trade Details',
        tabBarLabel: 'Trade Details',

        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => navigation.goBack(null)}>
                <Image
                    source={require('../assets/back.png')}
                    style={{marginLeft: 15, height: 30, width: 18}}
                />
            </TouchableOpacity>
        ),

        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 5,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#fff',
                fontFamily: "NolanNext-Bold",
            }
    });

    constructor() {
        super();
        this.state = {
            tradeListArray: [],
            spinner: false,

        }
    }

    componentDidMount() {
        console.log("uwk wiue yeirwuy")
        this.getTradeList();
    }


    getTradeList = async () => {
        var _this = this
        _this.setState({
            spinner: true
        });
        try {
            let response = await api.getRequest('/trade', 'GET');
            if (response.status === 200) {
                response.json()
                    .then(function (data) {
                        _this.setState({
                            spinner: false, tradeListArray: data

                        });
                    });
            } else {
                _this.setState({
                    spinner: false
                });
                console.log("error response ghjghj")
                console.log(response)
              //  alert(response)
            }
        } catch (error) {
        }
    };

    render() {
        return (
            <ScrollView ref='scrollView' style={styles.scrollContainer}>
                <FlatList
                    data={this.props.navigation.state.params.tradeRoutes[this.props.navigation.state.params.index].trades}
                    renderItem={({item, index}) =>
                        <View>
                            <TouchableOpacity
                                style={{
                                    height: 80, marginTop: '5%',
                                    marginLeft: '5%', marginRight: '5%',
                                    borderRadius: 8, borderColor: 'gray', borderWidth: 1,
                                    width: '90%',
                                    flexDirection: 'row'
                                }}
                                // onPress={() => this.props.navigateToTradeStep(this.state.tradeListArray.mytrade[index])}
                            >
                                <View style={{width: '100%', height: '100%'}}>
                                    <View style={{height: '20%', backgroundColor: 'grey', borderRadius: 6}}>
                                        <BoldText style={{
                                            textAlign: 'center',
                                            fontSize: 11,
                                            color: "white"
                                        }}>{this.props.navigation.state.params.tradeRoutes[this.props.navigation.state.params.index].name}</BoldText>
                                    </View>
                                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                        {/* <DefaultImage
                                        defaultImageStyle={{ height: 50, width: 50, borderRadius: 25, marginLeft: 15, marginTop: 9 }} /> */}
                                        <Image
                                            resizeMode='contain'
                                            source={{uri: this.props.navigation.state.params.tradeRoutes[this.props.navigation.state.params.index].trades[0].item.pictures[0].picture}}
                                            style={{
                                                height: 50,
                                                width: 50,
                                                borderRadius: 25,
                                                marginLeft: 15,
                                                marginTop: 9
                                            }}/>
                                        <View style={{alignSelf: 'center'}}>
                                            <FlatList
                                                data={item.trades_with}
                                                horizontal={true}
                                                renderItem={({item, index}) =>
                                                    <View style={{justifyContent: 'center', alignSelf: 'center'}}>
                                                        <Image resizeMode={"contain"}
                                                               source={require("../assets/forward_red.png")}
                                                               style={{
                                                                   height: 16, width: 16, marginTop: 5, marginLeft: 5
                                                               }}
                                                        />
                                                    </View>}/>
                                        </View>

                                        <Image
                                            resizeMode='contain'
                                            source={{uri: item.trades_with[0].pictures[0].picture}}
                                            style={{
                                                height: 50,
                                                width: 50,
                                                borderRadius: 25,
                                                marginRight: 15,
                                                marginTop: 9
                                            }}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    }/>


                <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.6)'
                    textStyle={{color: '#00DBBB'}}
                />

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#ffffff'
    }
});
