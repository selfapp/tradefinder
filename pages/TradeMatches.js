import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    AsyncStorage,
    TouchableOpacity,
    ScrollView, Text
} from 'react-native';
import _ from 'underscore'
import Spinner from 'react-native-loading-spinner-overlay';
import navigate from '../Components/navigate';
import api from '../api';
import Grid from '../Components/Grid';
import {
    RegularText
} from '../Components/styledTexts'

export default class TradeMatches extends Component {

    static navigationOptions = ({navigation}) => ({
        title: 'Trade Matches',
        tabBarLabel: 'Matches',
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../assets/tradeMatches.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
        headerLeft: (
            <TouchableOpacity activeOpacity={0.5}

                              onPress={() =>{
                                  AsyncStorage.getItem('profileCompleted').then((value) => {
                                      value === '1' ? navigation.navigate('PostItem') : alert('Please create a profile before posting the item.')
                                  });
                              }}
            >
                <Image
                    source={require('../assets/white_plus_icon.png')}
                    style={{marginLeft: 15, height: 28, width: 28}}
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity
                onPress={() => navigation.navigate('Offers')}
                activeOpacity={0.5}>
                <Image
                    source={require('../assets/chat.png')}
                    style={{marginRight: 15, height: 35, width: 35}}
                />
            </TouchableOpacity>
        ),
        headerTintColor: '#ffffff',
        headerStyle:
            {
                backgroundColor: 'rgba(52, 219, 184, 1)',
                borderBottomColor: '#ffffff',
                borderBottomWidth: 0,
            },
        headerTitleStyle:
            {
                fontWeight: '700',
                fontSize: 20,
                marginTop: 2,
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#fff',
                fontFamily: "NolanNext-Bold",
            }
    });

    constructor(props) {
        super(props);
        this.state = {
            customStyleIndex: 0,
            spinner: false,
            userId: '',
            tradingItems: false,
            itemDetailValues: '',
            tradeRouteItems: [],
            myTradeRouteItem: [],
            mySelectedItem: '', mySelectedItemName: '', selectedItemPicture: '',
            matchedItemlists: [0, 0, 0, 0, 0],
            TradeRoutebackButton: false,
            showMyTradeRouteItemsDetail: false,
            TradeDetailArray: [{
                title: 'Your Item',
                image: require('../assets/forward.png'),
                file: require('../assets/box.png'),
                color: "#00C0EF"
            },
                {
                    title: 'Trade Match',
                    image: require('../assets/forward.png'),
                    file: require('../assets/box.png'),
                    color: "gray"
                }
                
            ]
        };
        this.channel_detail = [];
        this.onReceived = this.onReceived.bind(this);
    }

    componentDidMount() {
        this.getUserDetailfromLocalDatbase()
    }

    onReceived(notification) {
    }

    save_channel = async () => {
        var _this = this;
        let body = {
            "channel": this.channel_detail
        }
        try {
            let response = await api.request('/channel', 'POST', body);
            if (response.status === 200) {
                response.json().then((data) => {
                    _this.setState({
                        spinner: false
                    });
                    navigate.navigateTo(this.props.navigation, 'Offers')
                    _this.channel_detail = [];
                });
            } else {
                _this.setState({spinner: false});
            }
        } catch (error) {
            _this.setState({spinner: false});
            error.json().then((err) => {
            });
            _this.setState({spinner: false});
        }
    }

    set_channel_detail = async (other_user_id) => {
        var channel_name = "";
        if (this.state.userId < other_user_id) {
            channel_name = this.state.userId + "-" + other_user_id;
        } else {
            channel_name = other_user_id + "-" + this.state.userId;
        }
        this.channel_detail.push({
            channel_name: channel_name,
            item_with_user_id: other_user_id
        })
    }


    setMyItem = item => {
        console.log('setMyItem in trade matches' + item);
        const TradeDetailArrayCopy = [...this.state.TradeDetailArray];
        const index = this.state.matchedItemlists.indexOf(0);

        if (this.state.tradingItems) {
            this.set_channel_detail(item.user.id)
            if (index != -1) {
                const matchedItemlistsCopy = [...this.state.matchedItemlists]
                matchedItemlistsCopy[index] = item.id
                TradeDetailArrayCopy[index + 1].file = {uri: item.pictures[0].picture}
                this.setState({TradeDetailArray: TradeDetailArrayCopy, matchedItemlists: matchedItemlistsCopy})
            } else {
                alert('You can select only 5 items')
            }
        } else {
            TradeDetailArrayCopy[0].file = {uri: item.pictures[0].picture}
            this.setState({
                TradeDetailArray: TradeDetailArrayCopy, mySelectedItem: item.id, mySelectedItemName: item.title,
                selectedItemPicture: item.pictures[0].picture
            })
        }
    }

    getToInitialState = () => {
        this.getmyItemList()
        this.setState(
            {
                mySelectedItem: '',
                matchedItemlists: [0, 0, 0, 0, 0],
                TradeDetailArray: [{
                    title: 'Your Item',
                    file: require('../assets/box.png'),
                    color: "#00C0EF"
                },
                    {
                        title: 'Trade1',
                        file: require('../assets/question.png'),
                        color: "#626464"
                    },
                    {
                        title: 'Trade2',
                        file: require('../assets/question.png'),
                        color: '#626464'
                    },
                    {
                        title: 'Trade3',
                        file: require('../assets/question.png'),
                        color: '#626464'
                    },
                    {
                        title: 'Trade4',
                        file: require('../assets/question.png'),
                        color: '#626464'
                    },
                    {
                        title: 'Trade5',
                        file: require('../assets/question.png'),
                        color: '#626464'
                    }
                ]
            });
    }


    async getUserDetailfromLocalDatbase() {
        var _this = this
        _this.setState({
            spinner: true
        })
        await AsyncStorage.getItem('userDetailValues').then(data => {
            var detailvalue = JSON.parse(data);
            _this.state.userId = detailvalue.user.id
            _this.getmyItemList()
            _this.getUserDetail()
        });
    }

    async getUserDetail() {
        var _this = this
        let response = await api.getRequest('/users/' + _this.state.userId, 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    _this.setState({
                        spinner: false,
                        tradingItems: false
                    })
                    let userDetail = _.omit(data.user, "item", "trade_chain", "tradeChain")
                    AsyncStorage.setItem('userDetail', JSON.stringify(userDetail));
                    myTradeRouteItem = _.filter(data.user.item, function (trade_item) {
                        return (trade_item.trade.length > 0 || trade_item.trade_with.length > 0);
                    });
                    _this.setState({
                        myTradeRouteItem: myTradeRouteItem
                    });
                })
        } else {
            _this.setState({
                spinner: false
            })
        }
    }


    saveTrade = async () => {
        var _this = this
        let matchedIdList = [];

        if (_this.state.matchedItemlists === [0, 0, 0, 0, 0]) {
            alert('Please select at least one item for trade')
        } else {
            for (var i = 0; i < _this.state.matchedItemlists.length; i++) {
                if (_this.state.matchedItemlists[i] != 0) {
                    matchedIdList.push(_this.state.matchedItemlists[i])
                } else {
                    break;
                }
            }
            let body = {
                "trade_item_id": _this.state.mySelectedItem,
                "tradewith": matchedIdList
            }
            try {
                let response = await api.request('/trade', 'POST', body);
                if (response.status === 200) {
                    response.json().then((data) => {
                        _this.setState({
                            spinner: false
                        })
                        _this.getToInitialState()
                        _this.getUserDetail()
                        _this.save_channel()
                    });
                } else {
                    _this.setState({spinner: false});
                }
            } catch (error) {
                _this.setState({spinner: false});
            }
        }
    }

    getSearchedResults = async () => {
        alert("gaurav")
        var _this = this
        _this.setState({
            spinner: true
        })
        let body = {
            "item": {
                "name": '',
                "min_value": '',
                "max_value": '',
                "distance_in_miles": '',
                "category_id": ''
            }
        }
        // try {
        let response = await api.request('/items/search', 'POST', body);
        if (response.status === 200) {
            response.json().then((data) => {
                _this.setState({
                    spinner: false,
                    itemDetailValues: data,
                    tradingItems: true
                })

            });
        } else {
            _this.setState({spinner: false});
        }
    };

    getmyItemList = async () => {
        var _this = this
        _this.setState({
            spinner: true
        })
        let response = await api.getRequest('/user/items', 'GET');
        if (response.status === 200) {
            response.json()
                .then(function (data) {
                    _this.setState({
                        itemDetailValues: data.items,
                        tradingItems: false,
                        spinner: false
                    })
                })
        } else {
            _this.setState({
                spinner: false
            })
        }
    }

    handleCustomIndexSelect = (index) => {
        this.setState({
            ...this.state,
            customStyleIndex: index,
        });
    }

    setTradeRoutebackButton = (isShow) => {
        if (isShow) {
            this.setState({
                TradeRoutebackButton: true,
                showMyTradeRouteItemsDetail: true
            })
        } else {
            this.setState({
                TradeRoutebackButton: false,
                showMyTradeRouteItemsDetail: false
            })
        }
    }


    render() {
        return (
            <View style={styles.mainContainer}>

                <View style={{height: '10%', justifyContent: 'center'}}>
                    <RegularText style={styles.text}>
                        Select an item to see what you can get
                    </RegularText>
                </View>

                <ScrollView style={{
                    width: '97%',
                    marginLeft: '1.5%',
                    marginRight: '1.5%',
                    borderWidth: 1,
                    borderColor: 'gray',
                    borderRadius: 5,
                    marginBottom: 10
                }}>
                    <View style={styles.mainContainer}>
                        <View style={{ flexDirection: 'row', width:'100%' }}>
                            {/* <ScrollView horizontal={true} directionalLockEnabled={true}
                                        style={{width: '100%'}}> */}
                                {this.state.TradeDetailArray.map((data, key) =>
                                    (
                                        <View key={key} style={{borderColor: 'gray', borderWidth: 0, width:'50%'}}>
                                            <View style={{
                                                backgroundColor: data.color,
                                                width: '100%',
                                                marginLeft: '0%',
                                                marginRight: 0,
                                                marginTop: 0,
                                                marginBottom: 0,
                                            }}>
                                                <RegularText style={{
                                                    textAlign: 'center',
                                                    color: '#ffffff',
                                                    fontWeight: 'bold'
                                                }}>{data.title}</RegularText>
                                            </View>

                                            <TouchableOpacity style={{ flexDirection:'row', width:'100%', justifyContent:'space-between',marginBottom: 15 }}>
                                                <View style={{
                                                    width:'100%', justifyContent:'space-around' ,
                                                    alignItems:'center', flexDirection:'row', borderBottomWidth: (key === 0) ? 0 : 1,
                                                    borderLeftWidth:(key === 0) ? 0 : 1,
                                                    borderBottomColor:'gray',borderLeftColor:'gray'
                                                }}>
                                                {(key === 1)?
                                                <View style={{ height:'100%', alignItems:'center', flexDirection:'row'}}>
                                                    <Image
                                                        resizeMode='contain'
                                                        source={require('../assets/forward.png')} 
                                                        style={{
                                                        width: 30,
                                                        height: 30,
                                                        alignSelf: 'center',
                                                    }}/>
                                                    <View style={{ backgroundColor:'gray', width: 1,height:'100%', marginLeft: 15}}></View>
                                                    </View>
                                                    : (null) }

                                                    <Image
                                                        resizeMode='contain'
                                                        source={data.file} style={{width: 65, height: 65}}
                                                        />
                                                        

                                                </View>

                                                

                                            </TouchableOpacity>
                                        </View>
                                    ))
                                }
                            {/* </ScrollView> */}
                        </View>
                        <Grid
                            itemsList={this.state.itemDetailValues}
                            passingGridData={(data) => {
                                console.log('setMyItem in trade matches in passingGridData');
                                this.setMyItem(data);
                            }}

                            mySelectedItem={this.state.mySelectedItem}
                            mySelectedItems={this.state.mySelectedItems}
                            tradingItems={this.state.tradingItems}
                        />

<TouchableOpacity  disabled={!this.state.mySelectedItem}
                        style={{
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor: '#1FDBB8',
                            marginTop: 20,
                            marginBottom:50,
                            height: 50,
                            marginLeft: '5%',
                            marginRight:'5%',
                        }}
                        onPress= {
                            () => this.props.navigation.navigate("TradeMatchesSecond",
                                {
                                    itemName: this.state.mySelectedItemName,
                                    selectedItemPicture: this.state.selectedItemPicture,
                                    itemId: this.state.mySelectedItem
                                })}>
                        <Text style={{
                            fontSize: 22, color: 'white',
                            fontWeight: '700',
                            fontFamily: "NolanNext-Regular"
                        }}>Find Matches</Text>
                </TouchableOpacity>


                    </View>
                </ScrollView>


                <Spinner
                    visible={this.state.spinner}
                    textContent={"Loading..."}
                    color='#00DBBB'
                    overlayColor='rgba(0, 0, 0, 0.6)'
                    textStyle={{color: '#00DBBB'}}
                />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#ffffff',
        flex: 1
    },
    icon: {
        resizeMode: 'contain'
    },
    tabStyle: {
        borderColor: '#D52C43'
    },
    activeTabStyle: {
        backgroundColor: '#D52C43',
    },
    tabTextStyle: {
        color: '#D52C43'
    },
    text: {
        fontSize: 15,
        textAlign: 'center',
        color: '#585858',
        marginTop: 8,
        marginBottom: 10,
        marginLeft: '10%',
        marginRight: '10%',
        fontWeight: 'bold'
    }
});
